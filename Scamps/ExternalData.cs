﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;

namespace Scamps
{
        [Serializable()]
        [XmlRoot("external")]
        public class ExternalData
        {
            [Serializable()]
            public class ExternalInfo
            {
                public string scamp { get; set; }
                public string form { get; set; }
            }

            [Serializable()]
            public class ExternalFeed
            {
                public string connection { get; set; }
                public string data { get; set; }
                public string mapping { get; set; }
            }

            [XmlElement("info")]
            public ExternalInfo info { get; set; }

            [XmlArray("feeds")]
            [XmlArrayItem("feed", typeof(ExternalFeed))]
            public ExternalFeed[] feeds;

            private static ExternalData parseFile(string filepath)
            {
                ExternalData self = null;
                
                if (!File.Exists(filepath))
                    return self;

                using (var rd = XmlReader.Create(File.OpenRead(filepath)))
                {
                    var xs = new XmlSerializer(typeof(ExternalData));
                    if (!xs.CanDeserialize(rd))
                        return self;
                    self = xs.Deserialize(rd) as ExternalData;
                }

                return self;
            }
            /// <summary>
            /// Maps a form name, ie "TOF_SDF1_PRE-OP_EVAL" to the on relative path of the form .xml file. This does not ensure it exists.
            /// </summary>
            /// <param name="form"></param>
            /// <returns></returns>
            public static string GetExternalFormRelativePath(string form)
            {
                return string.Format("\\resources\\external\\{0}.xml", form); //Lets us move it to the web config later while only having to update the logic here.
            }

            /// <summary>
            /// Imports measurement data from an external SQL source for a given patient or appointment given the path to the 
            /// XML file which contains the definiting information how how the data is accessed, retrieved, and mapped.
            /// </summary>
            /// <param name="mrn">identifier for specifying what patient to find the external data for. Can be left empty.</param>
            /// <param name="appt_id">identifier for specifying what appointment to find the external data for. Can be left empty.</param>
            /// <param name="filepath">file path to the file which contains the external data mapping definitions</param>
            /// <returns>Dictionary with the key representing the element code, and value representing the imported value</returns>
            public static Dictionary<string, string> ImportMeasurements(string mrn, string appt_id, string filepath)
            {
                //attempt to deserialize the XML file to an object
                var ed = ExternalData.parseFile(filepath);

                //if ed is null, unable to parse file defining external data feed
                if (ed == null)
                {
                    Console.WriteLine("ERROR parsing file {0}", filepath);
                    return null;
                }

                //stores the element_cd with values retrieved
                var res = new Dictionary<string, string>();
                //defines how an external feed maps to an element
				//e.g. external data with key 'external_ab' might map to SDF element codes 'ele_a' and 'ele_b' ...
                //a single external feed can map to multiple element_cds, hence the list
                var dmap = new Dictionary<string, List<string>>();

                //for each feed defined in the file, read the results for the specified patient or appointment and map it to an element code
                foreach (var feed in ed.feeds)
                {
                    //read the connection string name
                    var cname = feed.connection;
                    if (cname == "external")
                        cname = "External";
                    //lookup the connection name in web.config
                    var connstr = ConfigurationManager.ConnectionStrings[feed.connection];

                    if (connstr == null)
                        throw new Exception(string.Format("Unable to find connection string '{0}'", cname));

                    //for each new feed, clear the mapping dictionary
                    if (dmap.Count > 0)
                        dmap.Clear();

                    //process mapping string
                    var mapPair = feed.mapping.Split(',').Where(str => str.Length > 0);
                    //populate mapping dictionary
                    foreach (var pair in mapPair)
                    {
                        var sp = pair.Split('=');
                        if (!dmap.ContainsKey(sp[0]))
                            dmap.Add(sp[0], new List<string>());
                        dmap[sp[0]].Add(sp[1]);
                    }

                    //connect to external data source
                    using (var conn = new Scamps.DataTools(connstr))
                    {

                        var query = feed.data;
                        //insert the mrn and appt_id parameters if applicable to the query statement
                        if (query.Contains("[mrn]"))
                        {
                            if (string.IsNullOrEmpty(mrn))
                                continue;
                            query = query.Replace("[mrn]", mrn);
                        }
                        if (query.Contains("[appt_id]"))
                        {
                            if (string.IsNullOrEmpty(mrn))
                                continue;
                            query = query.Replace("[appt_id]", appt_id);
                        }

                        //for each record, only map element codes defined in the mapping statement
                        using (var rd = conn.getDataReader(query))
                        {
                            if (rd != null)
                            {
                                while (rd.Read())
                                {
                                    var key = rd.GetValue(0).ToString();
                                    var val = rd.GetValue(1).ToString();

                                    //if a mapping definition exists, get the element_cd translation and set the value in the result dictionary
                                    if (dmap.ContainsKey(key))
                                    {
                                        foreach (var t in dmap[key])
                                            res[t] = val;
                                    }
                                }
                            }
                        }
                    }
                }
                return res;
            }
        }
}
