﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Scamps
{
    public class Element
    {
        //data handling
        public string field = "";
        public string name = "";                    //for complex elements, master name/field
        public string label = "";
        public string title = "";
        public string subtitle = "";
        public string helptext = "";
        public string placeholder = "";
        public string list = "";
        public string listselector = "";
        public string layout = "";
        public string panel = "";
        public string version = "";
        public string units = "";
        public string ID = "";
        public bool error = false;
        public string image = "";
        //reliance and triggers
        public bool trigger = false;
        public string parent = "";                  // parent of this element that triggers its visibility 
        public string trigger_value = "";
        public string trigger_operator = "";
        public string link = "";
        public string formula = "";
        public string list_cd = "";
        //layout
        public string region = "";
        public string attributes = "";
        public string rowlabels = "";               //complex elements row and column headings in table view.
        public string columnlabels = "";
        public string rowsummary = "";
        public string columnsummary = "";

        //HTML Markup
        public string type = "";        //needs to be an enum eventually.  Gonna be a switch statement for now.  ugh.
        public string types = "";       //list of control types for complex control support

        public bool modern = true;      //default markup for modern browsers or for polyfill libraries that support modern input types
        public bool addIDs = true;      //Add fieldname_ID as an ID attribute to each element, default is true.
        public int? rows = 0;           //for multiselect "SIZE" attribute and text area "ROWS" attribute
        public int? cols = 0;           //for text area "COLS" attribute, possible for check list control indicating columns TODO
        public bool signed = false;     // for numeric fields, is the number value signed (+), i.e. does not allow negitive numbers;
        public bool UseRequiredAttribute = false;  //add required="required" to the markup for any required field, default = false

        public string CalendarIconMarkup = "";

        //CSS classes
        public string wrapperclass = "";  //added to the template at [wrapperclass]
        public string elementclass = "";  //added to the element markup
        public string labelclass = "";    //added to the label markup
        public string titleclass = "";    //added to template at [titleclass]
        public string ElementTemplate = "";
        public Dictionary<string, string> formvalues = new Dictionary<string, string>();

        public string value = "";

        // complex and compound support
        public string[] elementcodes = null;

        //validation
        public int datatype = 0;
        /// <summary>
        /// Is this element required, i.e. entering nothing fails form validation.
        /// </summary>
        public bool required = false;
        /// <summary>
        /// Is the element to be displayed as disabled?
        /// </summary>
        public bool disabled = false;
        public string min = "";
        public string max = "";
        public string step = "";
        public bool isreadonly = false;

        public int? length = 4000;   // ZAPHOD... we need to be more intelligent than this.  Painting with a pretty broad brush here.
        // to be redacted - 4000 is the current max lenght of the backing field. This needs to change under the new schema.
        // at some point nullable int became unnullable.  Back to nullable.
        // ultimately, this only needs to be nullable if the load method can't intelligently handle 
        // non-specified values for length.  We might want to consider handling length as a string
        // and just cast-checking it.

        public bool secondaryrequired = false;

        /// <summary>
        /// Regex pattern used to match user entry.  If there is no match, the element fails validation.
        /// </summary>
        public string validation_pattern = "";
        public string error_message = "";


        public string errormessage = "";

        private string _type = "";
        public string ImagePath = "";

        private bool _haserrors = false;
        private string _errors = "";
        private bool _hasmessages = false;
        private string _messages = "";

        public bool HasErrors
        {
            get
            {
                return _haserrors;
            }
        }
        public bool HasMessages
        {
            get
            {
                return _hasmessages;
            }
        }
        public string errors
        {
            get
            {
                return _errors;
            }
            set
            {
            }
        }

        private void Message(string message)
        {
            _hasmessages = true;
            _messages += message + System.Environment.NewLine;
        }

        private void Error(string errormessage)
        {
            _errors += errormessage + System.Environment.NewLine;
            _haserrors = true;
        }

        public class Summary
        {
            public string label = "";
            public string action = "";
        }
        // ==== Methods  =====

        public string InstanceTemplate()
        {
            string template = "";
            switch (layout)
            {
                case "span":

                    template = System.Environment.NewLine;
                    template += "\t<tr[wrapperclass]>" + System.Environment.NewLine;
                    template += "\t\t<td colspan=\"2\" class=\"span [parent]\">" + System.Environment.NewLine;
                    template += "\t\t\t<div class=\"question alt\">" + System.Environment.NewLine;

                    template += "\t\t\t\t<label for=\"[elementid]\"[labelclass]>[label]</label>" + System.Environment.NewLine;
                    template += "\t\t\t\t<i class=\"icon-info-circled helpinfo\" data-info=\"[help]\"></i>" + System.Environment.NewLine;
                    template += "\t\t\t\t<span>" + subtitle + "</span>" + System.Environment.NewLine;
                    template += "\t\t\t</div>" + System.Environment.NewLine;


                    template += "\t\t<div class=\"answer[parent]\">" + System.Environment.NewLine;
                    template += "\t\t\t[*required*][element][errormessage]" + System.Environment.NewLine;
                    template += "\t\t</div>" + System.Environment.NewLine;
                    template += "\t</tr>" + System.Environment.NewLine;

                    break;

                case "inline":

                    template = System.Environment.NewLine;
                    template += "\t<tr[wrapperclass]>" + System.Environment.NewLine;
                    template = Tools.AddClass(template, "inline", "tr");
                    template += "\t\t<td class=\"inline[parent]\">" + System.Environment.NewLine;
                    template += "\t\t\t<i class=\"icon-info-circled helpinfo\" data-info=\"[help]\"></i>" + System.Environment.NewLine;

                    template += "\t\t\t<label for=\"[elementid]\"[labelclass]>[label]</label>" + System.Environment.NewLine;
                    template += "\t\t\t<span>" + subtitle + "</span>" + System.Environment.NewLine;
                    template += "\t\t</td>" + System.Environment.NewLine;
                    template += "\t\t<td class=\"answer[parent]\">" + System.Environment.NewLine;
                    template += "\t\t\t[*required*][element][errormessage]" + System.Environment.NewLine;
                    template += "\t\t</td>" + System.Environment.NewLine;
                    template += "\t</tr>" + System.Environment.NewLine;
                    break;

                default:

                    template = System.Environment.NewLine;
                    template += "\t<tr[wrapperclass]>" + System.Environment.NewLine;
                    template += "\t\t<td class=\"question[parent]\">" + System.Environment.NewLine;
                    template += "\t\t\t<i class=\"icon-info-circled helpinfo\" data-info=\"[help]\"></i>" + System.Environment.NewLine;

                    template += "\t\t\t<label for=\"[elementid]\"[labelclass]>[label]</label>" + System.Environment.NewLine;
                    template += "\t\t\t<span>" + subtitle + "</span>" + System.Environment.NewLine;
                    template += "\t\t</td>" + System.Environment.NewLine;
                    template += "\t\t<td class=\"answer[parent]\">" + System.Environment.NewLine;
                    template += "\t\t\t[*required*][element][errormessage]" + System.Environment.NewLine;
                    template += "\t\t</td>" + System.Environment.NewLine;
                    template += "\t</tr>" + System.Environment.NewLine;

                    break;
            }
            return template;
        }


        public string RenderElement()
        {
            string tmp = "";
            string template = ElementTemplate;
            if (template.Length == 0)
            {
                template = InstanceTemplate();
            }

            string element = "";
            string elementid = "";
            string elementidstring = "";
            if (addIDs)
            {
                if (ID.Length == 0)
                {
                    if (name.Length > 0)
                    {
                        elementidstring = " id=\"" + name + "_id\"";
                        elementid = name + "_id";
                    }
                    else
                    {
                        elementidstring = " id=\"" + field + "_id\"";
                        elementid = field + "_id";
                    }
                }
                else
                {
                    elementidstring = " id=\"" + ID + "\"";
                    elementid = ID;
                }

            }

            if (trigger)
            {
                appendElementClass("trigger");
            }

            //need to support HTML 5 markup + old IE markup dynamically
            //this means different element types.  The boolean 'modern', default TRUE, changes the element type to modern HTML5 standards
            //if !modern, the element type reverts back to text.  A class is added regardless so that elements can be styled and targeted
            //in IE or modern browsers.

            int p = 0;
            string[] names;
            string[] rowz;
            string[] columns;

            switch (type.ToLower())
            {
                case "hidden":
                    template = createElement("input", "hidden", elementidstring);
                    break;
                case "text":
                case "textbox":
                    element = createElement("input", "text", elementidstring);
                    break;
                case "calculated":
                    appendElementClass("calculated");
                    isreadonly = true;
                    element = createElement("input", "text", elementidstring);
                    break;
                case "calculated-hidden":
                    appendElementClass("calculated");
                    isreadonly = true;
                    template = createElement("input", "hidden", elementidstring);
                    break;
                case "check":
                case "checkbox":
                    element = createElement("input", "checkbox", elementidstring);
                    break;
                case "password":
                    element = createElement("input", "password", elementidstring);
                    break;
                case "date":
                    appendElementClass("date");
                    if (!modern) { _type = "text"; } else { _type = "date"; }
                    element = createElement("input", _type, elementidstring);
                    element += Swap(CalendarIconMarkup, "title", "Date");
                    break;
                case "pastdate":
                    appendElementClass("pastdate");
                    if (!modern) { _type = "text"; } else { _type = "date"; }
                    this.max = Tools.AsDate(DateTime.Now, "MM/dd/yyyy");
                    element = createElement("input", _type, elementidstring);
                    element += Swap(CalendarIconMarkup, "title", "Date");
                    break;
                case "datetime":
                    appendElementClass("datetime");
                    if (!modern) { _type = "text"; } else { _type = "datetime"; }
                    element = createElement("input", _type, elementidstring);
                    element += Swap(CalendarIconMarkup, "title", "Date & Time");
                    break;

                case "number":
                    appendElementClass("num");
                    if (!modern) { _type = "text"; } else { _type = "number"; }
                    element = createElement("input", _type, elementidstring);
                    break;

                case "float":
                case "signedfloat":
                    tmp = "float";
                    if (type.ToLower() == "signedfloat" || signed == true)
                    {
                        tmp = "sfloat";
                    }
                    appendElementClass("num " + tmp);
                    if (!modern) { _type = "text"; } else { _type = "number"; }
                    element = createElement("input", _type, elementidstring);
                    break;

                case "integer":
                case "int":
                case "positiveint":
                    tmp = "int";
                    if (signed == true)
                    {
                        tmp = "sint";
                    }
                    appendElementClass("num " + tmp);
                    if (!modern) { _type = "text"; } else { _type = "number"; }
                    element = createElement("input", _type, elementidstring);
                    break;

                case "signedint":
                    appendElementClass("num sint");
                    if (!modern) { _type = "text"; } else { _type = "number"; }
                    element = createElement("input", _type, elementidstring);
                    break;

                case "textarea":
                    element = "<textarea name=\"" + field + "\"" + elementidstring + "[elementclass]"
                        + makeAttribute("maxlength", length.ToString())
                        + makeAttribute("data-value", trigger_value)
                        + makeAttribute("data-operator", trigger_operator);

                    if (rows > 0) { element += makeAttribute("rows", rows.ToString()); }
                    if (cols > 0) { element += makeAttribute("cols", cols.ToString()); }
                    element += makeFlag("required", required) + makeFlag("disabled", disabled) + ">" + GetValue() + "</textarea>";
                    break;

                case "select":
                case "selectbox":
                    element = createSelect(elementidstring);
                    break;

                case "multiselect":
                    element = createSelect(elementidstring, true);
                    break;

                case "imageselect":
                    template = createImageSelect();
                    element = createSelect(elementidstring);
                    break;

                case "checkgroup":
                case "checkboxlist":
                case "checklist":
                    element = CheckboxesFromList(field, elementid, list, value);
                    break;

                case "checksingle":
                case "checksinglelist":
                    element = CheckboxesFromList(field, elementid, list, value, false, "singlesel");
                    break;

                case "radiolist":
                    element = CheckboxesFromList(field, elementid, list, value, true);
                    break;

                case "submit":
                    element = "<input type=\"submit\" value=\"" + label + "\""
                            + makeAttribute("id", ID)
                            + makeAttribute("class", elementclass)
                            + makeFlag("disabled", disabled) + "/>";
                    break;

                case "button":
                    element = "<input type=\"button\" value=\"" + label + "\""
                            + makeAttribute("id", ID)
                            + makeAttribute("class", elementclass)
                            + makeFlag("disabled", disabled) + "/>";
                    break;

                case "image":
                    appendElementClass("popimage");

                    string imgvalue = value;
                    if (imgvalue.IsNullOrEmpty()) { imgvalue = name; }
                    element = "<img src=\"" + ImagePath + imgvalue + "\"" + elementidstring;
                    if (helptext != "")
                    {
                        element += makeAttribute("title", helptext);
                    }
                    else
                    {
                        element += makeAttribute("title", label);
                    }
                    element += "[elementclass]/>";
                    break;

                case "label":
                case "readonly":

                    switch (layout)
                    {
                        case "inline":

                            appendElementClass("label inline");
                            if (!parent.IsNullOrEmpty())
                            {
                                appendElementClass(parent + "_child hide");
                            }
                            template = "<tr>" + System.Environment.NewLine
                                        + "\t<td " + elementidstring
                                        + makeClass(elementclass)
                                        + " colspan=\"2\">" + System.Environment.NewLine
                                        + "\t\t\t<h3>" + label + "</h3>" + System.Environment.NewLine

                                        + "\t\t\t" + subtitle + System.Environment.NewLine
                                        + "\t</td>" + System.Environment.NewLine + "</tr>";

                            break;


                        default:
                            if (link.Length == 0)
                            {
                                element = "<span class=\"display\">" + GetValue() + "</span>";
                            }
                            else
                            {
                                element = "<span class=\"display\">" + System.Environment.NewLine
                                            + "<a href=\"" + link.Replace("[value]", GetValue()) + "\">"
                                            + value + "</a></span>";
                            }
                            break;
                    }
                    break;

                case "infobox":

                    appendElementClass("infobox");
                    if (!parent.IsNullOrEmpty())
                    {
                        appendElementClass(parent + "_child hide");
                    }
                    template = "<tr>" + System.Environment.NewLine
                                + "\t<td " + elementidstring
                                + makeClass(elementclass)
                                + " colspan=\"2\">" + System.Environment.NewLine
                                + value + System.Environment.NewLine
                                + "\t</td>" + System.Environment.NewLine + "</tr>";

                    break;

                case "recommendation":

                    appendElementClass("recommend");
                    if (!parent.IsNullOrEmpty())
                    {
                        appendElementClass(parent + "_child hide");
                    }

                    title = title.IsNullOrEmpty() ? "SCAMP RECOMMENDS" : title;
                    template = "<tr[wrapperclass]>" + System.Environment.NewLine
                                + "\t\t<td " + elementidstring + " class=\"recommend question[parent]\">" + System.Environment.NewLine
                                + "\t\t<i class=\"icon-info-circled helpinfo\" data-info=\"" + helptext + "\"></i>" + System.Environment.NewLine
                                + "\t\t<h3>" + title + "</h3>" + System.Environment.NewLine
                                + "\t</td>" + System.Environment.NewLine;

                    Tools.AppendLine(ref template, "\t<td class=\"answer[parent] recommendation\">");
                    Tools.AppendLine(ref template, "\t\t<p>" + label + "</p>");
                    Tools.AppendLine(ref template, "\t\t<p class=\"whyrecommend\">" + subtitle + "</p>");
                    Tools.AppendLine(ref template, value);
                    Tools.AppendLine(ref template, "\t</td>");
                    Tools.AppendLine(ref template, "</tr>");

                    break;
                case "section":


                    template = "\t<tr><td class=\"section\" colspan=\"2\">" + System.Environment.NewLine;
                    template += "<div class=\"section\" data-section=\"" + field + "\">" + System.Environment.NewLine;
                    template += "<h2>" + label + "</h2>";
                    if (helptext.Length > 0) { template += "<i class=\"icon-info-circled helpinfo\" data-info=\"" + helptext + "\"></i>"; }
                    template += System.Environment.NewLine;
                    if (subtitle.Length > 0) { template += "<span>" + subtitle + "</span>" + System.Environment.NewLine; }
                    template += System.Environment.NewLine;
                    template += "</div>" + System.Environment.NewLine;
                    template += "\t</td></tr>" + System.Environment.NewLine;
                    break;

                case "html":
                    //raw dump of whatever is in the value property;
                    template = value;
                    break;


                //==============   COMPOUND AND COMPLEX ELEMENTS   ====================
                case "compound":

                    //this handles controls in lists.
                    bool sdone = false;
                    //workaround for array of units...
                    string locunits = units;
                    string[] fieldz = Tools.SimpleList(field);
                    string[] controlz = Tools.SimpleList(types, true);
                    string[] rowzz = Tools.SimpleList(rowlabels, true);
                    string[] unitz = Tools.SimpleList(locunits, true);
                    string[] minz = Tools.SimpleList(min, true);
                    string[] maxz = Tools.SimpleList(max, true);
                    string instanceclass = "";

                    //render template...
                    string table = "<table class=\"compound\" name=\"" + this.name + "\">" + System.Environment.NewLine
                                  + "\t<tr>" + System.Environment.NewLine;
                    table += "{0}" + System.Environment.NewLine;
                    table += "\t</tr>" + System.Environment.NewLine + "</table>";

                    element = "";

                    if (fieldz.Length > controlz.Length)
                    {
                        Error("Element.Render.controlrow: more fields specified than controls");
                    }
                    else
                    {
                        if (!title.IsNullOrEmpty())
                        {
                            element += "\t<tr><td colspan=\"" + (fieldz.Length - 1).ToString() + "\">" + title + "</td></tr>";
                        }
                        string htmltype = "";

                        for (int i = 0; i < fieldz.Length; i++)
                        {
                            instanceclass = "";
                            units = i < unitz.Length ? unitz[i] : "";
                            htmltype = resolveElement(controlz[i], ref instanceclass);

                            element += "\t\t<td>" + System.Environment.NewLine;
                            if (rowlabels.Length > 0 && rowzz.Length > i) { element += "\t\t\t<label>" + rowzz[i] + "</label><br/>" + System.Environment.NewLine; }

                            if (minz.Length > i) { min = minz[i]; }
                            if (maxz.Length > i) { max = maxz[i]; }
                            switch (htmltype)
                            {
                                case "select":
                                    if (!sdone)
                                    {

                                        element += createSelect(fieldz[i] + "_id", false, fieldz[i], list);
                                        sdone = true;
                                    }
                                    break;
                                default:
                                    element += createElement(htmltype, _type, fieldz[i] + "_id", instanceclass, fieldz[i]);
                                    break;
                            }
                            element += System.Environment.NewLine + "\t\t</td>" + System.Environment.NewLine;
                        }

                        element = table.Replace("{0}", element);
                    }
                    units = locunits;
                    break;
                case "integer-table":
                case "select-table":
                case "float-table":

                    p = 0;
                    field = Tools.NormalString(field);
                    names = field.Split(',');

                    rowlabels = Tools.NormalString(rowlabels);
                    rowz = rowlabels.Split(',');

                    columnlabels = Tools.NormalString(columnlabels);
                    columns = columnlabels.Split(',');

                    int requiredelements = rowz.Length * columns.Length;
                    if (type == "check-table") { requiredelements = rowz.Length; }

                    if (names.Length < requiredelements)
                    {
                        //TODO: error condition   
                        element = "Incorrect field list.  Too many elements for the number of fields specified.";
                        break;
                    }
                    Summary rsum = new Summary();
                    Summary csum = new Summary();

                    if (rowsummary.Length > 0)
                    {
                        rsum.label = Tools.XNode(ref rowsummary, "label").Trim();
                        rsum.action = Tools.XNode(ref rowsummary, "action").Trim().ToLower();
                    }
                    if (columnsummary.Length > 0)
                    {
                        csum.label = Tools.XNode(ref columnsummary, "label");
                        csum.action = Tools.XNode(ref columnsummary, "action").ToLower();
                    }

                    StringBuilder sb = new StringBuilder();

                    sb.Append(System.Environment.NewLine + "<table class=\"complex " + name + "\""
                            + elementidstring
                            + makeAttribute("data-value", trigger_value)
                            + makeAttribute("data-operator", trigger_operator)
                            + ">" + System.Environment.NewLine);

                    sb.Append("\t<thead>" + System.Environment.NewLine);
                    sb.Append("\t\t<tr>" + System.Environment.NewLine);
                    sb.Append("\t\t\t<th>" + title + "</th>" + System.Environment.NewLine);
                    for (int c = 0; c < columns.Length; c++)
                    {
                        sb.Append("\t\t\t<th>" + columns[c] + "</th>" + System.Environment.NewLine);
                    }
                    if (rsum.action.Length > 0)
                    {
                        sb.Append("\t\t\t<th>" + rsum.label + "</th>" + System.Environment.NewLine);
                    }
                    sb.Append("\t\t</tr>" + System.Environment.NewLine);
                    sb.Append("\t</thead>" + System.Environment.NewLine);
                    sb.Append("\t<tbody>" + System.Environment.NewLine);

                    for (int r = 0; r < rowz.Length; r++)
                    {
                        sb.Append("\t\t<tr>" + System.Environment.NewLine);
                        sb.Append("\t\t\t<td class=\"rlabel\"><label>" + rowz[r] + "</label></td>" + System.Environment.NewLine);
                        for (int c = 0; c < columns.Length; c++)
                        {
                            switch (type.ToLower())
                            {
                                case "integer-table":
                                    if (!modern) { _type = "text"; } else { _type = "number"; }
                                    tmp = "int";
                                    if (signed == true)
                                    {
                                        tmp = "sint";
                                    }
                                    tmp += " " + name + "_row_" + r.ToString() + " " + name + "_col_" + c.ToString();

                                    //sb.Append("\t\t\t<td><input type=\"text\" name=\"" + names[p].Trim() + "\"" 
                                    //           + makeAttribute("maxlength",length.ToString())
                                    //           + makeClass(name + " num " + tmp + " " + name + "_row_" + r.ToString() + " " + name + "_col_" + c.ToString())
                                    //           + "/></td>"
                                    //           + System.Environment.NewLine
                                    //           );

                                    sb.Append("\t\t\t<td>" + createElement("input", type, names[p].Trim() + "_id", name + " num " + tmp, names[p].Trim()) + "</td>");
                                    break;

                                case "float-table":
                                    if (!modern) { _type = "text"; } else { _type = "number"; }
                                    tmp = "float";
                                    if (signed == true)
                                    {
                                        tmp = "sfloat";
                                    }
                                    tmp += " " + name + "_row_" + r.ToString() + " " + name + "_col_" + c.ToString();

                                    //sb.Append("\t\t\t<td><input type=\"text\" name=\"" + names[p].Trim() + "\""
                                    //           + makeAttribute("maxlength", length.ToString())
                                    //           + makeClass(name + " num " + tmp + " " + " " + name + "_row_" + r.ToString() + " " + name + "_col_" + c.ToString())
                                    //           + "/></td>"
                                    //           + System.Environment.NewLine
                                    //           );

                                    sb.Append("\t\t\t<td>" + createElement("input", _type, names[p].Trim() + "_id", name + " num " + tmp, names[p].Trim()) + "</td>");
                                    break;
                                case "select-table":
                                    tmp = elementclass;
                                    
                                    elementclass = name + " " + name + "_row_" + r.ToString() + " " + name + "_col_" + c.ToString();
                                    if (!tmp.IsNullOrEmpty()) { elementclass += " " + tmp; }

                                    sb.Append("\t\t\t<td>" + System.Environment.NewLine);

                                    //sb.Append("\t\t\t<select name=\"" + names[p].Trim() + "\""
                                    //          + makeClass(name + " " + name + "_row_" + r.ToString() + " " + name + "_col_" + c.ToString())
                                    //          + ">" + System.Environment.NewLine
                                    //          + OptionsFromList(list, value) + System.Environment.NewLine + "</select>"
                                    //          );
                                    sb.Append("\t\t\t" + createSelect(names[p].Trim() + "_id", false, names[p].Trim(), list));
                                    sb.Replace("[elementclass]", " class = \"" + elementclass + "\"");
                                    sb.Append("\t\t\t</td>" + System.Environment.NewLine);
                                    elementclass = tmp;
                                    break;

                            }

                            p++;
                        }
                        if (rsum.action.Length > 0)
                        {
                            sb.Append("\t\t\t<td class=\"" + name + "_row_" + r.ToString() + " " + rsum.action + "\"><span></span></td>" + System.Environment.NewLine);
                        }
                        sb.Append("\t\t</tr>" + System.Environment.NewLine);
                    }

                    //column summary, if one is defined...
                    if (csum.action.Length > 0)
                    {
                        sb.Append("\t\t<tr>" + System.Environment.NewLine);
                        sb.Append("\t\t\t<td class=\"rlabel summary\"><label>" + csum.label + "</label></td>" + System.Environment.NewLine);
                        for (int c = 0; c < columns.Length; c++)
                        {
                            sb.Append("\t\t\t<td class=\"" + name + "_col_" + c.ToString() + " summary\"><span></span></td>" + System.Environment.NewLine);
                        }

                        if (rsum.action.Length > 0)
                        {
                            sb.Append("\t\t\t<td class=\"" + rsum.action + "\"><span></span></td>" + System.Environment.NewLine);
                        }
                        sb.Append("\t\t</tr>" + System.Environment.NewLine);
                    }

                    sb.Append("\t</tbody>" + System.Environment.NewLine);
                    sb.Append("</table>" + System.Environment.NewLine);

                    element = sb.ToString();

                    break;


                case "check-table":

                    p = 0;
                    field = Tools.NormalString(field);
                    names = field.Split(',');

                    rowlabels = Tools.NormalString(rowlabels);
                    rowz = rowlabels.Split(',');
                    //make this a bit more robust...
                    string loperator = trigger_operator;
                    string lvalue = trigger_value;
                    string[] opz = trigger_operator.Split(',');
                    string[] valz = trigger_value.Split(',');

                    //column labels are the list labels....

                    string[] aM = list.Split(',');

                    string key;
                    string lval;
                    string group;
                    bool ok = false;
                    foreach (string item in aM)
                    {
                        ok = KeyBreak(item, out key, out lval);
                        //get rid of options, not supported here
                        ok = KeyBreak(key, out group, out key, '|');
                        columnlabels += lval + ",";
                    }
                    columnlabels = Tools.NotEndWith(columnlabels, ",");
                    columns = columnlabels.Split(',');

                    if (names.Length < rowz.Length)
                    {
                        //TODO: error condition   
                        element = "Incorrect field list.  Too many elements for the number of fields specified.";
                        break;
                    }

                    StringBuilder sbld = new StringBuilder();

                    sbld.Append(System.Environment.NewLine + "<table class=\"complex " + name + "\""
                        + elementidstring
                        //+ makeAttribute("data-value", trigger_value)
                        //+ makeAttribute("data-operator", trigger_operator)
                            + ">" + System.Environment.NewLine);

                    sbld.Append("\t<thead>" + System.Environment.NewLine);
                    sbld.Append("\t\t<tr>" + System.Environment.NewLine);
                    sbld.Append("\t\t\t<th>" + title + "</th>" + System.Environment.NewLine);
                    for (int c = 0; c < columns.Length; c++)
                    {
                        sbld.Append("\t\t\t<th>" + columns[c] + "</th>" + System.Environment.NewLine);
                    }

                    sbld.Append("\t\t</tr>" + System.Environment.NewLine);
                    sbld.Append("\t</thead>" + System.Environment.NewLine);
                    sbld.Append("\t<tbody>" + System.Environment.NewLine);

                    for (int r = 0; r < rowz.Length; r++)
                    {
                        trigger_operator = r < opz.Length ? opz[r] : "";
                        trigger_value = r < valz.Length ? valz[r] : "";
                        sbld.Append("\t\t<tr>" + System.Environment.NewLine);
                        sbld.Append("\t\t\t<td class=\"rlabel\"><label>" + rowz[r] + "</label></td>" + System.Environment.NewLine);

                        sbld.Append("\t\t\t" + CheckRow(names[r].Trim(), name + "_row_" + r.ToString(), list));

                        sbld.Append("\t\t</tr>" + System.Environment.NewLine);
                    }

                    sbld.Append("\t</tbody>" + System.Environment.NewLine);
                    sbld.Append("</table>" + System.Environment.NewLine);

                    element = sbld.ToString();
                    trigger_value = lvalue;
                    trigger_operator = loperator;
                    break;
            }
            if (error)
            {
                template = template.Replace("[errormessage]", "<span class=\"error\">" + errormessage + "</span>");
            }
            else
            {
                template = template.Replace("[errormessage]", "");
            }
            if (required)
            {
                //SDF form markup...
                template = template.Replace("question", "question required");
                template = template.Replace("[*required*]", "<i class=\"icon-star required\" title=\"Required\"></i>");
            }
            else
            {
                template = template.Replace("[*required*]", "");
            }

            if (parent.Length > 0)
            {
                template = template.Replace("[parent]", " " + parent + "_child hide");
            }
            else
            {
                template = template.Replace("[parent]", "");
            }

            template = template.Replace("[elementid]", elementid);
            template = template.Replace("[element]", element);
            template = template.Replace("[label]", label);
            template = template.Replace("[subtitle]", subtitle);
            template = template.Replace("[help]", helptext);

            template = insertClasses(template);
            if (image.Length > 0 && type != "imageselect")
            {
                template = "<tr><td colspan=\"2\" class=\"referenceimage\"><img class =\"popimage\" src=\"" + image + "\"/></td</tr>" + System.Environment.NewLine + template;
            }

            return template;

        }


        private string createElement(string htmltype, string elementtype = "", string elementid = "", string controlclass = "", string fieldoverride = "")
        {
            string element = "";
            if (!elementid.Trim().StartsWith("id="))
            {
                elementid = " id=\"" + elementid + "\"";
            }
            string eclass = (controlclass.Trim() + " " + elementclass).Trim();
            if (fieldoverride.Length == 0) { fieldoverride = field; }
            element = "<" + htmltype
                        + makeAttribute("name", fieldoverride)
                        + elementid + makeClass(eclass)
                        + makeAttribute("value", GetValue(fieldoverride))
                        + makeAttribute("min", min)
                        + makeAttribute("max", max)
                        + makeAttribute("step", step)
                        + makeAttribute("maxlength", length.ToString())
                        + makeAttribute("data-value", trigger_value)
                        + makeAttribute("data-operator", trigger_operator)
                        + makeAttribute("data-formula", formula);

            if (isreadonly) { element += makeAttribute("readonly", "readonly"); }

            if (elementtype.Length > 0)
            {
                element += makeAttribute("type", elementtype);
            }
            if (attributes.Length > 0)
            {
                element += " " + attributes;
            }
            element += makeFlag("required", required) + makeFlag("disabled", disabled) + "/>";
            if (units.Length > 0)
            {
                element += "<span class=\"units\">" + units + "</span>";
            }
            return element;
        }

        private string createSelect(string elementid, bool isMulti = false, string fieldoverride = "", string listoverride = "")
        {
            string element = "";
            if (!elementid.Trim().StartsWith("id="))
            {
                elementid = " id=\"" + elementid + "\"";
            }
            if (fieldoverride.Length == 0) { fieldoverride = field; }
            if (listoverride.Length == 0) { listoverride = list; }

            element = System.Environment.NewLine;
            element += "\t\t<select name=\"" + fieldoverride + "\"" + elementid + "[elementclass]";
            element += makeFlag("required", required) + makeFlag("disabled", disabled)
                + makeAttribute("data-value", trigger_value)
                + makeAttribute("data-operator", trigger_operator);
            if (isMulti)
            {
                element += makeFlag("multiple", true);
            }
            if (rows > 0) { element += makeAttribute("size", rows.ToString()); }
            if (attributes.Length > 0) { element += " " + attributes; }
            element += ">" + System.Environment.NewLine;
            element += System.Environment.NewLine + OptionsFromList(listoverride, GetValue(fieldoverride)) + System.Environment.NewLine + "\t\t</select>";

            if (units.Length > 0)
            {
                element += "<span class=\"units\">" + units + "</span>";
            }

            return element;
        }

        private string GetValue(string fieldname = "")
        {
            if (fieldname.IsNullOrEmpty()) { fieldname = field; }
            if (formvalues.ContainsKey(fieldname))
            {
                if (formvalues[fieldname].IsNullOrEmpty())
                {
                    return "";
                }
                else
                {
                    return formvalues[fieldname];
                }
            }
            else
            {
                return value;
            }
        }

        private string createImageSelect()
        {
            string template = "";
            Tools.AppendLine(ref template, "\t<tr>");
            Tools.AppendLine(ref template, "\t\t<td class=\"question clean\" colspan=\"2\">");
            Tools.AppendLine(ref template, "\t\t\t<div class=\"imageselect\">");
            Tools.AppendLine(ref template, "\t\t\t\t<ul>");
            Tools.AppendLine(ref template, "{imagemarkup}");
            Tools.AppendLine(ref template, "\t\t\t\t</ul>");
            Tools.AppendLine(ref template, "\t\t\t</div>");
            Tools.AppendLine(ref template, "\t\t\t<div class=\"selector\">");
            Tools.AppendLine(ref template, "\t\t\t\t<label>[label]</label>");
            Tools.AppendLine(ref template, "[*required*][element]<span>[subtitle]</span>");
            Tools.AppendLine(ref template, "\t\t\t</div>");
            Tools.AppendLine(ref template, "\t\t</td>");
            Tools.AppendLine(ref template, "\t</tr>");

            string imgmarkup = "";
            string[] az = image.Split(',');
            int wc = Tools.WordCount(list);
            if (az.Length < wc)
            {
                return "Not enough images specified to mark up this control (" + name + ")";
            }
            for (int i = 0; i < wc; i++)
            {
                Tools.AppendLine(ref imgmarkup, "<li><img src=\"" + ImagePath + az[i] + "\" value=\"" + Tools.ValueOf(i + 1, list) + "\" /></li>");
            }
            template = template.Replace("{imagemarkup}", imgmarkup);
            appendElementClass("imageselect");
            return template;
        }
        //validates an external string value against the current elements constraints
    public bool validateString(string val, out string msg, bool wasVisible = true, string subelement = null)
        {
            //refactored because it went off the trail
            bool OK = true;
            msg = string.Empty;

            if (val.IsNullOrEmpty())
            {
                if (this.required && wasVisible)
                {
                    msg = "missing required value";
                    OK = false;
                }
                else
                    return true;
            }

            if (!string.IsNullOrEmpty(this.validation_pattern)
                && ((Tools.RegexPatterns.ContainsKey(validation_pattern) && !Regex.Match(val, Tools.RegexPatterns[this.validation_pattern]).Success)
                || (!Tools.RegexPatterns.ContainsKey(validation_pattern) && !Regex.Match(val, this.validation_pattern).Success)))
            {
                msg = "does not match expected format.";
                OK = false;
            }

            var t = this.type.ToLower();

            if (t == "compound" && !String.IsNullOrWhiteSpace(subelement))
            {
                for (int i = 0; i < this.elementcodes.Length; i++)
                {
                    if (this.elementcodes[i] == subelement)
                    {
                        var typesarr = this.types.Split(',');

                        if (typesarr.Length > i)
                            t = typesarr[i].ToLower();
                    }
                }
            }

            var strIntTypes = new string[] { "integer", "int", "number", "positiveinteger", "positiveint", "signedint" };
            var strFloatTypes = new string[] { "float", "signedfloat" };
            var strDateTypes = new string[] { "date", "datetime", "pastdate" };

            if (this.length.HasValue && val.Length > this.length.Value)
            {
                msg = "value exceeds maximum allowable length";
                OK = false;
            }

            if (strIntTypes.Contains(t))
            {
                int tmp;
                if (!int.TryParse(val, out tmp))
                {
                    msg = "invalid integer value";
                    OK = false;
                }
                int min = 0;
                int max = 0;
                if (!string.IsNullOrEmpty(this.min) && int.TryParse(this.min, out min))
                {
                    if (tmp < min)
                    {
                        msg = "integer below minimum value";
                        OK = false;
                    }
                }
                if (!string.IsNullOrEmpty(this.max) && int.TryParse(this.max, out max))
                {
                    if (tmp > max)
                    {
                        msg = "integer above maximum value";
                        OK = false;
                    }
                }
            }
            else if (strFloatTypes.Contains(t))
            {
                float tmp;
                if (!float.TryParse(val, out tmp))
                {
                    msg = "invalid float value";
                    OK = false;
                }
                float min = 0;
                float max = 0;
                if (!string.IsNullOrEmpty(this.min) && float.TryParse(this.min, out min))
                {
                    if (tmp < min)
                    {
                        msg = "float below minimum value";
                        OK = false;
                    }
                }
                if (!string.IsNullOrEmpty(this.max) && float.TryParse(this.max, out max))
                {
                    if (tmp > max)
                    {
                        msg = "float above maximum value";
                        OK = false;
                    }
                }
            }
            else if (strDateTypes.Contains(t))
            {
                DateTime tmp;
                if (!DateTime.TryParse(val, out tmp))
                {
                    msg = "invalid DateTime value";
                    OK = false;
                }
                DateTime min;
                DateTime max;
                if (!string.IsNullOrEmpty(this.min) && DateTime.TryParse(this.min, out min))
                {
                    if (tmp < min)
                    {
                        msg = "DateTime below minimum value";
                        OK = false;
                    }
                }
                if (!string.IsNullOrEmpty(this.max) && DateTime.TryParse(this.max, out max))
                {
                    if (tmp > max)
                    {
                        msg = "DateTime above maximum value";
                        OK = false;
                    }
                }

                if (t == "pastdate")
                    if (tmp > DateTime.Now)
                    {
                        msg = "Date cannot be in the future.";
                        OK = false;
                    }
            }
            //set the values of error and errormessage
            error = !OK;
            errormessage = msg;
            return OK;
        }

        private string resolveElement(string element, ref string instanceclass)
        {
            string results = "input";
            switch (element.ToLower())
            {
                case "checkbox":
                    _type = "checkbox";
                    instanceclass = "singlecheck";
                    break;
                case "check":
                case "checklist":
                    _type = "checkbox";
                    instanceclass += "ckbox";
                    break;
                case "date":
                    if (!modern) { _type = "text"; } else { _type = "date"; }
                    instanceclass += "date";
                    break;
                case "datetime":
                    if (!modern) { _type = "text"; } else { _type = "date"; }
                    instanceclass += "datetime";
                    break;
                case "integer":
                case "int":
                case "number":
                    if (!modern) { _type = "text"; } else { _type = "number"; }
                    if (signed)
                        instanceclass += "num sint";
                    else
                        instanceclass += "num int";
                    break;

                case "positiveinteger":
                case "positiveint":
                    if (!modern) { _type = "text"; } else { _type = "number"; }
                    instanceclass += "num int";
                    break;
                case "signedint":
                    if (!modern) { _type = "text"; } else { _type = "number"; }
                    instanceclass += "num sint";
                    break;

                case "float":
                case "signedfloat":
                    if (!modern) { _type = "text"; } else { _type = "number"; }
                    if (signed || element.ToLower() == "signedfloat")
                        instanceclass += "num sfloat";
                    else
                        instanceclass += "num float";
                    break;

                case "select":
                    results = "select";
                    _type = "";
                    break;


                default:
                    break;
            }

            return results;

        }
        private string Swap(string source, string tag, string value)
        {
            return source.Replace("[" + tag.ToLower() + "]", value);
        }

        /// <summary>
        /// Renders class elements into the template
        /// </summary>
        /// <param name="template">The assembled string so far</param>
        /// <returns>string</returns>
        private string insertClasses(string template)
        {

            template = template.Replace("[wrapperclass]", makeClass(wrapperclass));
            template = template.Replace("[titleclass]", makeClass(titleclass));
            template = template.Replace("[labelclass]", makeClass(labelclass));
            template = template.Replace("[elementclass]", makeClass(elementclass));

            return template;
        }
        private void appendElementClass(string className)
        {
            if (elementclass != "" || className != "")
            {
                elementclass += " " + className;
            }
        }

        private string makeAttribute(string attribute, string attributevalue)
        {
            if (attributevalue != "")
            {
                return " " + attribute + "=\"" + attributevalue + "\"";
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Sets an attribute flag based on a boolean property. For example, selected="selected" or disabled="disabled"
        /// </summary>
        /// <param name="attribute">Name of the attribute</param>
        /// <param name="attributeflag">True of False.  If false, nothing is returned.</param>
        /// <returns>String.  Empty string in the case of a false attributeflag</returns>
        private string makeFlag(string attribute, bool attributeflag)
        {
            if (attributeflag)
            {
                if (attribute == "required" && !UseRequiredAttribute)
                    return "";
                else
                    return " " + attribute + "=\"" + attribute + "\"";
            }
            else
            {
                return "";
            }
        }
        private string makeClass(string classname)
        {
            string classstring = "";
            string errorclass = "";
            if (error)
            {
                errorclass = " error";
            }

            classstring = classname + errorclass;
            if (classstring.Trim().Length == 0)
            {
                return "";
            }
            else
            {
                return " class=\"" + classstring.Trim() + "\"";
            }

        }


        public string CheckRow(string name, string id, string list, string selectedvalue = "", bool isRadio = false)
        {

            if (selectedvalue.IsNullOrEmpty()) { selectedvalue = GetValue(name); }


            StringBuilder sb = new StringBuilder();
            string results = "";
            string key = "";
            string value = "";
            string label = "";
            string idstring = "";
            string group = "";          //perhaps used to target columns or sub groups??
            string itemclass = "singlesel";
            char c = '|';
            bool opt = false;
            bool q = false;
            int lc = 0;
            int gc = 0;

            string elclass = "";

            escapeList(ref list);
            string[] aM = list.Split(',');
            bool firstDT = true;
            foreach (string item in aM)
            {
                lc += 1;
                gc += 1;
                elclass = "";

                q = KeyBreak(item, out key, out value);
                opt = KeyBreak(key, out group, out key, c);

                restore(ref key);
                restore(ref value);
                restore(ref group);

                idstring = id + "_" + lc.ToString();
                label = "";
                idstring = " id=\"" + idstring + "\"";

                sb.AppendLine("\t\t\t<td class=\"center\">");
                if (InList(key, selectedvalue))
                {

                    sb.Append("\t\t\t\t<input type=\"checkbox\" name=\"" + name + "\"" + idstring + " checked=\"checked\" value=\"" + key + "\" ");
                    if (trigger)
                    {
                        elclass += " trigger";
                        if (firstDT)
                        {
                            firstDT = false;
                            //sb.Append(makeAttribute("data-value", trigger_value) + makeAttribute("data-operator", trigger_operator));
                            sb.AppendFormat(" data-value=\"{0}\" data-operator=\"{1}\" ", trigger_value, trigger_operator);
                        }
                    }
                }
                else
                {
                    sb.Append("\t\t\t\t<input type=\"checkbox\" name=\"" + name + "\"" + idstring + " value=\"" + key + "\" ");
                    if (trigger)
                    {
                        elclass += " trigger";
                        if (firstDT)
                        {
                            firstDT = false;
                            //sb.Append(makeAttribute("data-value", trigger_value) + makeAttribute("data-operator", trigger_operator));
                            sb.AppendFormat(" data-value=\"{0}\" data-operator=\"{1}\" ", trigger_value, trigger_operator);
                        }
                    }
                }

                sb.Append(makeClass(elclass.Trim() + " " + itemclass));
                sb.Append(" />" + label + "" + System.Environment.NewLine);
                sb.AppendLine("\t\t\t</td>");
            }

            results = sb.ToString();

            if (isRadio)
            {
                results = results.Replace("type=\"checkbox\"", "type=\"radio\"");
            }
            return results;
        }


        public string CheckboxesFromList(string name, string id, string list, string selectedvalue = "", bool isRadio = false, string singleselect = "")
        {

            if (selectedvalue.IsNullOrEmpty()) { selectedvalue = GetValue(); }
            //features - break the list into columns??
            //each column is marked up in a DIV with a class of column%%
            //  1       = 100
            //  2       = 50
            //  3       = 33
            //  4       = 25
            //  5       = 20
            //  6       = 16
            //  7       = 14
            //  8       = 12
            //  9       = 11
            //  10      = 10   etc.  int(100/columns).tostring() = column name

            int columns = Convert.ToInt32(cols);
            if (columns < 1) { columns = 1; }
            if (columns > 8) { columns = 8; }
            StringBuilder sb = new StringBuilder();
            string results = "";
            string key = "";
            string value = "";
            string label = "";
            string idstring = "";
            string group = "";          //perhaps used to target columns or sub groups??
            char c = '|';
            bool opt = false;
            bool q = false;
            int lc = 0;
            int gc = 0;
            int offset = 0;
            string currentgroup = "";
            if (columns < 1) { columns = 1; }
            string colclass = Convert.ToInt32(100 / columns).ToString();
            string elclass = "";

            escapeList(ref list);
            string[] aM = list.Split(',');
            offset = Convert.ToInt32((aM.Length + 1) / columns);
            bool firstDT = true;
            foreach (string item in aM)
            {
                lc += 1;
                gc += 1;
                elclass = "";
                if (gc > offset)
                {
                    results += "<div class=\"checklist column" + colclass + " " + parent + "_child"
                            + "\">"
                            + System.Environment.NewLine
                            + sb.ToString()
                            + System.Environment.NewLine
                            + "</div>" + System.Environment.NewLine;

                    sb.Length = 0;
                    gc = 1;
                }
                q = KeyBreak(item, out key, out value);
                opt = KeyBreak(key, out group, out key, c);

                restore(ref key);
                restore(ref value);
                restore(ref group);

                idstring = id + "_" + lc.ToString();
                label = "<label for=\"" + idstring + "\">" + value + "</label>";
                idstring = " id=\"" + idstring + "\"";

                if (opt && group != currentgroup)
                {
                    // currently no support for 3D lists with check box grouping
                }

                if (InList(key, selectedvalue))
                {
                    //sb.Append("<input type=\"checkbox\" name=\"" + name + "\"" + idstring + " checked=\"checked\" value=\"" + key + "\"/>" + label + "" + System.Environment.NewLine);
                    sb.Append("<input type=\"checkbox\" name=\"" + name + "\"" + idstring + " checked=\"checked\" value=\"" + key + "\" ");
                    if (trigger)
                    {
                        elclass += " trigger";
                        if (firstDT)
                        {
                            firstDT = false;
                            //sb.Append(makeAttribute("data-value", trigger_value) + makeAttribute("data-operator", trigger_operator));
                            sb.AppendFormat(" data-value=\"{0}\" data-operator=\"{1}\" ", trigger_value, trigger_operator);
                        }
                    }
                }
                else
                {
                    sb.Append("<input type=\"checkbox\" name=\"" + name + "\"" + idstring + " value=\"" + key + "\" ");
                    if (trigger)
                    {
                        elclass += " trigger";
                        if (firstDT)
                        {
                            firstDT = false;
                            //sb.Append(makeAttribute("data-value", trigger_value) + makeAttribute("data-operator", trigger_operator));
                            sb.AppendFormat(" data-value=\"{0}\" data-operator=\"{1}\" ", trigger_value, trigger_operator);
                        }
                    }
                }
                if (!singleselect.IsNullOrEmpty()) { elclass += " " + singleselect; }
                sb.Append(makeClass(elclass.Trim()));
                sb.Append(" />" + label + "" + System.Environment.NewLine);
                sb.Append("<br/>");

            }

            results += "<div class=\"checklist column" + colclass + " " + parent + "_child"
                    + "\">"
                    + System.Environment.NewLine
                    + sb.ToString()
                    + System.Environment.NewLine
                    + "</div>" + System.Environment.NewLine;

            if (isRadio)
            {
                results = results.Replace("type=\"checkbox\"", "type=\"radio\"");
            }
            return results;
        }

        public static string SelectOptions(string list, string selectedvalue = "", bool Multi = false)
        {
            Element e = new Element();
            string results = e.OptionsFromList(list, selectedvalue, Multi);
            return results;
        }

        /// <summary>
        /// Returns the supplied list as HTML options.
        /// </summary>
        /// <param name="list">A one, two, or three dimensional list delimited by comma, colon, and pipe.  Pipe delimited values represent the option group.  Only the first value in the group need specify the group.</param>
        /// <param name="selectedvalue">The value that is selected</param>
        /// <returns>String</returns>
        public string OptionsFromList(string list, string selectedvalue = "", bool Multi = false)
        {

            if (selectedvalue.IsNullOrEmpty()) { selectedvalue = GetValue(); }
            if (selectedvalue == null) { selectedvalue = ""; }
            //list is a single, double, or triple percision list
            //primary is ,
            //secondary is :
            //finally, |


            //if the first value is not empty we should add an empty selection
            //since no lists have this included right now, let's just add one blind
            //TODO: fix this for better list handling later
            list = ":" + listselector + "," + list;

            StringBuilder sb = new StringBuilder();
            string results = "";
            string key = "";
            string value = "";
            string group = "";
            char c = '|';
            bool opt = false;
            bool q = false;
            string currentgroup = "";

            if (Multi) { escapeList(ref selectedvalue); };
            string[] avals = selectedvalue.Split(',');

            escapeList(ref list);
            string[] aM = list.Split(',');
            foreach (string item in aM)
            {
                q = KeyBreak(item, out key, out value);
                opt = KeyBreak(key, out group, out key, c);

                restore(ref key);
                restore(ref value);
                restore(ref group);

                if (opt && group != currentgroup)
                {
                    if (currentgroup != "")
                    {
                        sb.Append("\t\t</optgroup>" + System.Environment.NewLine);
                    }
                    currentgroup = group;
                    sb.Append("\t\t<optgroup label=\"" + currentgroup + "\">" + System.Environment.NewLine);
                }

                if (InList(key, selectedvalue))
                {
                    sb.Append("\t\t\t<option selected=\"selected\" value=\"" + key + "\">" + value + "</option>" + System.Environment.NewLine);
                }
                else
                {
                    sb.Append("\t\t\t<option value=\"" + key + "\">" + value + "</option>" + System.Environment.NewLine);
                }
            }

            //close out the current group if groups are set...
            if (currentgroup != "")
            {
                sb.Append("\t\t</optgroup>" + System.Environment.NewLine);
            }


            results = sb.ToString();

            return results;
        }

        public bool KeyBreak(string item, out string key, out string value, char splitCharacter = ':')
        {
            //splits on :

            string[] aM = item.Split(splitCharacter);
            if (aM.Length > 1)
            {
                key = aM[0];
                value = aM[1];
                return true;
            }
            else
            {
                key = item;
                value = item;
                return false;
            }

        }

        public static void escapeList(ref string list)
        {
            //escape list characters used for delimiters using html entities...
            //unfortunately the traditional backslash gives us trouble because C#
            //tries to escape the escape, so here we use the forward slash...
            list = list.Replace("/,", "&#44;");
            list = list.Replace("/:", "&#58;");
            list = list.Replace("/|", "&#124;");

        }

        public static void restore(ref string list)
        {
            //escape list characters used for delimiters using html entities...
            list = list.Replace("&#44;", ",");
            list = list.Replace("&#58;", ":");
            list = list.Replace("&#124;", "|");

        }

        public bool InList(string value, string list)
        {
            if (!list.IsNullOrEmpty() && !value.IsNullOrEmpty())
            {
                string _list = list;
                escapeList(ref _list);
                string[] aM = _list.Split(',');

                foreach (string element in aM)
                {
                    if (element.ToLower() == value.ToLower())
                    {
                        return true;
                    }
                }
            }
            return false;

        }

        public bool InList(string value, Array list)
        {
            Array _list = list;
            foreach (string element in _list)
            {
                if (element.ToLower() == value.ToLower())
                {
                    return true;
                }
            }
            return false;

        }


    }

}
