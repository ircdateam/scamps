﻿using System;
using System.IO;
using System.Linq;
using System.Data;
using System.Collections.Generic;

namespace Scamps
{
    public class Form
    {
        public string errors = "";
        public bool haserrors = false;
        public string messages = "";
        public bool hasmessages = false;
        public string ConnectionString = "";
        public string Title = "";
        public string MajorVersion = "";
        public string MinorVersion = "";
        public string Scamp = "";
        public string Precedence = "";
        public string EncounterType = "";
        public string Comments = "";
        public string Entity = "";
        public string Action = "";
        public string Method = "post";
        public string ClassName = "";
        public string Attributes = "";
        public string ID = "";
        public string Evaluator = "";
        public string FormReferenceElementName = "_formreference";
        public string FormReference = "";
        public string Language = "";
        public bool HasFlaggedFields = false;
        public bool FilteredLists = false;
        //pass through templates and settings...
        public string ElementTemplate = "";
        public string PageTemplate = "";

        public Dictionary<string, string> FormStub = new Dictionary<string, string>();
        public Dictionary<string, string> FormData = new Dictionary<string, string>();

        public OptionsList Options = new OptionsList();

        //the form class is the top level class for reading and rendering forms
        public Dictionary<string, Panel> theform = new Dictionary<string, Panel>();
        public string Version = "";         // optional string version identifier matching only non-versioned and 
        // matching version elements.  Can be comma separated list.

        private string _provider = "sql";
        private string _imagepath = "";
        private string bitsofbits = "";
        private string _autocomplete = "off";

        public string localxml = "";        // this is required since the code got refactored and reading the xml was
                                            // pulled out of processing the xml.  I need to read it one place and process
                                            // in another.  Making it public so that perhaps sometime this will be a decent 
                                            // hook to allow more flexiblity in adding dynamic panels.
        public string Name { get { return FormInfo.Name; } }
        public string Success_Action { get { return FormInfo.Success_Action; } }
        public string Success_Identifier { get { return FormInfo.Success_Identifier; } }
        public string Error_Action { get { return FormInfo.Error_Action; } }
        public string Error_Identifier { get { return FormInfo.Error_Identifier; } }
        public string Target { get { return FormInfo.Target; } }
        public string Handler { get { return FormInfo.Handler; } }
        //public string Entity { get { return FormInfo.Entity; } }
        public string URL { get { return FormInfo.URL; } }
        public int FormOptions { get { return FormInfo.Options; } }

        public string ImagePath
        {
            set
            {
                _imagepath = value.ToLower();
                Tools.EndWith(_imagepath, @"/");
            }

        }

        public string AutoComplete
        {
            set
            {
                _autocomplete = value.ToLower();
                if (_autocomplete != "on" || _autocomplete != "off") { _autocomplete = "off"; }
            }
        }
        public string Provider
        {
            get { return _provider; }
            set
            {
                //TODO: make sure it is a supported provider, spelled correctly, etc.

                switch (value.ToLower())
                {
                    case "oracle.manageddataaccess.client":
                    case "oracle":
                    case "ora":
                    case "o":
                        _provider = "oracle";
                        break;

                    case "oledb":
                    case "ole":
                        _provider = "oledb";
                        break;
                    default:
                        _provider = "sql";
                        break;
                }
            }
        }

        public int ElementCount
        {
            get
            {
                int c = 0;
                foreach (KeyValuePair<string, Panel> panel in theform)
                {
                    c += panel.Value.Count;
                }
                return c;
            }
        }

        public int PanelCount
        {
            get
            {
                return theform.Count;
            }
        }
        /// <summary>
        /// BasePath is the absolute path of the root directory of the web applicaiton.  Setting base path also sets
        /// TemplatePath and FormPath as relative to BasePath in the resources/ directory.  If templates and forms are to 
        /// be found in another location, set these properties after setting BathPath.
        /// </summary>
        public string BasePath
        {
            get
            {
                return _basepath;
            }
            set
            {
                if (!value.EndsWith("\\"))
                {
                    value = value + "\\";
                }
                if (Directory.Exists(value))
                {
                    _basepath = value;
                    _templatepath = _basepath + "resources\\templates\\";
                    _formpath = _basepath + "resources\\forms\\";
                }
                else
                {
                    Error("BasePath.Directory:" + value + " does not exist.");
                }

            }
        }

        public string TemplatePath
        {
            get
            {
                return _templatepath;
            }
            set
            {
                if (!value.EndsWith("\\"))
                {
                    value = value + "\\";
                }
                if (Directory.Exists(value))
                {
                    _templatepath = value;
                }
                else
                {
                    Error("TemplatePath.Directory:" + value + " does not exist.");
                }

            }
        }

        /// <summary>
        /// The salt is a randon string of characters created when a form is rendered.  It is embedded in the form meta and used 
        /// to determine if a form has been tampered with.
        /// </summary>
        public string Salt
        {
            get { return _salt; }
        }

        public string FormPath
        {
            get
            {
                return _formpath;
            }
            set
            {
                if (!value.EndsWith("\\"))
                {
                    value = value + "\\";
                }
                if (Directory.Exists(value))
                {
                    _formpath = value;
                }
                else
                {
                    Error("FormPath.Directory:" + value + " does not exist.");
                }

            }
        }

        public class OptionsList
        {
            public bool UseHTML5Markup = true;
            public bool InnerFormOnly = false;       //return form markup without wrapping form tag/
            public bool TableMarkup = true;
            public bool PanelMarkup = false;
            public bool VersionWithHiddenElements = false;
        }

        public class FormInfo
        {
            public static string Name = "";
            public static string Success_Action = "";
            public static string Success_Identifier = "";
            public static string Error_Action = "";
            public static string Error_Identifier = "";
            public static string Target = "";
            public static string Handler = "";
            public static string Entity = "";
            public static string URL = "";
            public static int Options = 0;          //bitwise options flag
        }


        private string _templatepath = "";
        private string _formpath = "";
        private string _basepath = "";
        private string _salt = "";
        private bool _formloaded = false;
        private bool _haserrors = false;
        private long _RecordID = 0;

        /// <summary>
        /// Resets the object
        /// </summary>
        public void Clear()
        {
            errors = "";
            haserrors = false;

            messages = "";
            hasmessages = false;
        }
        public bool IsValid
        {
            get
            {
                return !_haserrors;
            }
        }

        private void Error(string message)
        {
            haserrors = true;
            errors += message + System.Environment.NewLine;
        }

        private void Message(string message)
        {
            hasmessages = true;
            messages += message + System.Environment.NewLine;
        }


        /// <summary>
        /// Load a form given an appointment id
        /// </summary>
        /// <param name="appt">appointment id of encounter</param>
        /// <returns>success of loading appointment</returns>
        public bool LoadAppointmentForm(long appt, long? subunit = null)
        {
            const string _querySubUnitFormDataByApptId = "select to_char(e.appt_id) as appt_id, e.har, e.encounter_cd as visit_type, to_char(en.scamp_id) as scamp_id, en.form_name, to_char(v.id) as sub_id, to_char(v.encounter_status) as encounter_status, v.seq_val as subunit_seq_val from pt_encounters e join visits_inpt_subunit v on e.har = v.har join admin_encounters en on en.encounter_id = v.encounter_type where e.APPT_ID = {0} and v.id={1}";
            const string _queryFormDataByApptId = "select to_char(e.appt_id) as appt_id, e.har, e.encounter_cd as visit_type, to_char(e.encounter_status) as encounter_status, to_char(en.scamp_id) as scamp_id, en.form_name from admin_encounters en join pt_encounters e on en.encounter_id = e.encounter_type where e.APPT_ID = {0}";
            string res;
            var stubitems = new string[] { "appt_id", "har", "visit_type", "scamp_id", "sub_id", "subunit_seq_val", "encounter_status" };
            try
            {
                var db = new DataTools();
                db.ConnectionString = this.ConnectionString;
                db.Provider = this._provider;
                db.OpenConnection();
                if (subunit.HasValue)
                    db.GetResultSet(string.Format(_querySubUnitFormDataByApptId, appt, subunit.Value));
                else
                    db.GetResultSet(string.Format(_queryFormDataByApptId, appt));

                var dict = db.GetRow();
                res = dict["form_name"].ToString();
                stubitems.ToList().ForEach(st => this.FormStub[st] = dict.ContainsKey(st) ? dict[st].ToString() : "");

                //I think this is what you are looking for?
                //db.Clear();
                //db = null;
                db.Dispose();
                Load(res);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("failed to load form by appointment: {0}", e);
                return false;
            }
            return true;

        }

        public string LoadFormXml(string formname)
        {
            string outvar;
            return LoadFormXml(formname, out outvar);
        }

        public string LoadFormXml(string formname, out string formpath)
        {
            string formxml = "";
            string fn = formname.Trim();
            formpath = "";
            string localform = Tools.FileName(fn);
            int i = localform.IndexOf('.');
            if (i > -1) { localform = localform.Substring(0, i); }
            localform += ".form";
            HasFlaggedFields = false;

            //===read in the file specified by formname and form path
            //===the form could be passed in as the full xml string...
            formxml = Tools.XNode(ref formname, "form", true);
            if (formxml.IsNullOrEmpty())
            {
                if (!fn.EndsWith(".form"))
                {
                    fn = fn + ".form";
                }
                // check if the passed reference is a fully qualified file name
                if (Tools.FileExists(fn))
                {
                    formxml = Tools.ReadFile(fn);
                    formpath = fn;
                    // figure out the file path and name of any possible local file...
                    string tmppath = Path.GetDirectoryName(fn);
                    fn = Path.GetFileName(fn);
                    if (Tools.FileExists(tmppath + "\\local." + localform))
                    {
                        localxml = Tools.ReadFile(tmppath + "\\local." + localform);
                    }
                }
                else
                {
                    if (Tools.FileExists(_formpath + fn))
                    {
                        formxml = Tools.ReadFile(_formpath + fn);
                        formpath = _formpath + fn;
                        if (Tools.FileExists(_formpath + "local." + localform))
                        {
                            localxml = Tools.ReadFile(_formpath + "local." + localform);
                        }
                    }
                    else
                    {
                        //get from the database or ???
                        var newFilename = string.Empty;
                        var apptid = string.Empty;
                        var prov = this.Provider;
                        if (prov.Equals("oracle", StringComparison.OrdinalIgnoreCase))
                            prov = "Oracle.DataAccess.Client";

                        if (this.FormStub.ContainsKey("sub_id") && !string.IsNullOrEmpty(this.FormStub["sub_id"]) && !string.IsNullOrEmpty((apptid = string.Concat("0\0", this.FormStub["sub_id"]))))
                        {
                            bool exported = false;
                            exported = LegacyExport.exportSCAMPFormDBtoXML(this.ConnectionString, prov, apptid, _formpath, out newFilename);
                            if (exported)
                            {
                                formxml = Tools.ReadFile(_formpath + newFilename);
                                formpath = _formpath + newFilename;
                            }
                            else
                                throw new Exception("unable to export legacy form via subunit id '" + this.FormStub["sub_id"] + "'");
                        }
                        else if (this.FormStub.ContainsKey("appt_id") && !string.IsNullOrEmpty((apptid = this.FormStub["appt_id"])))
                        {
                            bool exported = false;
                            exported = LegacyExport.exportSCAMPFormDBtoXML(this.ConnectionString, prov, apptid, _formpath, out newFilename);
                            if (exported)
                            {
                                formxml = Tools.ReadFile(_formpath + newFilename);
                                formpath = _formpath + newFilename;
                            }
                            else
                                throw new Exception("unable to export legacy form via appt_id '" + apptid + "'");
                        }
                        else
                            throw new Exception("unable to export legacy form '" + fn + "'");
                        //Console.WriteLine("Unable to export form '{0}'", fn);
                    }
                }
            }

            return formxml;
        }

        /// <summary>
        /// Load a form by name
        /// </summary>
        /// <param name="formname">Name of the form to load</param>
        public void Load(string formname)
        {
            string formxml = "";
            string fn = formname.Trim();
            string elements = "";
            string tmp = "";
            string formpath = "";
            HasFlaggedFields = false;

            formxml = LoadFormXml(formname, out formpath);

            if (formxml.Length > 0)
            {
                int len = 0;
                string eversion = "";

                //remove comment blocks.  This is a bit expensive, so maybe sleep elements instead
                formxml = Tools.ReplaceRange(formxml, "<!--", "-->", "");

                //process the form into object collections...
                //first, get the header info...
                string info = Tools.XNode(ref formxml, "info");
                if (info.Length == 0)
                {
                    int n = formxml.IndexOf("<panel");
                    if (n > 0)
                    {
                        info = formxml.Substring(0, n);
                    }
                }
                // child nodes...
                string success = Tools.XNode(ref info, "success").Trim();
                FormInfo.Success_Action = Tools.XNode(ref success, "action").Trim();
                FormInfo.Success_Identifier = Tools.XNode(ref success, "identifier").Trim();
                success = Tools.XNode(ref info, "error").Trim();
                FormInfo.Error_Action = Tools.XNode(ref success, "action").Trim();
                FormInfo.Error_Identifier = Tools.XNode(ref success, "identifier").Trim();
                FormInfo.Target = Tools.XNode(ref info, "target").Trim();
                if (Entity.Length > 0)
                {
                    FormInfo.Entity = Entity;
                }
                else
                {
                    FormInfo.Entity = Tools.XNode(ref info, "entity").Trim();
                }

                if (Action.Length == 0) { Action = Tools.XNode(ref info, "action").Trim(); }
                if (Method.Length == 0) { Method = Tools.XNode(ref info, "method").Trim(); }

                FormInfo.Name = Tools.XNode(ref info, "name").Trim();
                FormInfo.Handler = Tools.XNode(ref info, "handler").Trim();
                FormInfo.URL = Tools.XNode(ref info, "url").Trim();
                FormInfo.Options = Tools.AsInteger(Tools.XNode(ref info, "options").Trim());
                tmp = Tools.XNode(ref info, "autocomplete").Trim().ToLower();
                if (tmp == "on" || tmp == "off") { _autocomplete = tmp; }

                Title = Tools.XNode(ref info, "title").Trim();
                MajorVersion = Tools.XNode(ref info, "major").Trim();
                if (MajorVersion.IsNullOrEmpty()) { MajorVersion = Tools.XNode(ref info, "majorversion").Trim(); }
                MinorVersion = Tools.XNode(ref info, "minor").Trim();
                if (MinorVersion.IsNullOrEmpty()) { MinorVersion = Tools.XNode(ref info, "minorversion").Trim(); }
                Scamp = Tools.XNode(ref info, "scamp").Trim();
                Precedence = Tools.XNode(ref info, "precedence").Trim();
                EncounterType = Tools.XNode(ref info, "encountertype").Trim();
                Comments = Tools.XNode(ref info, "comments").Trim();

                tmp = Tools.XNode(ref info, "panelmarkup").Trim();
                if (tmp.Length > 0) { Options.PanelMarkup = Tools.AsBoolean(tmp); }

                tmp = Tools.XNode(ref info, "class").Trim();
                if (tmp.Length > 0) { ClassName = tmp; }

                tmp = Tools.XNode(ref info, "formid").Trim();
                if (tmp.Length > 0) { ID = tmp; }

                string panelxml = Tools.XNode(ref formxml, "panels").Trim();
                if (panelxml.Length == 0) { panelxml = formxml; }

                // local form additions insert.
                panelxml = localxml + panelxml;

                string[] panels = panelxml.Split(new string[] { "<panel>" }, StringSplitOptions.None);

                Panel Panel = new Panel();
                Element element = new Element();

                string theelement = "";
                for (int p = 0; p < panels.Length; p++)
                {
                    if (panels[p].Trim().Length == 0)
                    {
                        continue;
                    }

                    elements = Tools.XNode(ref panels[p], "elements").Trim();
                    if (elements.Length == 0)
                    {
                        elements = Tools.StringPart(panels[p], "<element", "</panel>").Trim();
                    }
                    if (elements.Length == 0)
                    {
                        continue;
                    }

                    //=== each panel ===
                    Panel = new Panel();
                    Panel.PanelMarkup = Options.PanelMarkup;
                    Panel.TableMarkup = Options.TableMarkup;

                    Panel.Title = Tools.XNode(ref panels[p], "title").Trim();
                    if (Panel.Title.IsNullOrEmpty()) { Panel.Title = Title + " (" + FormInfo.Name + ")"; }
                    Panel.Description = Tools.XNode(ref panels[p], "description").Trim();
                    Panel.Name = Tools.XNode(ref panels[p], "name").ToLower().Trim();
                    Panel.TableClass = Tools.XNode(ref panels[p], "tableclass").Trim();
                    Panel.Type = Tools.XNode(ref panels[p], "type").ToLower().Trim();
                    Panel.Evaluator = Tools.XNode(ref panels[p], "evaluator").Trim();
                    Panel.Wrapper = Tools.XNode(ref panels[p], "wrapper").Trim();
                    tmp = Tools.XNode(ref panels[p], "elementtemplate").Trim();
                    if (tmp.Length > 0) { Panel.ElementTemplate = tmp; } else { Panel.ElementTemplate = ElementTemplate; }

                    theelement = Tools.XNode(ref elements, "element");

                    while (theelement.Length > 0)
                    {
                        eversion = Tools.XNode(ref theelement, "version").ToLower();
                        //if the element has no version, or the forms version is in the list of the element's version(s)
                        if (eversion.Length == 0 || Tools.IsWord(Tools.NormalString(eversion), Tools.NormalString(Version).Replace(" ", ",")))
                        {
                            element = new Element();
                            element.formvalues = FormData;
                            element.modern = Options.UseHTML5Markup;
                            element.type = Tools.XNode(ref theelement, "type").ToLower();
                            element.types = Tools.XNode(ref theelement, "types").ToLower();
                            element.field = Tools.XNode(ref theelement, "field").ToLower();
                            element.name = Tools.XNode(ref theelement, "name").ToLower();

                            if (element.field.Length == 0) { element.field = element.name; }
                            // element.field now contains either a single element name or a list of element codes
                            // lets split them into a list...
                            // element cds for this element
                            element.elementcodes = element.field.Split(',')
                                                    .Select(x => x.Trim())
                                                    .Where(x => !string.IsNullOrWhiteSpace(x))
                                                    .ToArray();


                            element.validation_pattern = Tools.XNode(ref theelement, "validation_pattern").ToLower();
                            element.label = Tools.XNode(ref theelement, "label").Trim();
                            //language or externalized string support
                            if (element.label.StartsWith("#@")) { element.label = StringBit(element.label.Substring(2).Trim(), Language); }

                            element.title = Tools.XNode(ref theelement, "title").Trim();
                            if (element.title.StartsWith("#@")) { element.title = StringBit(element.label.Substring(2).Trim(), Language); }
                            if (element.label.IsNullOrEmpty()) { element.label = element.title; }

                            element.subtitle = Tools.XNode(ref theelement, "subtitle").Trim();
                            if (element.subtitle.StartsWith("#@")) { element.subtitle = StringBit(element.subtitle.Substring(2).Trim(), Language); }

                            element.helptext = Tools.XNode(ref theelement, "help").Trim();
                            if (element.helptext.StartsWith("#@")) { element.helptext = StringBit(element.subtitle.Substring(2).Trim(), Language); }

                            element.image = Tools.XNode(ref theelement, "image").Trim();
                            element.ImagePath = _imagepath;
                            element.list = Tools.XNode(ref theelement, "list");
                            if (element.list.StartsWith("@") || element.list.StartsWith("#"))
                            {
                                element.list = GetList(element.list, ref element.list_cd);
                            }

                            element.layout = Tools.XNode(ref theelement, "layout");
                            element.listselector = Tools.XNode(ref theelement, "listselector");
                            len = Tools.AsInteger(Tools.XNode(ref theelement, "length"));
                            if (len > 0) { element.length = len; }
                            element.rows = Tools.AsInteger(Tools.XNode(ref theelement, "rows"));
                            element.cols = Tools.AsInteger(Tools.XNode(ref theelement, "columns"));
                            element.value = Tools.XNode(ref theelement, "value");
                            element.min = Tools.XNode(ref theelement, "min").Trim();
                            element.max = Tools.XNode(ref theelement, "max").Trim();
                            element.step = Tools.XNode(ref theelement, "step").Trim();
                            element.disabled = Tools.AsBoolean(Tools.XNode(ref theelement, "disabled").Trim());
                            element.attributes = Tools.XNode(ref theelement, "attributes").Trim();
                            element.signed = Tools.AsBoolean(Tools.XNode(ref theelement, "signed"));
                            element.required = Tools.AsBoolean(Tools.XNode(ref theelement, "required"));
                            element.parent = Tools.XNode(ref theelement, "parent");
                            element.ElementTemplate = Tools.XNode(ref theelement, "elementtemplate");
                            element.elementclass = Tools.XNode(ref theelement, "class").Trim();
                            element.wrapperclass = Tools.XNode(ref theelement, "wrapperclass").Trim();
                            if (element.wrapperclass.IndexOf("toggle") > -1) { HasFlaggedFields = true; }

                            element.labelclass = Tools.XNode(ref theelement, "labelclass").Trim();
                            element.ID = Tools.XNode(ref theelement, "id").Trim();
                            element.formula = Tools.XNode(ref theelement, "formula");
                            element.trigger = Tools.AsBoolean(Tools.XNode(ref theelement, "trigger"));
                            element.trigger_value = Tools.XNode(ref theelement, "trigger_value");
                            if (element.trigger_value.IsNullOrEmpty()) { element.trigger_value = Tools.XNode(ref theelement, "trigger-value"); }
                            element.trigger_operator = Tools.XNode(ref theelement, "trigger_operator");
                            if (element.trigger_operator.IsNullOrEmpty()) { element.trigger_operator = Tools.XNode(ref theelement, "trigger-operator"); }
                            element.units = Tools.XNode(ref theelement, "units");
                            element.rowlabels = Tools.XNode(ref theelement, "rowlabels");
                            Element.escapeList(ref element.rowlabels);

                            element.columnlabels = Tools.XNode(ref theelement, "columnlabels");
                            element.rowsummary = Tools.XNode(ref theelement, "rowsummary");
                            element.columnsummary = Tools.XNode(ref theelement, "columnsummary");
                            element.error = Tools.AsBoolean(Tools.XNode(ref theelement, "error"));

                            Panel.Add(element.name, element);
                        }
                        else  //version does not match...
                        {
                            if (Options.VersionWithHiddenElements)
                            {
                                element = new Element();
                                element.formvalues = FormData;
                                element.type = "hidden";
                                element.name = Tools.XNode(ref theelement, "name").ToLower();
                                Panel.Add(element.name, element);
                            }
                        }
                        theelement = Tools.XNode(ref elements, "element");
                    }

                    //=== elements addeded to the panel ===
                    if (Panel.Name.Length == 0) { Panel.Name = "panel"; }
                    if (theform.ContainsKey(Panel.Name))
                    {
                        //the name of the panel is not especially important, right?
                        int i = 0;
                        bool ok = false;
                        while (!ok)
                        {
                            if (theform.ContainsKey(Panel.Name + i.ToString()))
                            {
                                i++;
                            }
                            else
                            {
                                Panel.Name += i.ToString();
                                ok = true;
                            }
                        }
                    }
                    theform.Add(Panel.Name, Panel);
                }

                //== all panels loaded...

                FormReference = "name=" + FormInfo.Name;
                FormReference += "&form=" + formpath;
                FormReference += "&entity=" + FormInfo.Entity;
                FormReference += "&initialized=" + DateTime.Now.ToString();
                FormReference += "&panelcount=" + theform.Count;
                FormReference += "&version=" + Version;
                _salt = Tools.QSSerialize(FormStub);
                if (_salt.Length > 0) { FormReference += "&" + Tools.QSSerialize(FormStub); }
                _salt = Tools.RandomSeed(16);
                FormReference += "&salt=" + _salt;
                FormReference = Tools.Base64Encode(Tools.Encrypt(FormReference));

                //figure out the record ID from the form data
                if (_RecordID == 0 && FormData.ContainsKey("_recordid"))
                {
                    _RecordID = Tools.AsLong(FormData["_recordid"].ToString());
                }
                _formloaded = true;
            }
        }

        public void dataCorrection()
        {


        }

        /// <summary>
        /// Return string array of element codes in a loaded form.
        /// </summary>
        /// <returns>Array of all element codes that can contain data in a form.  Optionally, all element codes, including non-data elements like images or HTML</returns>
        public string[] Elements(bool AllElements = false)
        {
            string[] results = null;
            int offset = 0;
            if (!_formloaded)
            {
                Message("There is no loaded form - there are no elements.");
            }
            else
            {
                foreach (KeyValuePair<string, Panel> panel in theform)
                {
                    string[] elelist = panel.Value.Elements(AllElements);
                    elelist.CopyTo(results, offset);
                    offset += elelist.Length;
                }
            }
            return results;
        }

        public void Validate()
        {
            bool b = false;
            if (!_formloaded)
            {
                Message("There is no loaded form, therefore nothing to validate.");
            }
            else
            {
                Panel thispanel = new Panel();
                Element thiselement = new Element();
                foreach (KeyValuePair<string, Panel> panel in theform)
                {
                    thispanel = panel.Value;
                    thispanel.FormData = FormData;
                    b = thispanel.Validate();
                    if (!b)
                    {
                        _haserrors = true;
                    }
                    //data could have been modified...
                    FormData = thispanel.FormData;
                }
            }
        }

        /// <summary>
        /// Get an element by element name from a loaded form
        /// </summary>
        /// <param name="name">Name of the target element</param>
        /// <returns>Element object</returns>
        public Element GetElement(string name)
        {
            name = name.ToLower();
            bool foundit = false;
            Element results = new Element();
            foreach (KeyValuePair<string, Panel> panel in theform)
            {
                Dictionary<string, Element> d = panel.Value.dictionary;
                foreach (KeyValuePair<string, Element> e in d)
                {
                    if (e.Value.name == name)
                    {
                        results = e.Value;
                        foundit = true;
                        break;
                    }
                    if (foundit) { break; }
                }
            }
            return results;
        }

        /// <summary>
        /// not in use - placeholder
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private Element GetElementCode(string name)
        {
            name = name.ToLower();
            bool foundit = false;
            Element results = new Element();
            foreach (KeyValuePair<string, Panel> panel in theform)
            {
                Dictionary<string, Element> d = panel.Value.dictionary;
                foreach (KeyValuePair<string, Element> e in d)
                {
                    if (e.Value.name == name)
                    {
                        results = e.Value;
                        foundit = true;
                        break;
                    }
                    if (foundit) { break; }
                }
            }
            return results;
        }

        /// <summary>
        /// Set an element to an error state, set the error message
        /// </summary>
        /// <param name="name">Element name</param>
        /// <param name="message">Error message</param>
        /// <returns>True if the element was found and the message was set</returns>
        public bool SetError(string name, string message)
        {
            bool OK = false;
            bool b = false;
            foreach (KeyValuePair<string, Panel> panel in theform)
            {
                b = panel.Value.SetError(name, message);
                if (b)
                {
                    OK = true;
                    _haserrors = true;
                    continue;
                }
            }
            return OK;
        }

        public Dictionary<string, string> FormContext(string reference)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            try
            {
                data = Tools.QSParse(Tools.Decrypt(Tools.Base64Decode(reference)));
            }
            catch (Exception ex)
            {
                Error("FormContext:" + ex.Message);
            }
            return data;
        }
        public void setMeasurements()
        {
            if (this.FormStub.Count < 1 || !this.FormStub.ContainsKey("appt_id"))
                throw new Exception("missing appointment id from form stub");

            //using the values set in stub, get measurements
            long appt = 0;
            int? subseq = null;

            if (!long.TryParse(this.FormStub["appt_id"], out appt))
                throw new Exception("appt_id is an invalid string representation of a long value");

            if (this.FormStub.ContainsKey("subunit_seq_val") && !this.FormStub["subunit_seq_val"].IsNullOrEmpty())
            {
                int tmp = 0;
                if (int.TryParse(this.FormStub["subunit_seq_val"], out tmp))
                    subseq = tmp;
                else
                    throw new Exception("invalid subunit sequence value '" + this.FormStub["subunit_seq_val"] + "'");
            }

            const string _queryMeasurementsByAppt = "select LOWER(element_cd), element_val, to_char(seq_num) as seq_num from measurements where appt_id = {0} and (SEQ_NUM != -1 or SEQ_NUM is null)";
            const string _queryMeasurementsByApptSub = "select LOWER(element_cd), element_val, to_char(seq_num) as seq_num from measurements where appt_id = {0} and subunit_seq_val = {1} and (SEQ_NUM != -1 or SEQ_NUM is null)";

            try
            {
                DataTable res = null;
                var db = new DataTools();
                db.ConnectionString = this.ConnectionString;
                db.Provider = this._provider;
                db.OpenConnection();
                if (subseq.HasValue)
                    db.GetResultSet(string.Format(_queryMeasurementsByApptSub, appt, subseq.Value));
                else
                    db.GetResultSet(string.Format(_queryMeasurementsByAppt, appt));
                res = db.ResultSet;
                setMeasurements(res);
                db.Dispose();  //I assume...
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error setting form measurements via appointment id: {0}", e);
            }

        }
        /// <summary>
        /// set form element values based on a previous instance or interaction with a form
        /// </summary>
        /// <param name="measurements">Resultset of measurements to bind elements to a value by their name. 3 string Columns are expected in the DataTable</param>
        public void setMeasurements(DataTable measurements)
        {
            //create lookup list of each element by iterating through each panel
            var eleLookup = new Dictionary<string, Element>();
            var ignoreTypes = new string[]{ "image","section","panel","html","infobox","label","submit"
            };
            foreach (var panel in this.theform.Values)
            {
                //don't set measurements from the database for ignored types
                foreach (var ele in panel.getElementList().Where(element => !ignoreTypes.Contains(element.type.ToLower())))
                {
                    if (ele.elementcodes != null)
                        foreach (var elecd in ele.elementcodes)
                        {
                            if (!eleLookup.ContainsKey(elecd))
                                eleLookup.Add(elecd, ele);
                        }
                }
            }

            //ordinality expected--  0:element_cd,1:element_val;2:seq_num;
            foreach (DataRow row in measurements.Rows)
            {
                var name = row.Field<string>(0);
                var val = row.Field<string>(1);
                var seq = row.Field<string>(2);

                if (string.IsNullOrEmpty(val) || val.Equals("{empty}", StringComparison.OrdinalIgnoreCase))
                    val = "";

                if (!eleLookup.ContainsKey(name))
                {
                    continue;
                }
                var currEle = eleLookup[name];
                if (currEle.formvalues.ContainsKey(name))
                {
                    currEle.formvalues[name] = currEle.formvalues[name] + "," + val;
                }
                else
                {
                    currEle.formvalues.Add(name, val);
                }
            }
        }

        /// <summary>
        /// Render the loaded form and return the form as an HTML sring.
        /// </summary>
        /// <returns>String</returns>
        public string Render()
        {
            string results = "";
            string _formaction = "formprocessor.aspx";
            if (Action.Length > 0)
            {
                _formaction = Action;
            }
            string _attributes = "";
            if (Attributes.Length > 0)
            {
                _attributes = " " + Attributes;
            }

            foreach (KeyValuePair<string, Panel> panel in theform)
            {
                results += panel.Value.Render();
            }


            string formtagwrapper = System.Environment.NewLine;
            formtagwrapper += "<form[id][name] [class] method=\"" + Method + "\" action=\"" + _formaction + "\"" + _attributes + " autocomplete = \"" + _autocomplete + "\" >" + System.Environment.NewLine;
            formtagwrapper += "{form}" + System.Environment.NewLine;
            formtagwrapper += "</form>" + System.Environment.NewLine;

            //TODO:Externalize this...
            if (_RecordID == 0) { _RecordID = Tools.AsInteger(Tools.GetValue("_ID", FormData)); }
            string formtemplate = System.Environment.NewLine;
            formtemplate += "\t<input type=\"hidden\" name=\"_ID\" value=\"" + _RecordID + "\"/>" + System.Environment.NewLine;
            formtemplate += "\t<input type=\"hidden\" name=\"" + FormReferenceElementName + "\" value=\"" + FormReference + "\"/>" + System.Environment.NewLine;
            formtemplate += "{form}" + System.Environment.NewLine;

            results = formtemplate.Replace("{form}", results);
            if (!Options.InnerFormOnly)
            {
                results = formtagwrapper.Replace("{form}", results);
                results = _replacekey(results, "name", Tools.URLString(FormInfo.Name.ToLower()));
                results = _replacekey(results, "id", ID);
                results = _replacekey(results, "class", ClassName);
            }
            return results;
        }

        private string _replacekey(string source, string key, string value)
        {
            string results = "";
            if (value.Length > 0)
            {
                results = source.Replace("[" + key + "]", " " + key + "=\"" + value + "\"");
            }
            else
            {
                results = source.Replace("[" + key + "]", "");
            }
            return results;
        }
        /// <summary>
        /// Retreive a list of items for a select box, multi-select, or check box field.  Returns either from the cache or from the database
        /// </summary>
        /// <param name="listcode">The LIST CD of the requested list, the list name.</param>
        /// <returns>String, the list in double percision mode, list escaped.</returns>
        public string GetList(string listcode, ref string list_cd)
        {
            int cacheto = 30;       //time to cache a list, in minutes.
            string cachename = "";
            if (_basepath.Length == 0)
            {
                Error("GetList.BasePath:required");
                list_cd = listcode.Substring(1).ToUpper();
                return "";
            }
            bool externalref = false;
            if (listcode.StartsWith("#"))
            {
                externalref = true;
                //how do we cache this?  The "filename" is actually the
                //sql statement or it is a reference named query
                //if a reference, we should be ok
            }

            listcode = listcode.Substring(1).ToUpper();
            list_cd = listcode;

            cachename = Tools.URLString(listcode).ToLower();
            if (externalref)
            {
                cacheto = 12;   //reduce the cache time for these more dynamic lists
                //if the dynamic query has "nocache in it, turn off caching.  This is for lists
                //built with context sensitivity
                if (cachename.IndexOf("nocache") > -1) { cacheto = 0; }
                cachename = cachename.Replace("-from-", "");
                cachename = cachename.Replace("select-", "");
                cachename = cachename.Replace("-as-", "");
                cachename = cachename.Replace("-join-", "");
                cachename = cachename.Replace("-name-", "");
                cachename = cachename.Replace("-value-", "");
                cachename = Tools.TrimTo(cachename, 120);

            }

            string sql = "";
            string results = "";

            Scamps.Cache cache = new Scamps.Cache();
            cache.cachePath = _basepath + "cache\\";
            if (FilteredLists) { cachename += ".filtered"; }

            //string cachepath = _basepath + "cache\\lists\\" + cachename;
            //if (FilteredLists) { cachepath += ".filtered"; }
            //cachepath += ".cache";

            if (!cache.IsExpired(cacheto,cachename, "lists"))  // Tools.TimespanSinceLastCache(cachepath).TotalMinutes < cacheto)
            {
                //recent cache, no need to go fetch it again...
                results = cache.Get(cachename, "lists");
                //results = Scamps.Tools.ReadFile(cachepath);
                if (!results.IsNullOrEmpty()) 
                { 
                    return results; 
                }
            }

            if (ConnectionString.Length == 0)
            {
                Error("GetList.ConnectionString:required");
                return "";
            }
            if (externalref)
            {
                //this is a passed SQL query or a sql query in scampsweb.sql
                //if the list code doesn't start with select<space> it is a
                //reference to a named block...
                if (!listcode.ToLower().StartsWith("select "))
                {
                    listcode = Tools.ReadBlock(_basepath + "resources\\data\\scampsweb.sql", listcode);
                }
                //we should have our sql at this point...
                sql = listcode;
            }
            else
            {
                //current list implementation only
                //added list filtering for providers 12/18/2014
                sql = "select description_txt as name, value_txt as value from admin_element_list where list_cd='" + listcode.ToUpper() + "'";
                if (FilteredLists) { sql += " and (visible_abbr != 'N' or visible_abbr is null)"; }
                sql += " order by seq_num";
            }

            Templating t = new Templating();
            t.Provider = Provider;
            t.ConnectionString = ConnectionString;
            results = t.Results(sql, "[value]:[name:listify],");
            if (results.EndsWith(","))
            {
                results = results.Substring(0, results.Length - 1);
            }
            //cache the results, yo.
            cache.Set(results, cachename, "lists");
            //Tools.WriteFile(cachepath, results);

            return results;
        }

        private string StringBit(string key, string language = "")
        {
            //TODO: should be calling this from templates, proof of concept here.
            string results = "";
            if (bitsofbits.Length == 0)
            {
                bitsofbits = Tools.ReadFile(_templatepath + "strings.xml");
            }
            //local hackable copy....
            string xb = bitsofbits;
            string[] aZ = key.Split('.');
            int wc = aZ.Length - 1;

            for (int x = 0; x < wc; x++)
            {
                xb = Tools.XNode(ref xb, aZ[x]);
            }
            //xb is now the containing node
            //for language support, we want to check inner nodes
            //by language first, then get the base node if not found
            if (!language.IsNullOrEmpty())
            {
                xb = Tools.XNode(ref xb, aZ[wc] + "." + language.ToLower());
                if (xb.IsNullOrEmpty())
                    xb = Tools.XNode(ref xb, aZ[wc]);
            }
            else
            {
                xb = Tools.XNode(ref xb, aZ[wc]);
            }
            results = xb;
            return results;
        }
    }

}
