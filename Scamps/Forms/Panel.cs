﻿using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Scamps
{
    public class Panel
    {
        //colleciton of element (singular) objects
        public string Title = "";
        public string Description = "";
        public string Name = "";        //name of the panel.  Not sure how we use it yet.
        public string Type = "";        //Evaluator,Section,Normal(default)
        public string Region = "";      //this is a named region in the overall page template where this gets inserted.  Allows for very dynamic layout
        public string ClassName = "";
        public bool TableMarkup = true;
        public bool PanelMarkup = false;
        public string TableClass = "";
        public string Evaluator = "";
        public string ImagePath = "";
        public string ElementTemplate = "";

        //something that will be useful eventually but for now is a blank slate.

        public Dictionary<string, Element> dictionary = new Dictionary<string, Element>();
        public Dictionary<string, string> FormData = new Dictionary<string, string>();

        private bool _haserrors = false;
        private string _errors = "";
        private string _wrapper = "";   //string of the template of the full wrapper with {panel} as the replacement target.

        public string Errors
        {
            get { return _errors; }
        }
        public bool HasErrors
        {
            get { return _haserrors; }
        }
        private void Error(string message)
        {
            _haserrors = true;
            _errors += message + System.Environment.NewLine;
        }
        /// <summary>
        /// Number of elements in the Panel collection.
        /// </summary>
        public int Count
        {
            get
            {
                return dictionary.Count;
            }
        }

        /// <summary>
        /// Each panel can be rendered with an optional HTML wrapper.  
        /// </summary>
        public string Wrapper
        {
            set
            {
                _wrapper = value;
            }
        }

        public Element theKey(string value)
        {
            {
                return dictionary[value];
            }
        }

        public void New()
        {
            dictionary.Clear();
        }

        public void Clear()
        {
            if (dictionary.Count > 0)
            {
                dictionary.Clear();
            }
            _haserrors = false;
            _errors = "";
        }

        public IEnumerable<Element> getElementList()
        {
            return dictionary.Select(d => d.Value);
        }


        public void Add(string name, Element newelement)
        {
            name = name.ToLower();
            bool ok = true;
            if (dictionary.ContainsKey(name))
            {
                ok = false;
                switch (newelement.type)
                {
                    case "label":
                    case "readonly":
                    case "section":
                    case "infobox":
                    case "button":
                    case "html":
                    case "image":

                        if (name.Length == 0) { name = newelement.type; }
                        string newname = name;
                        int i = 0;
                        while (!ok)
                        {
                            if (dictionary.ContainsKey(newname))
                            {
                                i++;
                                newname = name + i.ToString();
                            }
                            else
                            {
                                ok = true;
                            }
                        }
                        break;

                    default:
                        Error("Panel.Add:Data critical element key '" + name + "' is not unique.");
                        break;
                }
            }

            if (ok) { dictionary.Add(name.ToLower(), newelement); }

        }

        public void Remove(string name)
        {
            dictionary.Remove(name);
        }

        public string Render()
        {
            StringBuilder sb = new StringBuilder();
            string results = "";
            string panelwrapper = "";
            string tablewrapper = "";

            string classstring = "";

            if (ClassName.Length > 0)
            {
                classstring = " class=\"panel " + ClassName + "\"";
            }
            else
            {
                classstring = " class=\"panel\"";
            }

            foreach (KeyValuePair<string, Element> item in dictionary)
            {
                sb.Append(dictionary[item.Key].RenderElement());
            }

            results = sb.ToString();

            //TODO: Panel Eval block...

            if (_wrapper.Length > 0)
            {
                results = _wrapper.Replace("{panel}", results);
            }

            panelwrapper = "{panel}";
            if (PanelMarkup)
            {
                //internal panel wrapper...
                panelwrapper = System.Environment.NewLine;
                Tools.AppendLine(ref panelwrapper, "<div id=\"" + Name + "\"" + classstring + ">");
                Tools.AppendLine(ref panelwrapper, "\t<div class=\"heading\">");
                Tools.AppendLine(ref panelwrapper, "\t\t<i class=\"collapse icon-angle-circled-up\"></i>");
                Tools.AppendLine(ref panelwrapper, "\t\t<h2>" + Title + "</h2>");
                Tools.AppendLine(ref panelwrapper, "\t</div>");
                Tools.AppendLine(ref panelwrapper, "{panel}");
                Tools.AppendLine(ref panelwrapper, "</div>");
            }

            tablewrapper = "{table}";
            if (TableMarkup)
            {
                tablewrapper = System.Environment.NewLine;
                Tools.AppendLine(ref tablewrapper, "\t<table id=\"" + Name + "_block\"[class]>");
                Tools.AppendLine(ref tablewrapper, "\t\t<tbody>");
                Tools.AppendLine(ref tablewrapper, "{table}");

                //TODO: only if this is of the right panel type? if this is an evaluator panel, add that in now...
                if (Type == "branch")
                {
                    Tools.AppendLine(ref tablewrapper, "<tr><td class=\"nextbar\" colspan=\"2\">");
                    Tools.AppendLine(ref tablewrapper, "\t<var class=\"notation hide\">" + Evaluator + "</var>");
                    Tools.AppendLine(ref tablewrapper, "\t\t<ul class=\"evaluator\"><li class=\"next button\"><input type=\"button\" value=\"NEXT\" /><span class=\"icon-right-open\">&nbsp;</span></li><li class=\"prev button\"><span class=\"icon-left-open\">&nbsp;</span><input type=\"button\" value=\"PREV\" /></li></ul>");

                    //Tools.AppendLine(ref tablewrapper, "\t\t<ul class=\"evaluator\"><li class=\"next\"><input type=\"button\" class=\"evaluator\" value=\"NEXT\" /></li>");
                    Tools.AppendLine(ref tablewrapper, "</td></tr>");
                }
                else if (!Evaluator.IsNullOrEmpty())
                {
                    Tools.AppendLine(ref tablewrapper, "<tr><td><var class=\"notation hide\">" + Evaluator + "</var></td></tr>");
                }

                Tools.AppendLine(ref tablewrapper, "\t\t</tbody>");
                Tools.AppendLine(ref tablewrapper, "\t</table>");

                if (TableClass.Length > 0)
                {
                    tablewrapper = tablewrapper.Replace("[class]", " class=\"" + TableClass + "\"");
                }
                else
                {
                    tablewrapper = tablewrapper.Replace("[class]", "");
                }
            }

            results = tablewrapper.Replace("{table}", results);
            results = panelwrapper.Replace("{panel}", results);

            return results;
        }

        public string[] Elements(bool AllElements = false)
        {
            List<string> results = new List<string>();
            bool included = true;
            foreach (KeyValuePair<string, Element> item in dictionary)
            {
                if (!AllElements)
                {
                    switch (item.Value.type.ToLower())
                    {
                        case "image":
                        case "section":
                        case "panel":
                        case "html":
                        case "infobox":
                        case "label":
                        case "submit":
                        case "button":

                            included = false;
                            break;

                        default:
                            included = true;
                            break;
                    }
                }
                if (included)
                {
                    results.AddRange(item.Value.elementcodes);
                }
            }
            return results.ToArray();
        }

        public bool Validate()
        {
            bool b = false;
            bool OK = true;
            string msg = "";
            Element element = new Element();
            foreach (KeyValuePair<string, Element> item in dictionary)
            {
                element = item.Value;
                if (FormData.ContainsKey(item.Key))
                {
                    b = element.validateString(FormData[item.Key], out msg);
                }
                else
                {
                    b = element.validateString("", out msg);
                    if (element.type == "checkbox")
                    {
                        //the element is a checkbox
                        //it is not in the FormData
                        //it is therefore "false" or 0
                        //add it to the dictionary
                        FormData.Add(item.Key, "0");
                    }
                }
                if (!b)
                {
                    OK = false;
                }
            }
            return OK;
        }
        /// <summary>
        /// Set an element to an error state, set the error message
        /// </summary>
        /// <param name="name">Element name</param>
        /// <param name="message">Error message</param>
        /// <returns>True if the element was found and the message was set</returns>
        public bool SetError(string name, string message)
        {
            bool OK = false;
            if (dictionary.ContainsKey(name))
            {
                dictionary[name].error = true;
                dictionary[name].errormessage = message;
                OK = true;
            }
            return OK;
        }

    }

}
