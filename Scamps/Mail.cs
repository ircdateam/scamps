﻿using System.Net.Mail;
using System.Text;
using System.Collections.Generic;

namespace Scamps
{
    public class Mail
    {
        private Encoding _bodyEncoding = Encoding.UTF8;
        public string Subject = "";
        public string Body = "";
        public string FromAddress = "";
        public string FromName = "";
        public string ReplyTo = "";

        public bool IsHTML = false;
        public MailPriority Priority = MailPriority.Normal;

        public string MailServer = "127.0.0.1";
        public int Port = 25;
        public string NetworkUser = "";
        public string NetworkPassword = "";
        public bool NetworkMail = false;
        public bool SecureEmail = false;

        public Dictionary<string, string> Data = new Dictionary<string, string>();

        private Dictionary<string, string> _to = new Dictionary<string, string>();
        private Dictionary<string, string> _cc = new Dictionary<string, string>();
        private Dictionary<string, string> _bcc = new Dictionary<string, string>();
        private List<string> attachments = new List<string>();

        #region "Errors and Messages"

        //=========== ERROR & MESSAGES =====================
        private bool _haserrors = false;
        private string _errors = "";
        private bool _hasmessages = false;
        private string _messages = "";

        public bool HasErrors
        {
            get
            {
                return _haserrors;
            }
        }
        public bool HasMessages
        {
            get
            {
                return _hasmessages;
            }
        }
        public string errors
        {
            get
            {
                return _errors;
            }
            set
            {
            }
        }

        private void Message(string message)
        {
            _hasmessages = true;
            _messages += message + System.Environment.NewLine;
        }

        private void Error(string errormessage)
        {
            _errors += errormessage + System.Environment.NewLine;
            _haserrors = true;
        }
        #endregion


        public void AddTo(string email, string name = "")
        {
            _Add(email, name, "to");
        }
        public void AddCC(string email, string name = "")
        {
            _Add(email, name, "cc");
        }
        public void AddBCC(string email, string name = "")
        {
            _Add(email, name, "bcc");
        }

        private void _Add(string email, string name = "", string list = "to")
        {
            if (Tools.ValidateMailFormat(email))
            {
                if (name.Length == 0)
                {
                    //clean up the name from the addres...
                    name = Tools.StringTo(email, "@");
                    name = name.Replace("_", " ");
                    name = name.Replace(".", " ");
                    name = Tools.ProperCase(name);
                }
                if (!_to.ContainsKey(name))
                {
                    switch (list)
                    {
                        case "to":
                            _to.Add(name, email);
                            break;

                        case "cc":
                            _cc.Add(name, email);
                            break;

                        case "bcc":
                            _bcc.Add(name, email);
                            break;
                    }
                }
                else
                {
                    Error("Warning:Email address is blank or invalid");
                }
            }
            else
            {
                Error("Warning:Name is blank or invalid (duplicate violation?)");
            }
        }

        /// <summary>
        /// Add an attachment to a mail message
        /// </summary>
        /// <param name="FullPath">Fully qualified path to the file to be included</param>
        public void AddAttachment(string FullPath)
        {
            if (Tools.FileExists(FullPath))
            {
                attachments.Add(FullPath);
            }
            else
            {
                Error("AddAttachment:File does not exist");
            }
        }

        /// <summary>
        /// Formats a mail message body and subject using key/value items in the Data dictionary, and populates object properties from an email template.
        /// </summary>
        /// <param name="template">The full path to an email template or the full template as a string</param>
        public void FormatMessage(string template)
        {
            //sets the subject and body of the email based on properties in the mail template
            //resolve the template
            if (Tools.FileExists(template))
            {
                template = Tools.ReadFile(template);
            }

            Templating ts = new Templating();

            if (Body.Length == 0) { Body = Tools.XNode(ref template, "message"); }
            if (Subject.Length == 0) { Subject = Tools.XNode(ref template, "subject"); }
            if (FromName.Length == 0) { FromName = Tools.XNode(ref template, "fromname"); }
            if (FromAddress.Length == 0) { FromAddress = Tools.XNode(ref template, "fromaddress"); }
            if (ReplyTo.Length == 0) { ReplyTo = Tools.XNode(ref template, "replyto"); }

            Body = ts.Format(Body, Data);
            Subject = ts.Format(Subject, Data);
        }

        /// <summary>
        /// Sends a mail message using the configuration specified by the object's porperties.
        /// </summary>
        public void Send()
        {
            MailMessage message = new MailMessage();

            message.BodyEncoding = _bodyEncoding;
            message.Priority = Priority;

            _loadaddresses(ref message, _to, "to");
            _loadaddresses(ref message, _cc, "cc");
            _loadaddresses(ref message, _bcc, "bcc");

            message.Body = Body;
            message.IsBodyHtml = IsHTML;

            message.Subject = Subject;
            MailAddress address = new MailAddress(FromAddress, FromName);
            message.From = address;
            if (ReplyTo.Length > 0)
            {
                message.ReplyTo = new MailAddress(ReplyTo);
            }
            foreach (string item in attachments)
            {
                System.Net.Mail.Attachment att = new System.Net.Mail.Attachment(item);
                message.Attachments.Add(att);
            }

            SmtpClient SmtpMail = new SmtpClient();
            SmtpMail.Host = MailServer;
            SmtpMail.Port = Port;
            SmtpMail.EnableSsl = SecureEmail;

            //network mail credentials.....
            if (NetworkMail == true)
            {
                if (NetworkUser.Length == 0 | NetworkPassword.Length == 0)
                {
                    Error("NetworkMailException: The NetworkUser and NetworkPassword properties must both be set.");
                    return;
                }
                SmtpMail.UseDefaultCredentials = false;
                SmtpMail.Credentials = new System.Net.NetworkCredential(NetworkUser, NetworkPassword);
                SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            }

            try
            {
                SmtpMail.Send(message);
            }
            catch (SmtpException ex)
            {
                Error("Send.SMTPException: " + ex.Message);
            }
        }

        private void _loadaddresses(ref MailMessage message, Dictionary<string, string> addresses, string scope = "to")
        {
            foreach (KeyValuePair<string, string> address in addresses)
            {
                MailAddress thisaddress = new MailAddress(address.Value.ToString(), address.Key.ToString());
                switch (scope)
                {
                    case "cc":
                        message.CC.Add(thisaddress);
                        break;
                    case "bcc":
                        message.Bcc.Add(thisaddress);
                        break;
                    default:
                        message.To.Add(thisaddress);
                        break;
                }
            }
        }
    }
}
