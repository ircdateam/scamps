﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Scamps
{
    public class Scripter
    {
        public class expression
        {
            public string keyword = "";
            public string condition = "";
            public string action = "";
            public string value = "";

            //break out of the condition...
            public bool conditionIsFieldReference = false;
            public bool leftIsFieldReference = false;
            public string leftHandArgument = "";
            public string logicOperator = "";
            public bool rightIsFieldReference = false;
            public string rightHandArgument = "";

        }

        public bool HasErrors = false;

        public string Errors
        {
            get
            {
                return _errors;
            }
        }
        public string Keywords
        {
            set
            {
                value = value.Replace(",", "|");
                _keywords = value;
            }
        }

        public string Actions
        {
            set
            {
                value = value.Replace(",", "|");
                _actions = value;
            }
        }

        public void Clear()
        {
            HasErrors = false;
            _errors = "";
        }
        private string _errors = "";
        private string _keywords = "if|onchange|each|after|set|onodd|oneven";
        private string _actions = "use|insert|append|skip|wrap|addclass";

        private void Error(string message)
        {
            HasErrors = true;
            _errors += message + System.Environment.NewLine;
        }

        public List<expression> Compile(string script)
        {
            List<expression> stack = new List<expression>();

            //prep the script block...
            //remove comment blocks
            script = Tools.ReplaceRange(script, "/*", "*/", "");
            script = script.Trim();
            script = script.Replace("\n", "");
            script = script.Replace("\r", "");
            script = script.Replace("\t", "");

            if (script.EndsWith(";"))
            {
                script = script.Substring(0, script.Length - 1);
            }

            string[] s = script.Split(';');
            for (int i = 0; i < s.Length; i++)
            {
                //comments are ignored...
                if (!s[i].StartsWith("//"))
                {
                    stack.Add(Parse(s[i].Trim(), i + 1));
                }
            }

            return stack;
        }

        public expression Parse(string phrase, int linenumber = 0)
        {
            expression e = new expression();

            string pattern = "";

            pattern = @"^" + _keywords + @"\s*\( ";

            Regex ck = new Regex(pattern, RegexOptions.IgnoreCase);

            Match match = ck.Match(phrase);
            if (match.Success)
            {
                e.keyword = match.Value.ToString().Trim().ToLower();
                phrase = phrase.Replace(match.Value, "").Trim();
                ck = new Regex(@"^\(.*?\)");

                match = ck.Match(phrase);
                if (!match.Success)
                {
                    //try for just a field reference...
                    ck = new Regex(@"\[(.*?)\]");
                    match = ck.Match(phrase);
                    if (match.Success) { e.conditionIsFieldReference = true; }
                }
                if (match.Success)
                {
                    e.condition = match.Value.ToString().Trim();
                    e.condition = e.condition.Substring(1, e.condition.Length - 2);
                    phrase = phrase.Replace(match.Value, "").Trim();

                    // got the condition, now get the action...
                    pattern = @"^" + _actions + @"\s*";
                    ck = new Regex(pattern, RegexOptions.IgnoreCase);
                    match = ck.Match(phrase);
                    if (match.Success)
                    {
                        e.action = match.Value.ToString().Trim().ToLower();
                        phrase = phrase.Replace(match.Value, "").Trim();
                        e.value = phrase;

                        //now parse out the condition block...

                        //find the logical operator...
                        pattern = @"[\<\>\!\=]([=>]?)";
                        ck = new Regex(pattern);
                        match = ck.Match(e.condition);
                        if (match.Success)
                        {
                            e.logicOperator = match.Value.ToString().Trim();
                            string[] a = Regex.Split(e.condition, pattern);
                            if (a.Length > 0)
                            {
                                e.leftHandArgument = a[0].Trim();
                                if (e.leftHandArgument.StartsWith("[") & e.leftHandArgument.EndsWith("]"))
                                {
                                    e.leftHandArgument = e.leftHandArgument.Substring(1, e.leftHandArgument.Length - 2);
                                    e.leftIsFieldReference = true;
                                }
                            }
                            if (a.Length > 2)
                            {
                                e.rightHandArgument = Tools.UnQ(a[2].Trim());
                                if (e.rightHandArgument.StartsWith("[") & e.rightHandArgument.EndsWith("]"))
                                {
                                    e.rightHandArgument = e.rightHandArgument.Substring(1, e.rightHandArgument.Length - 2);
                                    e.rightIsFieldReference = true;
                                }
                            }
                        }
                        else
                        {
                            //no logical operator...
                            //should be single item
                            e.leftHandArgument = e.condition;
                        }
                    }
                    else
                    {
                        //Syntax error, no action specified.
                        Error("Invalid action specified, line " + linenumber.ToString());
                        e.keyword = "ERROR";
                    }
                }
                else
                {
                    //SYNTAX ERROR: no condition found in the phrase
                    Error("No conditional phrase found, line " + linenumber.ToString());
                    e.keyword = "ERROR";
                }
            }
            else
            {
                //SYNTAX ERROR: no keyword found in the phrase
                Error("Invalid keyword, line " + linenumber.ToString());
                e.keyword = "ERROR";
            }

            return e;

        }

    }

}
