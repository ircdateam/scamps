﻿using System;
using System.IO;
using System.Data;
using System.Text;
using System.Collections.Generic;

namespace Scamps
{
    public class Templating
    {
        public string errors = "";
        public bool haserrors = false;

        public string messages = "";
        public bool hasmessages = false;

        public bool NodeFound = false;
        public string ConnectionString = "";

        public Dictionary<string, string> session = new Dictionary<string, string>();
        public Dictionary<string, string> data = new Dictionary<string, string>();
        public Dictionary<string, string> qs = new Dictionary<string, string>();
        public Dictionary<string, string> parameters = new Dictionary<string, string>();
        public Dictionary<string, string> formdata = new Dictionary<string, string>();

        public PageInfo Page = new PageInfo();
        public OptionsList Options = new OptionsList();
        public string Language = "";
        private string _provider = "sql";
        private int RED = 100;
        private int GREEN = 160;
        private int BLUE = 255;

        public string Provider
        {
            get { return _provider; }
            set
            {
                //TODO: make sure it is a supported provider, spelled correctly, etc.

                switch (value.ToLower())
                {
                    case "oracle.manageddataaccess.client":
                    case "oracle":
                    case "ora":
                    case "o":
                        _provider = "oracle";
                        break;

                    default:
                        _provider = "sql";
                        break;
                }
            }
        }
        /// <summary>
        /// BasePath is the absolute path of the root directory of the web applicaiton.  Setting base path also sets
        /// TemplatePath and FormPath as relative to BasePath in the resources/ directory.  If templates and forms are to 
        /// be found in another location, set these properties after setting BathPath.
        /// </summary>
        public string BasePath
        {
            get
            {
                return _basepath;
            }
            set
            {
                if (!value.EndsWith("\\"))
                {
                    value = value + "\\";
                }
                if (Directory.Exists(value))
                {
                    _basepath = value;
                    _templatepath = _basepath + "resources\\templates\\";
                    _formpath = _basepath + "resources\\forms\\";
                }
                else
                {
                    Error("BasePath.Directory:" + value + " does not exist.");
                }

            }
        }

        public string TemplatePath
        {
            get
            {
                return _templatepath;
            }
            set
            {
                if (!value.EndsWith("\\"))
                {
                    value = value + "\\";
                }
                if (Directory.Exists(value))
                {
                    _templatepath = value;
                }
                else
                {
                    Error("TemplatePath.Directory:" + value + " does not exist.");
                }

            }
        }

        public string FormPath
        {
            get
            {
                return _formpath;
            }
            set
            {
                if (!value.EndsWith("\\"))
                {
                    value = value + "\\";
                }
                if (Directory.Exists(value))
                {
                    _formpath = value;
                }
                else
                {
                    Error("FormPath.Directory:" + value + " does not exist.");
                }

            }
        }

        public class OptionsList
        {
            public bool DisallowZeroLengthValuesOnSignature = false;
            public bool TableizeDelimitOnPipes = false;
            public bool TableizeSupressFieldClasses = false;
        }


        private string bitsofbits = "";     //cached copy of the strings.xml file...
        private string _templatepath = "";
        private string _formpath = "";
        private string _basepath = "";



        /// <summary>
        /// Resets the object
        /// </summary>
        public void Clear()
        {
            errors = "";
            haserrors = false;

            messages = "";
            hasmessages = false;
            NodeFound = false;
        }


        private void Error(string message)
        {
            haserrors = true;
            errors += message + System.Environment.NewLine;
        }

        private void Message(string message)
        {
            hasmessages = true;
            messages += message + System.Environment.NewLine;
        }

        public void Dispose()
        {
            //any other code to release handles or connections here
            //included as good practice and a consistent pattern
            this.Dispose();
        }

        //=====================================================================
        //            Web Page Building Methods
        //=====================================================================

        public class PageInfo
        {
            public string Content = "";
            public string Title = "";
            public string MetaDescription = "";
            public string MetaKeywords = "";
            public string CSSReferences = "";
            public string ScriptReferences = "";
            public string Head = "";

            public void Clear()
            {
                Content = "";
                Title = "";
                MetaDescription = "";
                MetaKeywords = "";
                CSSReferences = "";
                ScriptReferences = "";
                Head = "";
            }
        }

        public string NextColor()
        {
            RED = Math.Abs(RED + 4) % 220;
            GREEN = (Math.Abs(GREEN - 16) % 180) + 60;
            BLUE = (Math.Abs(BLUE + 24) % 180) + 40;
            return (RED.ToString("X") + GREEN.ToString("X") + BLUE.ToString("X") + "00000F").Substring(0, 6);

        }
        public string ShiftColorUp()
        {

            int R = RED + 16;
            int G = GREEN + 16;
            int B = BLUE + 16;
            return (R.ToString("X") + G.ToString("X") + B.ToString("X") + "00000F").Substring(0, 6);
        }
        public string ShiftColorDown()
        {
            int R = RED - 10;
            if (R < 5) { R = 5; }
            int G = GREEN - 10;
            if (G < 5) { G = 5; }
            int B = BLUE - 10;
            if (B < 50) { B = 50; }

            return (R.ToString("X") + G.ToString("X") + B.ToString("X") + "00000F").Substring(0, 6);
        }
        /// <summary>
        /// Process a block document and return the results
        /// </summary>
        /// <param name="pageTemplate">The fully qualified path to the template file, the name of the template (with or 
        /// without extension) that will be found in the template/page path specified in the TemplatePath property, or the full 
        /// template contents as a string.</param>
        /// <returns>String - block processed template.  Only the BODY node of the template will be returned, if present.</returns>
        public string WebPage(string pageTemplate, string innertemplate = "")
        {
            Page.Clear();
            pageTemplate = xResolveTemplate(pageTemplate);
            innertemplate = xResolveTemplate(innertemplate);
            if (innertemplate.Length > 0)
            {
                pageTemplate = pageTemplate.Replace("[innertemplate]", innertemplate);
            }


            //break the page template into pieces based on xnodes
            string head = Tools.XNode(ref pageTemplate, "head");
            string title = Tools.XNode(ref head, "title");
            if (!title.IsNullOrEmpty())
            {
                Tools.AddOrReplace("page.title", title, ref parameters);
            }
            //it could be a fully built out page or maybe just the page body or a sub block.
            //let's find out which...
            string body = Tools.XNode(ref pageTemplate, "body");
            if (!Tools.NodeFound)
            {
                body = pageTemplate;
            }
            body = _pageprocess(body);
            head = _pageprocess(head);
            body = _commonreference(body);
            head = _commonreference(head);
            //set page properties based on what has been passed in...
            Page.Content = body;
            Page.Head = head;

            return body;
        }
        /// <summary>
        /// Process a block document and return the results
        /// </summary>
        /// <param name="pageTemplate">The fully qualified path to the template file, the name of the template (with or 
        /// without extension) that will be found in the template/page path specified in the TemplatePath property, or the full 
        /// template contents as a string.</param>
        /// <returns>String - block processed template.  Only the BODY node of the template will be returned, if present.</returns>
        public string WebBlock(string pageTemplate)
        {

            pageTemplate = xResolveTemplate(pageTemplate);

            //it could be a fully built out page or maybe just the page body or a sub block.
            //let's find out which...
            string body = Tools.XNode(ref pageTemplate, "body");
            if (!Tools.NodeFound)
            {
                body = pageTemplate;
            }
            body = _pageprocess(body);
            body = _commonreference(body);
            return body;
        }

        private string _pageprocess(string block)
        {
            //process the pieces through the block processor or other processors

            //set the Page 
            string results = block;
            int i = 0;
            int e = 0;
            int n = 0;
            string substr = "";
            string b = "";



            //include - do first...
            b = Tools.StringPart(results, "[#include@", "/]");
            while (!(b.Length == 0))
            {
                results = results.Replace("[#include@" + b + "/]", Include(b));
                b = Tools.StringPart(results, "[#include@", "/]");
            }

            //if 
            i = results.IndexOf("[#if(");

            while (!(i == -1))
            {
                b = Tools.StringPart(results, "[#if(", ")/]");
                n = results.IndexOf("[#else/]", i);
                e = results.IndexOf("[#end/]", i);
                if (n > e)
                    n = -1;
                if (bIf(b))
                {
                    if (n > 0)
                    {
                        substr = Tools.StringPart(results, "[#if(" + b + ")/]", "[#else/]");
                    }
                    else
                    {
                        substr = Tools.StringPart(results, "[#if(" + b + ")/]", "[#end/]");
                    }
                }
                else
                {
                    if (n > 0)
                    {
                        substr = Tools.StringPart(results, "[#else/]", "[#end/]");
                    }
                }

                results = Tools.ReplaceRange(results, "[#if(" + b, "[#end/]", substr, 0, true);
                i = results.IndexOf("[#if(");
                substr = "";
            }

            //string bits
            b = Tools.StringPart(results, "[#string@", "/]");
            while (!(b.Length == 0))
            {
                results = results.Replace("[#string@" + b + "/]", StringBit(b, Language));
                b = Tools.StringPart(results, "[#string@", "/]");
            }

            //replacement of cache....
            //TODO: make this a custom HTML tag and do params by attributes...
            b = Tools.StringPart(results, "[#cache@", "[#end/]");
            while (!(b.Length == 0))
            {
                substr = Tools.StringTo(ref b, "/]");
                results = results.Replace("[#cache@" + substr + "/]" + b + "[#end/]", Caches(substr, b));
                b = Tools.StringPart(results, "[#cache@", "[#end/]");
            }

            //replacement of widgets....
            b = Tools.StringPart(results, "[#widget@", "/]");
            while (!(b.Length == 0))
            {
                results = results.Replace("[#widget@" + b + "/]", Widget(b));
                b = Tools.StringPart(results, "[#widget@", "/]");
            }

            b = Tools.StringPart(results, "[#form@", "/]");
            while (!(b.Length == 0))
            {
                results = results.Replace("[#form@" + b + "/]", Form(b));
                b = Tools.StringPart(results, "[#form@", "/]");
            }

            //loop through the dictionaries and press into template...     
            foreach (KeyValuePair<string, string> entry in data)
            {
                results = xMergeField(results, entry.Key, entry.Value);
            }

            foreach (KeyValuePair<string, string> entry in session)
            {
                results = xMergeField(results, entry.Key, entry.Value);
            }

            foreach (KeyValuePair<string, string> entry in parameters)
            {
                results = xMergeField(results, entry.Key, entry.Value);
            }

            return results;
        }
        /// <summary>
        /// Templated file listing
        /// </summary>
        /// <param name="Path">Path of the directory to examine</param>
        /// <param name="Pattern">File pattern to match, such as "*.dbt".  Default is all files.</param>
        /// <param name="Template">Template or template file to use to markup the file information.  If not supplied, it will look for 
        /// 'filelisting.dbt' in the normal template path or default to a simple file name and size ul</param>
        /// <returns>String, templated results.</returns>
        /// <remarks>Files are assumed to be text files and are opened and examined looking for an XML block 'info' which contains meta information 
        /// about the file and its use/purpose.  </remarks>
        public string FileListing(string Path, string Pattern = "", string Template = "")
        {
            string results = "";
            if (Template.Length == 0)
            {
                Template = Tools.ReadFile(_templatepath + "fileListing.dbt");
                if (Template.Length == 0)
                {
                    //still got no template, make one
                    Template += "<wrapper><ul>{0}</ul></wrapper>" + System.Environment.NewLine;
                    Template += "<template><li>[filename] ([bytes])</li></template>";
                }
            }
            results = Results(Tools.Files(Path, Pattern), Template);
            return results;
        }

        public string Form(string formname)
        {
            string results = "";
            //formname could have qs keys that should be processed...
            Dictionary<string, string> attributes = new Dictionary<string, string>();
            int i = formname.IndexOf("?");
            if (i != -1)
            {
                results = formname.Substring(i + 1);
                formname = formname.Substring(0, i);
                attributes = Tools.QSParse(results);
            }
            Scamps.Form form = new Scamps.Form();
            form.Provider = Provider;
            form.ConnectionString = ConnectionString;
            form.BasePath = _basepath;
            form.Language = Language;

            //form version:
            if (attributes.ContainsKey("version")) { form.Version = attributes["version"]; }
            //form class:
            if (attributes.ContainsKey("class")) { form.ClassName = attributes["class"]; }
            //form ID:
            if (attributes.ContainsKey("id")) { form.ID = attributes["id"]; }

            form.FormData = formdata;

            string formxml = form.LoadFormXml(formname);
            form.Load(_pageprocess(formxml)); //Get the form XML, do the pageprocess and load it.

            results = form.Render();
            return results;
        }
        public string Widget(string WidgetName,Boolean testmode = false)
        {
            string results = "";
            string tmp = WidgetName;
            string widget = "";

            //the widget name could be either the file name (no path) or the internal name in the widget file.
            if (tmp.LastIndexOf('.') == -1)
            {
                tmp = tmp + ".xml";
            }
            //TODO:externalize pathing via a config file or config from database via dictionary.
            if (Tools.ExtensionOf(BasePath + "resources\\templates\\" + tmp) == ".dbt")
            {
                widget = Tools.ReadFile(BasePath + "resources\\templates\\" + tmp);
            }
            else
            {
                if (Tools.FileExists(BasePath + "resources\\widgets\\" + tmp))
                {
                    widget = Tools.ReadFile(BasePath + "resources\\widgets\\" + tmp);
                }
                else
                {
                    //TODO:check inside the file for the info>name in widget method
                }
            }
            widget = Tools.XNode(ref widget, "widget");

            results = Results(widget, qs);

            // remove any unresolved tokens to clean up display
            // testmode = false only
            if (!testmode)
            {
                results = Tools.ReplaceRange(results, "[", "]","");
            }

            return results;
        }

        private string dbtinner(string source)
        {
            string tmp = Tools.XNode(ref source, "dbt").Trim();
            if (tmp.Length > 0) { source = tmp; }
            return source;
        }

        private string Include(string filename)
        {
            if (filename.IndexOf('.') == -1)
            {
                filename += ".dbt";
            }
            string results = dbtinner(Tools.ReadFile(_templatepath + filename));
            return results;
        }


        public string StringBit(string key, string language = "")
        {
            string results = "";
            if (bitsofbits.Length == 0)
            {
                bitsofbits = Tools.ReadFile(_templatepath + "strings.xml");
            }
            //local hackable copy....
            string xb = bitsofbits;
            string[] aZ = key.Split('.');
            int wc = aZ.Length - 1;

            for (int x = 0; x < wc; x++)
            {
                xb = Tools.XNode(ref xb, aZ[x]);
            }
            //xb is now the containing node
            //for language support, we want to check inner nodes
            //by language first, then get the base node if not found
            if (!language.IsNullOrEmpty())
            {
                xb = Tools.XNode(ref xb, aZ[wc] + "." + language.ToLower());
                if (xb.IsNullOrEmpty())
                    xb = Tools.XNode(ref xb, aZ[wc]);
            }
            else
            {
                xb = Tools.XNode(ref xb, aZ[wc]);
            }
            results = xb;
            return results;
        }



        private string Caches(string Options, string block)
        {

            // Cached blocks are expressed like this in a template:
            //
            //         [#cache@cachename=thename&cacheto=30/]
            //
            // cacheto is expressed in minutes.

            Dictionary<string, string> cacheoptions = Tools.QSParse(Options);

            string cachename = "";
            if (cacheoptions.Count == 0)
            {
                cachename = Options;
            }
            else
            {
                cachename = cacheoptions["cachename"];
            }


            DateTime t = DateTime.Now;
            string results = "";
            int p = 0;
            bool refreshCache = false;
            TimeSpan s = default(TimeSpan);
            int cacheto = -1;           //defualt - no cache, return the file if found, otherwise create it
            bool output = true;         //does this method return the resulting cache?  Default is true.  If false, an empty string is returned.
            //This is used in instances where the cached file is required for other operations but not for display
            //i.e. refreshing data from a web service.
            string cacheFolder = "";    //what folder/directory is the cache written to?
            string baseFolder = BasePath;
            string filepath = "";

            //if we have no params, including a name, drop out...
            if (cachename.Length == 0)
                return "";


            //the cache looks for a file in the /cache directory named
            //<cachepath>/<cachename>.cache
            //If it finds it, and the touch date/time is within cacheto (default to -1)
            //minutes, it returns the file, otherwise it executes the function
            //specified by cachefunction through the functions method.

            //if cacheto=-1 then the file is returned if found, generated if 
            //not found.


            if (cacheoptions.ContainsKey("cacheto"))
            {
                cacheto = Tools.AsInteger(cacheoptions["cacheto"]);
                if (cacheto == 0)
                    cacheto = -1;
            }

            if (cacheoptions.ContainsKey("output"))
            {
                output = Tools.AsBoolean(cacheoptions["output"]);
            }


            if (cacheoptions.ContainsKey("folder"))
            {
                cacheFolder = Tools.AsText(cacheoptions["folder"].Trim('/').Trim('\\'));
                if (cacheFolder.Length == 0)
                    cacheFolder = "base";
                cacheFolder += "\\";
            }

            filepath = baseFolder + "cache\\" + cacheFolder + cachename + ".cache";

            if (Tools.FileExists(filepath))
            {
                if (cacheto < 0)
                {
                    results = Tools.ReadFile(filepath);

                }
                else
                {
                    s = TimespanSinceLastCache(filepath);
                    if (s.TotalMinutes > cacheto)
                    {
                        refreshCache = true;
                    }
                    else
                    {
                        results = Tools.ReadFile(filepath);
                        if (results.Length == 0)
                            refreshCache = true;
                    }
                }
            }
            else
            {
                refreshCache = true;
            }

            if (refreshCache)
            {
                results = WebBlock(block);
                Tools.WriteFile(filepath, results);
            }


            cacheoptions = null;

            if (!output)
                results = "";
            return results;
        }


        /// <summary>
        /// How long since a file was last cached?
        /// </summary>
        /// <param name="filename">name of file to check (with or without extension)</param>
        /// <returns>minutes since last cache</returns>
        /// <remarks></remarks>
        private TimeSpan TimespanSinceLastCache(string filename)
        {
            TimeSpan returnVal = default(TimeSpan);
            try
            {
                string directory = Directory.GetParent(filename).FullName;
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                if (Directory.Exists(directory))
                {
                    string file = filename;
                    if (!file.Contains("."))
                        file += ".cache";
                    System.IO.FileInfo cache_info = new System.IO.FileInfo(file);
                    returnVal = DateTime.Now.Subtract(cache_info.LastWriteTime);
                }
            }
            catch (Exception ex)
            {
                Error("TimespanSinceLastCache.Exception:" + ex.Message);
            }
            return returnVal;
        }







        private bool bIf(string expression)
        {

            bool OK = true;


            expression = expression.Replace("<", " < ");
            expression = expression.Replace(">", " > ");
            expression = expression.Replace("!", " ! ");
            expression = Tools.NormalString(expression);

            expression = expression.Replace("! =", "!=");
            expression = expression.Replace("< >", "<>");
            expression = expression.Replace("= =", "==");

            string[] aM = new string[1];
            int n = 0;
            int p = 0;
            int s = 0;

            expression = Tools.CollapseString(expression, ref aM);

            string[] stack = expression.Split(' ');

            //first, preprocess the stack...

            try
            {

                for (int i = 0; i <= stack.Length - 1; i++)
                {
                    stack[i] = stack[i].Trim();

                    if (Tools.isNumeric(stack[i]))
                    {
                    }
                    else
                    {
                        if (stack[i].StartsWith("<[") & stack[i].EndsWith("]>"))
                        {
                            //this is a literal string...
                            n = Tools.AsInteger(Tools.StringPart(stack[i], "<[", "]>"));
                            stack[i] = aM[n];
                            if (stack[i].StartsWith("\""))
                                stack[i] = stack[i].Substring(1);
                            if (stack[i].EndsWith("\""))
                                stack[i] = stack[i].Substring(0, stack[i].Length - 1);

                        }
                        else if (stack[i].ToLower().StartsWith("data."))
                        {
                            if (data.ContainsKey(stack[i].ToLower().Substring(5)))
                            {
                                stack[i] = data[stack[i].Substring(5)];
                            }

                        }
                        else if (stack[i].ToLower().StartsWith("form."))
                        {
                            if (formdata.ContainsKey(stack[i].ToLower().Substring(5)))
                            {
                                stack[i] = formdata[stack[i].Substring(5)];
                            }

                        }
                        else if (stack[i].ToLower().StartsWith("qs."))
                        {
                            if (qs.ContainsKey(stack[i].ToLower().Substring(3)))
                            {
                                stack[i] = qs[stack[i].Substring(3)];
                            }
                        }
                        else if (stack[i].ToLower().StartsWith("session."))
                        {
                            if (session.ContainsKey(stack[i].ToLower().Substring(8)))
                            {
                                stack[i] = session[stack[i].Substring(8)];
                            }
                        }
                        else if (stack[i].ToLower().StartsWith("param."))
                        {
                            if (parameters.ContainsKey(stack[i].ToLower().Substring(6)))
                            {
                                stack[i] = parameters[stack[i].Substring(6)];
                            }
                        }
                        else if (stack[i].ToLower().StartsWith("date."))
                        {
                            switch (stack[i].ToLower())
                            {
                                case "date.today":
                                    stack[i] = DateTime.Today.ToString();
                                    break;
                                case "date.tomorrow":
                                    stack[i] = DateTime.Today.AddDays(1).ToString();
                                    break;
                                case "date.yesterday":
                                    stack[i] = DateTime.Today.AddDays(1).ToString();
                                    break;
                                case "date.thisyear":
                                    stack[i] = DateTime.Today.Year.ToString();
                                    break;
                                case "date.lastyear":
                                    stack[i] = (DateTime.Today.Year - 1).ToString();
                                    break;
                                case "date.nextyear":
                                    stack[i] = (DateTime.Today.Year + 1).ToString();
                                    break;
                                case "date.thismonth":
                                    p = (DateTime.Today.Month);
                                    stack[i] = p.ToString();
                                    break;
                                case "date.lastmonth":
                                    p = (DateTime.Today.Month - 1);
                                    if (p == 0) { p = 12; }
                                    stack[i] = p.ToString();
                                    break;
                                case "date.nextmonth":
                                    p = (DateTime.Today.Month + 1);
                                    if (p == 13) { p = 1; }
                                    stack[i] = p.ToString();
                                    break;

                            }
                        }
                        else
                        {
                            switch (stack[i].ToLower())
                            {

                                //evaluators...
                                case "=":
                                case "<>":
                                case "!=":
                                case "==":
                                case ">":
                                case "<":

                                    break;
                                case "and":
                                case "or":

                                    break;

                                default:
                                    //just a regular term, look for it in data
                                    if (data.ContainsKey(stack[i].ToLower()))
                                    {
                                        stack[i] = data[stack[i]];
                                    }
                                    break;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                Error("Templating.Logical.IF.Parse:" + ex.Message);
            }


            //next, evaluate the stack...

            try
            {

                for (int i = 0; i <= stack.Length - 1; i++)
                {
                    switch (stack[i].ToLower())
                    {

                        case "=":
                        case "==":
                            if (Tools.IsDate(stack[i - 1]) & Tools.IsDate(stack[i + 1]))
                            {
                                if (Tools.CompareDate(stack[i - 1], stack[i + 1]) != 0)
                                {
                                    OK = false;
                                }
                            }
                            else
                            {
                                if (!(stack[i - 1] == stack[i + 1]))
                                {
                                    OK = false;
                                }
                            }

                            break;
                        case "!=":
                        case "<>":

                            if (Tools.IsDate(stack[i - 1]) & Tools.IsDate(stack[i + 1]))
                            {
                                if (Tools.CompareDate(stack[i - 1], stack[i + 1]) == 0)
                                {
                                    OK = false;
                                }
                            }
                            else
                            {
                                if (stack[i - 1] == stack[i + 1])
                                {
                                    OK = false;
                                }
                            }

                            break;
                        case ">":

                            if (Tools.IsDate(stack[i - 1]) & Tools.IsDate(stack[i + 1]))
                            {
                                if (Tools.CompareDate(stack[i - 1], stack[i + 1]) < 1)
                                {
                                    OK = false;
                                }
                            }
                            else
                            {
                                if (stack[i - 1].CompareTo(stack[i + 1]) > 0)
                                {
                                    OK = false;
                                }
                            }

                            break;
                        case "<":

                            if (Tools.IsDate(stack[i - 1]) & Tools.IsDate(stack[i + 1]))
                            {
                                if (Tools.CompareDate(stack[i - 1], stack[i + 1]) > -1)
                                {
                                    OK = false;
                                }
                            }
                            else
                            {
                                if (stack[i - 1].CompareTo(stack[i + 1]) < 0)
                                {
                                    OK = false;
                                }
                            }

                            break;
                        case "or":
                            if (OK)
                                break;

                            break;
                    }
                }


            }
            catch (Exception ex)
            {
                Error("Templating.Logical.IF.Eval:" + ex.Message);
            }
            return OK;
        }

        //===============================================================================================

        /// <summary>
        /// Formats data in the passed dictionary into the referenced or passed template. 
        /// </summary>
        /// <param name="template">Fully qualified path to the template or the template as a string</param>
        /// <param name="Data">Dictionary containing key|value data to be pressed into the template.</param>
        /// <returns>String - the processed template and data.</returns>
        public string Format(string template, Dictionary<string, string> Data)
        {
            template = xResolveTemplate(template);
            //this method formats the dictionary as the data source
            foreach (KeyValuePair<string, string> item in Data)
            {
                template = xMergeField(template, item.Key, item.Value);
            }
            return template;
        }
        /// <summary>
        /// Formats data in the passed dictionary into the referenced or passed template. 
        /// </summary>
        /// <param name="template">Fully qualified path to the template or the template as a string</param>
        /// <param name="Data">Dictionary containing key|value data to be pressed into the template.</param>
        /// <returns>String - the processed template and data.</returns>
        public string Format(string template, Dictionary<string, object> Data)
        {
            template = xResolveTemplate(template);
            //this method formats the dictionary as the data source
            foreach (KeyValuePair<string, object> item in Data)
            {
                template = xMergeField(template, item.Key, item.Value.ToString());
            }
            return template;
        }

        /// <summary>
        /// Formats data into the referenced or passed template, which includes a query node.  Uses the Parameters dictionary to 
        /// situationally modify the query.
        /// </summary>
        /// <param name="template">Fully qualified path to the template or the template as a string</param>
        /// <param name="Parameters">Dictionary containing key|value parameters to modify the query in the template</param>
        /// <returns>String - the processed template and data.</returns>
        public string Results(string template, Dictionary<string, string> Parameters)
        {
            template = xResolveTemplate(template);
            string results = "";
            //this method has no SQL, counting on that being in the template in either query or data nodes
            //the dictionary can contain replacable [tokens] that will be processed through the resulting
            //query to give the final, executable version.  
            string SQLStatement = Tools.XNode(ref template, "query").Trim();
            if (SQLStatement.Length == 0)
            {
                SQLStatement = Tools.XNode(ref template, "data").Trim();
            }
            SQLStatement = xResolveSQL(SQLStatement, Parameters);

            //go get the datatable to pass to the local worker process...
            DataTools dt = new DataTools();
            dt.Provider = _provider;
            dt.OpenConnection(ConnectionString);
            dt.GetResultSet(SQLStatement);
            results = xResults(dt.ResultSet, template);

            dt.Dispose();
            return results;
        }


        /// <summary>
        /// Formats data into the referenced or passed template, which includes a query node.  Uses the Parameters dictionary to 
        /// situationally modify the query.
        /// </summary>
        /// <param name="SQLStatement">SQL select statement to execute, fully qualified</param>
        /// <param name="template">Template name, path, or string</param>
        /// <returns>String - formated data according to the template specifications</returns>
        public string Results(string SQLStatement, string template)
        {
            template = xResolveTemplate(template);
            string results = "";
            //this method has a SQL Statement being passed to it
            //go get the datatable to pass to the local worker process...
            DataTools dt = new DataTools();
            dt.Provider = _provider;
            dt.OpenConnection(ConnectionString);
            dt.GetResultSet(SQLStatement);
            if (dt.haserrors)
            {
                Error("Results." + dt.errors);
            }
            results = xResults(dt.ResultSet, template);
            dt.Dispose();
            return results;
        }

        /// <summary>
        /// Formats data into the referenced or passed template, which includes a query node.  Uses the Parameters dictionary to 
        /// situationally modify the query.
        /// </summary>
        /// <param name="data">DataTable of data you want formatted/templated</param>
        /// <param name="template">Template name, path, or string</param>
        /// <returns>String - formated data according to the template specifications</returns>
        public string Results(DataTable data, string template)
        {

            template = xResolveTemplate(template);
            return xResults(data, template);
        }

        public string ResolveTemplate(string template)
        {
            return xResolveTemplate(template);
        }

        private string xResolveTemplate(string template)
        {

            string tmp = template;
            int p = 0;
            bool resolved = false;
            //template could be a reference to a file...
            //if the reference is short, perhaps it is just the name of the template?
            //lets check that...
            if (tmp.Length < 260)
            {
                p = tmp.LastIndexOf('.');
                if (p == -1)
                {
                    //no extension, add one
                    tmp += ".dbt";
                }
                if (Tools.FileExists(_templatepath + tmp))
                {
                    tmp = Tools.ReadFile(_templatepath + tmp).Trim();
                    resolved = true;
                }
                else
                {
                    tmp = template;
                }
            }

            if (!resolved)
            {
                if (Tools.FileExists(tmp))
                {
                    //well that answers that...
                    tmp = Tools.ReadFile(tmp).Trim();
                }
            }
            tmp = dbtinner(tmp);
            return tmp;
        }

        /// <summary>
        /// Given a block of text containing SQL statements, separated by semicolons, and each containing tokens, return the statement that matches
        /// the signature represented by the paramlist dictionary, and resolve the tokens with the key value pairs.
        /// </summary>
        /// <param name="SQLStack">String containing one or more SQL statements delimited by semicolons.</param>
        /// <param name="paramlist">Dictionary of key|value pairs, keys being the token names that could be contained in each SQL statement</param>
        /// <returns>The resolved statement that matches the signiture specified by the key|value pairs, or the first statement if no signature match is found.</returns>
        /// <remarks>If using multiple SQL statements within a data block, order your statements from least complex to most complex (by token count).  
        /// Typically the tokens are used in where criteria - the more specific criteria should be listed last.</remarks>
        public string ResolveSQLStatement(string SQLStack, Dictionary<string, string> paramlist)
        {
            return xResolveSQL(SQLStack, paramlist);
        }

        private string xResolveSQL(string SQLStack, Dictionary<string, string> paramlist)
        {
            string results = "";
            bool sigmatch = false;
            string[] sql = SQLStack.Split(';');
            string thissql = "";
            List<string> tokens = new List<string>();

            for (int i = sql.Length - 1; i >= 0; i--)
            {
                thissql = sql[i].Trim();
                if (thissql.Length > 0)
                {
                    if (thissql.IndexOf(" ") == -1)
                    {
                        //in the external sql file?
                        //this should be in the default path
                        //we should be able to also swap out portions of this based on path.
                        thissql = Tools.ReadBlock(_basepath + "resources\\data\\scampsweb.sql", thissql);
                    }
                    tokens = Tools.TokenList(thissql);
                    sigmatch = Tools.SignatureCompare(paramlist, tokens, Options.DisallowZeroLengthValuesOnSignature);
                    if (sigmatch)
                    {
                        //yippie, we got a match!
                        results = thissql.Trim();
                        foreach (string token in tokens)
                        {
                            results = xMergeField(results, token, Tools.SQLSafe(paramlist[token]));
                        }
                        break;
                    }
                }
            }

            if (!sigmatch)
            {
                Message("No SQL statement matched the parameter signature.  Returning the default (first) statement.");
                results = sql[0];
            }


            //resolve SQL statement embedded parameters
            foreach (KeyValuePair<string, string> entry in parameters)
            {
                results = xMergeField(results, entry.Key, entry.Value);
            }
            foreach (KeyValuePair<string, string> entry in qs)
            {
                results = xMergeField(results, entry.Key, entry.Value);
            }

            tokens = null;

            return results;
        }



        private string xResults(DataTable datablock, string template)
        {
            string results = "";
            StringBuilder sb = new StringBuilder();

            Dictionary<string, string> reference = new Dictionary<string, string>();
            Dictionary<string, string> nodes = new Dictionary<string, string>();

            //template stuff
            string wrapper = Tools.XNode(ref template, "wrapper");
            string subwrapper = "";
            string rowtmp = Tools.XNode(ref template, "template");
            string noresults = Tools.XNode(ref template, "noresults");

            //if you want to ignore the wrapper entirely, use norecords
            string norecords = Tools.XNode(ref template, "norecords");

            string tableize = Tools.XNode(ref template, "tableize");

            string conditionals = Tools.XNode(ref template, "conditional");
            Scripter s = new Scripter();
            List<Scripter.expression> ex = s.Compile(conditionals);


            if (tableize.Length > 0)
            {
                results = Tableize(tableize);
                subwrapper = Tools.XNode(ref results, "wrapper");
                rowtmp = Tools.XNode(ref results, "template");
            }

            if (rowtmp.Length == 0)
            {
                rowtmp = template;
            }
            string lcltmp = rowtmp;

            //data stuff
            DataRow thisrow;
            int rowcount = datablock.Rows.Count;
            int colcount = datablock.Columns.Count;
            string key = "";
            string val = "";
            bool doit = false;
            string lval = "";
            string rval = "";
            string[] options;
            string argL = "";
            string argR = "";

            if (rowcount == 0)
            {
                sb.Append(noresults);
            }
            else
            {
                for (int i = 0; i < rowcount; i++)
                {
                    thisrow = datablock.Rows[i];
                    lcltmp = rowtmp;

                    //process conditional expressions...
                    for (int c = 0; c < ex.Count; c++)
                    {

                        doit = false;

                        //pull field values to set left and right references...
                        if (ex[c].leftIsFieldReference)
                        {
                            argL = ex[c].leftHandArgument.ToString().ToLower();
                            try
                            {

                                //ok, this is where we need to possibly switch out some other variables
                                //that we can support within a template, querystring, page data, parameters, etc.
                                //we want to be selective and also force a specific reference within the 
                                //source conditional template expression
                                if (argL.StartsWith("query."))
                                {
                                    lval = Tools.GetValue(argL.Substring(6),qs);
                                }
                                else
                                {
                                    if (argL.StartsWith("data."))
                                    {
                                        lval = Tools.GetValue(argL.Substring(5), data);
                                    }
                                    else
                                    {
                                        lval = thisrow[argL].ToString();
                                    }
                                }
                            }
                            catch
                            {
                                lval = "";
                            }

                        }
                        else
                        {
                            lval = ex[c].leftHandArgument;
                        }

                        if (ex[c].rightIsFieldReference)
                        {

                            argR = ex[c].rightHandArgument.ToString().ToLower();
                            try {
                                if (argR.StartsWith("query."))
                                {
                                    rval = Tools.GetValue(argR.Substring(6),qs);
                                }
                                else
                                {
                                    if (argR.StartsWith("data."))
                                    {
                                        lval = Tools.GetValue(argR.Substring(5), data);
                                    }
                                    else
                                    {
                                        lval = thisrow[argR].ToString();
                                    }
                                }
                            }

                            catch
                            {
                                rval = "";
                            }

                        }
                        else
                        {
                            rval = ex[c].rightHandArgument;
                        }

                        switch (ex[c].keyword)
                        {
                            case "if":
                                doit = _compare(lval, ex[c].logicOperator, rval);
                                break;
                            case "onchange":
                                lval = thisrow[ex[c].leftHandArgument].ToString();
                                if (ex[c].conditionIsFieldReference)
                                {
                                    if (reference.ContainsKey(ex[c].condition))
                                    {
                                        //this is a field reference and it is in the dictionary
                                        //look to see it the value has changed...
                                        if (reference[ex[c].condition] != lval)
                                        {
                                            //different, doit
                                            reference[ex[c].condition] = lval;
                                            doit = true;
                                        }
                                    }
                                    else
                                    {
                                        //no key in the dictionary.  Hey now..
                                        reference.Add(ex[c].condition, lval);
                                        doit = true;
                                    }
                                }
                                break;

                            case "each":

                                if (((i + 1) % Tools.AsInteger(lval)) == 0)
                                {
                                    doit = true;
                                }
                                break;

                            case "after":

                                if ((i + 1) > Tools.AsInteger(lval))
                                {
                                    doit = true;
                                }
                                break;

                            case "oneven":

                                if ((i + 1) % 2 == 0)
                                {
                                    doit = true;
                                }
                                break;

                            case "onodd":

                                if ((i + 1) % 2 != 0)
                                {
                                    doit = true;
                                }
                                break;
                        }

                        if (doit)
                        {
                            //second half of eval.  yummy...
                            switch (ex[c].action)
                            {
                                case "addclass":
                                    //class and selector, in that order.  No spaces!
                                    options = ex[c].value.Split(' ');
                                    if (options.Length > 1)
                                    {
                                        lcltmp = Tools.AddClass(lcltmp, options[0], options[1]);
                                    }
                                    break;

                                case "use":

                                    if (nodes.ContainsKey(ex[c].value))
                                    {
                                        lcltmp = nodes[ex[c].value];
                                    }
                                    else
                                    {
                                        if (ex[c].value.Length > 0)
                                        {
                                            nodes.Add(ex[c].value, Tools.XNode(ref template, ex[c].value));
                                            lcltmp = nodes[ex[c].value];
                                        }
                                    }
                                    break;

                                case "insert":
                                    if (nodes.ContainsKey(ex[c].value))
                                    {
                                        lcltmp = nodes[ex[c].value] + System.Environment.NewLine + lcltmp;
                                    }
                                    else
                                    {
                                        if (ex[c].value.Length > 0)
                                        {
                                            nodes.Add(ex[c].value, Tools.XNode(ref template, ex[c].value));
                                            lcltmp = nodes[ex[c].value] + System.Environment.NewLine + lcltmp;
                                        }
                                    }
                                    break;

                                case "skip":

                                    lcltmp = "";
                                    break;

                                case "append":
                                    if (nodes.ContainsKey(ex[c].value))
                                    {
                                        lcltmp = lcltmp + System.Environment.NewLine + nodes[ex[c].value];
                                    }
                                    else
                                    {
                                        if (ex[c].value.Length > 0)
                                        {
                                            nodes.Add(ex[c].value, Tools.XNode(ref template, ex[c].value));
                                            lcltmp = lcltmp + System.Environment.NewLine + nodes[ex[c].value];
                                        }
                                    }
                                    break;

                                case "wrap":

                                    break;
                            }
                        }
                    }


                    for (int c = 0; c < colcount; c++)
                    {
                        key = thisrow.Table.Columns[c].ColumnName;
                        val = thisrow.ItemArray[c].ToString();
                        lcltmp = xMergeField(lcltmp, key, val);
                    }

                    lcltmp = xMergeField(lcltmp, "rownumber", (i + 1).ToString());
                    sb.Append(lcltmp);
                }
            }


            results = sb.ToString();


            if (subwrapper.Length > 0)
            {
                results = subwrapper.Replace("{0}", results);
            }
            if (wrapper.Length > 0)
            {
                results = wrapper.Replace("{0}", results);
            }

            //if no records in the datatable and there is a norecords node in the
            //template, overwrite the results with just the contents of the norecords node
            if (rowcount == 0 && norecords.Length > 0)
            {
                results = norecords;
            }

            //changed to follow single code path for all token swapping - zaphod 2015-08
            results = xMergeField(results,"rowcount", datablock.Rows.Count.ToString());

            //add in a global swap out of other variables that could be passed to the system
            //and finally, remove all remaining token blocks that are not resolved.
            foreach (KeyValuePair<string, string> entry in session)
            {
                results = xMergeField(results, entry.Key, entry.Value);
            }

            foreach (KeyValuePair<string, string> entry in qs)
            {
                results = xMergeField(results, entry.Key, entry.Value);
            }

            foreach (KeyValuePair<string, string> entry in parameters)
            {
                results = xMergeField(results, entry.Key, entry.Value);
            }

            foreach (KeyValuePair<string, string> entry in data)
            {
                results = xMergeField(results, entry.Key, entry.Value);
            }


            //now figure totals and averages based on a column name.
            string token = "";
            string field = "";
            decimal value = 0;
            int lc = 0;  //loop count for emergency exit...

            // TOTALS
            token = Tools.StringPart(results, "[total.", "]");
            field = fieldonly(token);
            while (!token.IsNullOrEmpty())
            {
                lc += 1;
                try
                {
                    value = Tools.AsDecimal(datablock.Compute("sum(" + field + ")", ""));
                }
                catch (Exception iex) {

                    Error("xResults-Total Calculations:" + iex.Message);
                    results += "<span class=\"scamp_internal\">" + "xResults-Total Calculations:" + iex.Message + "</span>";
                }

                results = MergeField(results, "total." + field, value.ToString());
                if (lc > 10)
                {
                    Message("xResults-Total Calculations: max of 10 total calculations reached in the template, exiting");
                    break;
                }
                token = Tools.StringPart(results, "[total.", "]");
                field = fieldonly(token);
            }


            // AVERAGE
            token = Tools.StringPart(results, "[average.", "]");
            field = fieldonly(token);
            while (!token.IsNullOrEmpty())
            {
                lc += 1;
                try
                {
                    value = Tools.AsDecimal(datablock.Compute("avg(" + field + ")", ""));
                }
                catch (Exception iex)
                {

                    Error("xResults-Average Calculations:" + iex.Message);
                    results += "<span class=\"scamp_internal\">" + "xResults-Average Calculations:" + iex.Message + "</span>";
                }

                results = MergeField(results, "average." + field, value.ToString());
                if (lc > 10)
                {
                    Message("xResults-Average Calculations: max of 10 total calculations reached in the template, exiting");
                    break;
                }
                token = Tools.StringPart(results, "[average.", "]");
                field = fieldonly(token);
            }

            results = _commonreference(results);

            nodes.Clear();
            nodes = null;
            reference.Clear();
            reference = null;
            template = "";
            lcltmp = "";
            wrapper = "";

            return results;

        }

        private string _commonreference(string source)
        {
            source = source.Replace("[this.year]", DateTime.Now.Year.ToString());
            source = source.Replace("[last.year]", (DateTime.Now.Year - 1).ToString());
            source = source.Replace("[this.month]", DateTime.Now.ToString("MMMM"));

            return source;
        }

        private bool _compare(string left, string logicoperator, string right)
        {

            int test = 0;
            bool results = false;

            if (Tools.isNumeric(left) & Tools.isNumeric(right))
            {
                //numeric compare

                switch (logicoperator)
                {
                    case "==":
                    case "=":
                        if (Tools.AsDecimal(left) == Tools.AsDecimal(right))
                            results = true;
                        else
                            results = false;

                        break;

                    case "!=":
                    case "!":
                    case "<>":
                        if (Tools.AsDecimal(left) == Tools.AsDecimal(right))
                            results = false;
                        else
                            results = true;

                        break;

                    case "<":
                        if (Tools.AsDecimal(left) < Tools.AsDecimal(right))
                            results = true;
                        else
                            results = false;
                        break;


                    case ">":
                        if (Tools.AsDecimal(left) > Tools.AsDecimal(right))
                            results = true;
                        else
                            results = false;

                        break;


                    case "<=":
                    case "=<":
                        if (Tools.AsDecimal(left) <= Tools.AsDecimal(right))
                            results = true;
                        else
                            results = false;

                        break;

                    case ">=":
                    case "=>":
                        if (Tools.AsDecimal(left) >= Tools.AsDecimal(right))
                            results = true;
                        else
                            results = false;

                        break;

                    case ">>":
                        if (right.IndexOf(left) >= 1)
                            results = true;
                        else
                            results = false;

                        break;
                }
            }
            else if (Tools.IsDate(left) & Tools.IsDate(right))
            {
                test = DateTime.Compare(DateTime.Parse(left), DateTime.Parse(right));
                switch (logicoperator)
                {
                    case "==":
                    case "=":
                        if (test == 0)
                            results = true;
                        else
                            results = false;

                        break;

                    case "!=":
                    case "!":
                    case "<>":
                        if (test != 0)
                            results = false;
                        else
                            results = true;

                        break;

                    case "<":
                        if (test < 0)
                            results = true;
                        else
                            results = false;
                        break;

                    case ">":
                        if (test > 0)
                            results = true;
                        else
                            results = false;
                        break;

                    case "<=":
                    case "=<":
                        if (test <= 0)
                            results = true;
                        else
                            results = false;
                        break;

                    case ">=":
                    case "=>":
                        if (test >= 0)
                            results = true;
                        else
                            results = false;
                        break;
                }
            }
            else
            {
                switch (logicoperator)
                {
                    case "==":
                    case "=":
                        if (left == right)
                            results = true;
                        else
                            results = false;

                        break;

                    case "!=":
                    case "!":
                    case "<>":
                        if (left == right)
                            results = false;
                        else
                            results = true;

                        break;

                    case "<":
                        test = left.CompareTo(right);
                        if (test < 0)
                            results = true;
                        else
                            results = false;
                        break;

                    case ">":
                        test = left.CompareTo(right);
                        if (test > 0)
                            results = true;
                        else
                            results = false;
                        break;

                    case "<=":
                    case "=<":
                        test = left.CompareTo(right);
                        if (test <= 0)
                            results = true;
                        else
                            results = false;
                        break;

                    case ">=":
                    case "=>":
                        test = left.CompareTo(right);
                        if (test >= 0)
                            results = true;
                        else
                            results = false;
                        break;

                    case ">>":
                        if (right.IndexOf(left) >= 1)
                            results = true;
                        else
                            results = false;

                        break;
                }
            }

            return results;
        }



        public string MergeField(string template, string key, string value)
        {
            return xMergeField(template, key, value);
        }


        /// <summary>
        /// Press a key/value into a template using the standard [key] notation
        /// </summary>
        /// <param name="template">The template</param>
        /// <param name="key">key name</param>
        /// <param name="value">key data value</param>
        /// <returns></returns>
        private string xMergeField(string template, string key, string value)
        {

            string lt = template;
            string directive = "";
            string resultcode = "";
            int i = 0;
            int n = 0;
            int lc = 0;

            key = key.ToLower();

            //base simple replace
            lt = lt.Replace("[" + key + "]", value);

            //with directives...
            n = lt.IndexOf("[" + key + ":");
            while (n > -1)
            {
                lc += 1;
                i = lt.IndexOf("]", n + 1);
                if (i > -1)
                {
                    n += key.Length + 2;
                    directive = lt.Substring(n, i - n);
                    resultcode = ProcessDirective(directive, value);
                    lt = lt.Replace("[" + key + ":" + directive + "]", resultcode);
                }
                n = lt.IndexOf("[" + key + ":");
                if (lc > 30)
                {
                    break;
                }
            }

            return lt;
        }

        public string Tableize(string template)
        {
            string fields = Tools.XNode(ref template, "columns");
            string labels = Tools.XNode(ref template, "labels");
            string classes = Tools.XNode(ref template, "classes");

            string css = Tools.XNode(ref template, "css").Trim();
            string tableid = Tools.XNode(ref template, "id");
            string tableclass = Tools.XNode(ref template, "tableclass");

            string colref = "";
            string tmp = "";
            string block = "";
            string fc = "";     //field class, if not switched off
            int p = 0;
            string wrapper = "";
            char splitchar = ',';
            if (Options.TableizeDelimitOnPipes) { splitchar = '|'; }

            //handle link references and a url pattern match
            Dictionary<string, string> links = new Dictionary<string, string>();
            string link = Tools.XNode(ref template, "link");
            while (link.Length > 0)
            {
                links.Add(Tools.XNode(ref link, "on"), Tools.XNode(ref link, "url"));
                link = Tools.XNode(ref template, "link");
            }

            string results = "";
            StringBuilder sb = new StringBuilder();

            string[] aO = labels.Split(splitchar);
            string[] cO = classes.Split(splitchar);

            //wrap css in style tag if we have any...
            if (css.Length > 0)
            {
                css = "<style>" + System.Environment.NewLine + css + System.Environment.NewLine + "</style>";
                sb.Append(css + System.Environment.NewLine);
            }
            //table, table head
            if (tableclass.Length > 0)
            {
                tableclass += " tableize";
            }
            else
            {
                tableclass = "tableize";
            }
            if (tableid.Length > 0)
            {
                tableid = " id=\"" + tableid + "\"";
            }
            sb.Append("<table" + tableid + " class=\"" + tableclass + "\" cellpadding=\"0\" cellspacing=\"0\">" + System.Environment.NewLine);
            sb.Append("\t<thead><tr>" + System.Environment.NewLine);
            for (int i = 0; i < aO.Length; i++)
            {
                sb.Append("\t\t<th>" + aO[i].Trim() + "</th>" + System.Environment.NewLine);
            }

            sb.Append("\t</tr></thead>" + System.Environment.NewLine);
            sb.Append("\t<tbody>" + System.Environment.NewLine);

            sb.Append("{0}");

            sb.Append("\t</tbody>" + System.Environment.NewLine);
            sb.Append("</table>" + System.Environment.NewLine);

            wrapper = "<wrapper>" + sb.ToString() + "</wrapper>";
            sb.Length = 0;


            //now do the template part of this
            sb.Append("\t\t<tr>" + System.Environment.NewLine);

            fields = fields.Replace("\\,", "<~C~>");
            aO = fields.Split(splitchar);

            for (int i = 0; i < aO.Length; i++)
            {
                //restore eacaped values
                aO[i] = aO[i].Replace("<~C~>", ",");
                colref = aO[i].Trim();
                if (!Options.TableizeSupressFieldClasses) { fc = " " + fieldonly(aO[i]); }
                block = "";
                if (i == 0)
                {
                    if (fc.Length > 0)
                    {
                        fc = fc + " firstcell";
                    }
                    else
                        fc = "firstcell";
                }
                if (i == aO.Length - 1)
                {
                    if (fc.Length > 0)
                    {
                        fc = fc + " lastcell";
                    }
                    else
                    {
                        fc = "lastcell";
                    }
                }

                if (i < cO.Length && cO[i].Trim().Length > 0)
                {
                    block = " class=\"" + cO[i] + fc + "\"";
                }
                else if (fc.Length > 0)
                {
                    block = " class=\"" + fc.Trim() + "\"";
                }

                if (links.ContainsKey(colref))
                {
                    //we have a link for this field by name
                    tmp = links[colref].Trim();  //the url pattern...
                    sb.Append("\t\t\t<td" + block + "><a href=\"" + tmp + "\">[" + aO[i] + "]</td>" + System.Environment.NewLine);
                }
                else
                {
                    sb.Append("\t\t\t<td" + block + ">[" + aO[i] + "]</td>" + System.Environment.NewLine);
                }

            }

            sb.Append("\t\t</tr>" + System.Environment.NewLine);
            tmp = "<template>" + sb.ToString() + "</template>";
            results = wrapper + System.Environment.NewLine + tmp;
            return results;
        }

        private string fieldonly(string source)
        {
            //remove any directives from a field for using as a class name;
            int p = source.IndexOf(':');
            if (p > -1)
            {
                source = source.Substring(0, p);
            }
            return source;
        }
        public string ProcessDirective(string directive, string value)
        {
            string results = value;
            string strOperation = "";
            int pc = 0;
            //escape the : in directive list
            directive = directive.Replace("\\:", "``~``");

            string[] aOP = directive.Split(':');
            string[] plist;
            try
            {
                for (int i = 0; i < aOP.Length; i++)
                {
                    strOperation = aOP[i].Replace("``~``", "\\:");
                    plist = Parameters(ref strOperation);
                    pc = plist.Length;

                    strOperation = strOperation.ToLower();

                    switch (strOperation)
                    {
                        case "nextcolor":
                            results = NextColor();
                            break;
                        case "shiftcolorup":
                            results = ShiftColorUp();
                            break;

                        case "shiftcolordown":
                            results = ShiftColorDown();
                            break;
                        //date/time methods
                        case "formatdate":
                        case "customdate":
                            if (pc > 0)
                            {
                                //TODO:AsDate needs to be externalized...
                                results = Tools.AsDate(results, plist[0]);
                            }
                            break;

                        case "ago":
                            results = Tools.Ago(results);
                            break;

                        //numbers...

                        case "currency":
                            //results = string.Format("{0:C}", results);
                            results = float.Parse(results).ToString("C");
                            break;

                        case "round":
                            if (pc == 0)
                            {
                                results = Math.Round(double.Parse(results), 0).ToString();
                            }
                            else
                            {
                                results = Math.Round(double.Parse(results), int.Parse(plist[0])).ToString();
                            }
                            break;

                        case "number":
                            if (pc == 0)
                            {
                                //no parameters, assume a standard double
                                results = String.Format("{0:###,###,###,###.##}", Tools.AsDouble(results));
                            }
                            else
                            {
                                results = String.Format("{0:" + plist[0] + "}", Tools.AsDouble(results));
                            }
                            break;

                        case "int":
                            results = int.Parse(results).ToString();
                            break;

                        case "integer":
                            if (pc == 0)
                            {
                                //no parameters, assume a standard double
                                results = String.Format("{0:###,###,###,###}", Tools.AsInteger(results));
                            }
                            else
                            {
                                results = String.Format("{0:" + plist[0] + "}", Tools.AsInteger(results));
                            }
                            break;

                        //Strings...

                        //first character, as is
                        case "first":
                        case "firstletter":
                            results = results.Substring(0, 1);
                            break;

                        //first character, capitalized.
                        case "initial":
                        case "firstinitial":
                            results = results.Substring(0, 1).ToUpper();
                            break;

                        case "lowercase":
                        case "tolower":
                            results = results.ToLower();
                            break;

                        case "uppercase":
                        case "toupper":
                            results = results.ToUpper();
                            break;

                        case "propercase":
                        case "proper":
                            results = Tools.ProperCase(results, true);
                            break;

                        // respect possible anacronyms
                        case "toproper":
                        case "properupper":
                            results = Tools.ProperCase(results);
                            break;
                        case "padleft":

                            switch (pc)
                            {
                                case 1:
                                    //only take and user first parameter as total width, use space character.
                                    results = results.PadLeft(int.Parse(plist[0]), ' ');
                                    break;

                                case 2:
                                    //first two, character and total width.

                                    if (plist[0].ToLower() == "&nbsp;")
                                    {
                                        results = Tools.StringOf("&nbsp;", int.Parse(plist[1])) + results;
                                    }
                                    else
                                    {
                                        results = results.PadLeft(int.Parse(plist[1]), char.Parse(plist[0].Substring(0, 1)));
                                    }
                                    break;

                                default:

                                    break;
                            }
                            break;

                        case "padright":

                            switch (pc)
                            {
                                case 1:
                                    //only take and user first parameter as total width, use space character.
                                    results = results.PadRight(int.Parse(plist[0]), ' ');
                                    break;

                                case 2:
                                    //first two, character and total width.
                                    if (plist[0].ToLower() == "&nbsp;")
                                    {
                                        results = results + Tools.StringOf("&nbsp;", int.Parse(plist[1]));
                                    }
                                    else
                                    {
                                        results = results.PadRight(int.Parse(plist[1]), char.Parse(plist[0].Substring(0, 1)));
                                    }
                                    break;

                                default:

                                    break;
                            }
                            break;

                        case "wrap":
                            results = results.Trim();
                            if (results.Length > 0)
                            {
                                if (pc > 0)
                                {
                                    //first param is text BEFORE
                                    results = plist[0] + results;
                                }
                                if (pc > 1)
                                {
                                    //second param is text AFTER
                                    results = results + plist[1];
                                }
                            }
                            else
                            {
                                if (pc > 2)
                                {
                                    //if results are empty AND there is a third "default" parameter, return that.
                                    results = plist[2];
                                }
                            }
                            break;

                        case "replace":
                            for (int ii = 0; ii < plist.Length; ii += 2)
                            {
                                results = results.Replace(plist[ii], plist[ii + 1]);
                            }
                            break;

                        case "lookup":
                            string tmp = "";
                            try
                            {
                                for (int ii = 0; ii < plist.Length; ii += 2)
                                {
                                    if (results == plist[ii])
                                    {
                                        tmp = plist[ii + 1];
                                        break;
                                    }
                                }
                                if (tmp.Length == 0)
                                {
                                    //no match found, is there a default?
                                    if (Tools.isOdd(pc))
                                    {
                                        tmp = plist[plist.Length - 1];
                                    }
                                    else
                                    {
                                        //all else fails, return the original value...
                                        tmp = results;
                                    }
                                }
                            }
                            catch
                            {
                                Error("FormatDirective.lookup: Check your parameter list for matching pairs and correct formatting");
                            }
                            results = tmp;
                            break;

                        case "in":
                            //results is one of a list of possible values, or it is none of them
                            bool ok = false;
                            for (int ii = 0; ii < plist.Length; ii++)
                            {
                                if (plist[ii].ToLower() == results.ToLower())
                                {
                                    ok = true;
                                }
                            }
                            if (!ok)
                            { results = ""; }
                            break;

                        case "is":
                            switch (plist[0].ToLower())
                            {
                                case "letters":
                                    if (!Tools.isLetters(results))
                                    {
                                        results = "";
                                    }
                                    break;

                                case "letter":
                                    if (results.Length > 0) { results = results.Substring(0, 1); }
                                    if (!Tools.isLetters(results))
                                    {
                                        results = "";
                                    }
                                    break;

                                case "numbers":
                                case "digits":
                                    if (!Tools.isDigits(results))
                                    {
                                        results = "";
                                    }
                                    break;

                                case "digit":
                                    if (results.Length > 0) { results = results.Substring(0, 1); }
                                    if (!Tools.isDigits(results))
                                    {
                                        results = "";
                                    }
                                    break;

                                case "lettersandnumbers":
                                case "letters+numbers":
                                    if (!Tools.isLettersOrDigits(results))
                                    {
                                        results = "";
                                    }
                                    break;

                                default:

                                    //none of the defined groups, just get an intersection
                                    //and drop results if the intersect fails.
                                    if (!Tools.Intersect(plist[0], results))
                                    {
                                        results = "";
                                    }
                                    break;
                            }

                            break;
                        case "listescape":
                        case "listify":
                            //this is so that commas,colons, and pipes can work in a list template
                            results = results.Replace(",", "&#44;");
                            results = results.Replace(":", "&#58;");
                            results = results.Replace("|", "&#124;");
                            break;

                        case "clean":
                        case "urlstring":
                            results = Tools.URLString(results);
                            break;

                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Error("Directive." + strOperation + ":" + ex.Message);
            }


            return results;
        }

        private string[] Parameters(ref string directive)
        {
            int p = 0;
            int n = 0;
            string[] parameters = { };
            string tmp = "";
            string lcldirective = directive;

            //escape commas in directive
            lcldirective = lcldirective.Replace("\\,", "++~++");

            p = lcldirective.IndexOf("(");
            if (p > -1)
                tmp = lcldirective.Substring(0, p).Trim();
            {
                n = lcldirective.IndexOf(")", p + 1);
                if (n > -1)
                {
                    parameters = lcldirective.Substring(p + 1, n - (p + 1)).Split(',');
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        parameters[i] = parameters[i].Replace("++~++", ",");
                    }
                    directive = tmp;
                }
                else
                {
                    //no closing parens
                }
            }
            return parameters;
        }
    }
}
