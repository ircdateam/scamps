﻿using System.Collections.Generic;

namespace Scamps
{
    public static class IDictionaryExtensions
    {
        public static void AddOrReplace<K, V>(this IDictionary<K, V> dictionary, K key, V replacement)
        {
            if (dictionary.ContainsKey(key))
                dictionary[key] = replacement;
            dictionary.Add(key, replacement);
        }
    }
}
