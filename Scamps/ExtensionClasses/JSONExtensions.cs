﻿using System;
using System.Collections;
using Newtonsoft.Json;

namespace Scamps
{
    public static class JsonTextWriterExtensions
    {
        public static JsonTextWriter WrtPropertyName(this JsonTextWriter w, string name)
        {
            w.WritePropertyName(name);
            return w;
        }
        public static JsonTextWriter WrtValue(this JsonTextWriter w, string value)
        {
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtWriteDictionary(this JsonTextWriter w, IDictionary idc)
        {
            w.WriteRawValue(JsonConvert.SerializeObject(idc));
            return w;
        }
        public static JsonTextWriter WrtWriteEnum(this JsonTextWriter w, IEnumerable ienum)
        {
            w.WriteStartArray();
            foreach (var o in ienum)
                w.WriteValue(o);
            w.WriteEndArray();
            return w;
        }
        public static JsonTextWriter WrtPropertyValue(this JsonTextWriter w, string name, string value)
        {
            w.WritePropertyName(name);
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtPropertyValue(this JsonTextWriter w, string name, bool value)
        {
            w.WritePropertyName(name);
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtPropertyValue(this JsonTextWriter w, string name, DateTime value)
        {
            w.WritePropertyName(name);
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtStartObject(this JsonTextWriter w)
        {
            w.WriteStartObject();
            return w;
        }
        public static JsonTextWriter WrtEndObject(this JsonTextWriter w)
        {
            w.WriteEndObject();
            return w;
        }
        public static JsonTextWriter WrtStartArray(this JsonTextWriter w)
        {
            w.WriteStartArray();
            return w;
        }
        public static JsonTextWriter WrtEndArray(this JsonTextWriter w)
        {
            w.WriteEndArray();
            return w;
        }
    }
}
