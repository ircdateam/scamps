﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Scamps
{
    public class Cache
    {
        private string _cachepath = "/cache/";

        public string cachePath
        {
            get { return _cachepath; }

            set
            {
                _cachepath = value;
                if (!_cachepath.EndsWith("/")) { _cachepath += "/"; }
            }
        }

        public Cache()
        {
        }

        public Cache(string CachePath)
        {
            cachePath = CachePath;
        }

        private string ensureHasExtension(string filename, string extensionIfNone = ".cache")
        {
            if (filename != null && !filename.IsNullOrEmpty() && !Path.HasExtension(filename))
                return filename + extensionIfNone;
            else
                return filename;
        }

        /// <summary>
        /// Returns the full path of the cache file.
        /// 
        /// If you do not provide a file extension for the cache file, it will append .cache.
        /// </summary>
        /// <param name="cache"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        private string setPath(string cache, string filename)
        {
            return Path.Combine(_cachepath, cache, ensureHasExtension(filename));
        }

        /// <summary>
        /// Write a file to the cache
        /// </summary>
        /// <param name="data">string data to write to the cache</param>
        /// <param name="file">name of the cache file.  The extension .cache will be added no extension is present.</param>
        /// <param name="cache">The name of the cache, i.e. the cache folder to write the file to.  This is optional.</param>
        /// <returns></returns>
        public bool Set(string data, string file, string cache = "")
        {
            bool ok = false;
            string zfilename = setPath(cache, file);
            try
            {
                Tools.WriteFile(zfilename, data);
                ok = true;
            }
            catch (Exception ex)
            {
                //log here
            }

            return ok;
        }

        public string Get(string file, string cache = "")
        {
            string results = "";
            string zfilename = setPath(cache, file);
            try
            {
                results = Tools.ReadFile(zfilename);
            }
            catch (Exception ex)
            {
                //log here
            }

            return results;
        }


        public bool Exists(string file, string cache = "")
        {
            bool ok = false;
            string zfilename = setPath(cache, file);
            try
            {
                ok = Tools.FileExists(zfilename);
            }
            catch (Exception ex)
            {
                //log here
            }

            return ok;
        }

        /// <summary>
        /// Clear a cache folder, file, or entire cache
        /// </summary>
        /// <param name="pattern">file pattern to remove</param>
        /// <param name="cache">folder (cache) name</param>
        public void Clear(string pattern = "*.*", string cache = "")
        {
            if (pattern.IsNullOrEmpty()) { pattern = "*.*"; }
            string path = setPath(cache, "");
            try
            {
                Tools.DeleteFiles(path,pattern) ;
            }
            catch (Exception ex)
            {
                //log here
            }

        }

        /// <summary>
        /// Given a cache time in minutes, has the given cache file expired?
        /// </summary>
        /// <param name="cachetime">How long, in minutes, a cache is alive</param>
        /// <param name="file">cache file name</param>
        /// <param name="cache">the cache directory, if any</param>
        /// <returns></returns>
        public bool IsExpired(int cachetime,string file, string cache = "")
        {
            double cacheage = Tools.AsDouble(cachetime);
            //System.Diagnostics.Debug.WriteLine("File: {0}, Cachetime: {1}, Age: {2}, IsExpired {3}", file, cachetime, Age(file, cache), Age(file, cache) > cacheage);

            if (Age(file, cache) > cacheage)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public double Age(string file, string cache = "")
        {
            double i = 0;
            string zfilename = setPath(cache, file);
            try
            {
                i = Tools.TimespanSinceLastCache(zfilename).TotalMinutes;
            }
            catch (Exception ex)
            {
                //log here
            }

            return i;
        }






    }
}
