﻿using System;
using System.Data;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Configuration;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ionic.Zip;
using Ionic.Zlib;
using Scamps.Extract.Interfaces;

namespace Scamps.Extract
{
    public class ReadContext : IDisposable
    {
        /// <summary>
        /// an enumerable reader
        /// </summary>
        public IDataReader reader { get; set; }
        /// <summary>
        /// a description of the deliverable. Will typically be a table name or a file name.
        /// </summary>
        public string context { get; set; }

        public void Dispose()
        {
            if (reader != null)
                reader.Dispose();
        }
    }

    #region Configuration File Classes
    /// <summary>
    /// A singleton query provider which allows for SQL queries to be accessed globally.
    /// The file read should be in XML or JSON format.
    /// </summary>
    public class SQLQueryData
    {
        private static readonly SQLQueryData instance = new SQLQueryData();
        public iSQLextract sqldata { get; private set; }

        public string configpath = "";
        public void init()
        {
            var sqldatafile = ConfigurationManager.AppSettings["DEFINITION_FILE"];

            if (sqldatafile.EndsWith(".json", StringComparison.OrdinalIgnoreCase))
                this.sqldata = JSONsql.ReadFile(configpath + sqldatafile);
            else if (sqldatafile.EndsWith(".xml", StringComparison.OrdinalIgnoreCase))
                this.sqldata = XMLsql.ReadFile(configpath + sqldatafile);

            if(sqldata == null)
                throw new Exception(string.Format("Unable to load query configuration file '{0}'", sqldatafile));
        }

        private SQLQueryData(){}

        public static SQLQueryData Instance
        {
            get
            {
                return instance;
            }
        }
    }

    /// <summary>
    /// object representation of a parsed XML file containing complementary select and insert statements
    /// </summary>
    [XmlRoot("SCAMPSQLExtract")]
    public class XMLsql : iSQLextract
    {
        public class STATEMENT
        {
            public string NAME { get; set; }
            public string QUERY { get; set; }
        }

        [XmlIgnore]
        public Dictionary<string, Dictionary<string, string>> Statement;

        public IDictionary<string, string> SelectStatements
        {
            get
            {
                return Statement["select"];
            }
        }

        public IDictionary<string, string> InsertStatements
        {
            get
            {
                return Statement["insert"];
            }
        }

        [XmlElement("SELECT")]
        public STATEMENT[] select
        {
            get
            {
                return Statement["select"].Select(q => new STATEMENT() { NAME = q.Key, QUERY = q.Value }).ToArray();
            }
            set
            {
                if (Statement == null)
                    Statement = new Dictionary<string, Dictionary<string, string>>();
                if (value != null)
                    Statement["select"] = value.ToDictionary(stk => stk.NAME, stv => stv.QUERY);
            }
        }

        [XmlElement("INSERT")]
        public STATEMENT[] insert
        {
            get
            {
                return Statement["insert"].Select(q => new STATEMENT() { NAME = q.Key, QUERY = q.Value }).ToArray();
            }
            set
            {
                if (Statement == null)
                    Statement = new Dictionary<string, Dictionary<string, string>>();
                if (value != null)
                    Statement["insert"] = value.ToDictionary(stk => stk.NAME, stv => stv.QUERY);
            }
        }

        public static XMLsql ReadFile(string filename)
        {

            XMLsql sqlobj = null;
            using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                var xd = new XmlSerializer(typeof(XMLsql));

                var obj = xd.Deserialize(fs);
                if (obj is XMLsql)
                    sqlobj = (obj as XMLsql);
            }
            return sqlobj;
        }
    }


    /// <summary>
    /// object representation of a parsed JSON file containing complementary select and insert statements
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class JSONsql : iSQLextract
    {

        [JsonProperty("SCAMPSQLExtract")]
        public Dictionary<string, Dictionary<string, string>> Statement;

        public IDictionary<string, string> SelectStatements
        {
            get
            {
                return Statement["select"];
            }
        }

        public IDictionary<string, string> InsertStatements
        {
            get
            {
                return Statement["insert"];
            }
        }

        public static JSONsql ReadFile(string filepath)
        {
            JSONsql obj = null;
            using (var jr = new JsonTextReader(new StreamReader(File.Open(filepath, FileMode.Open), true)))
            {
                JsonSerializer serializer = new JsonSerializer()
                {
                    Formatting = Newtonsoft.Json.Formatting.Indented,
                    CheckAdditionalContent = false,
                    TypeNameHandling = TypeNameHandling.Auto

                };
                obj = serializer.Deserialize<JSONsql>(jr);
            }
            return obj;
        }
    }

    #endregion

    public class ExtractManager
    {
        private class ExtractHandler
        {
            private bool extracting = false;
            private bool extractfinished = false;
            private bool extractprepared = false;
            private string zipfilename = "";
            private Exception err = null;

            public string filepath { get; set; }
            public string filename {
                get { return zipfilename; }
                set {
                    if (extracting)
                        throw new InvalidOperationException("You cannot change the filename when the file is extracting.");
                    else
                        zipfilename = value;
                }
            }

            public void BeginExtract()
            {
                lock (this)
                {
                    if (extracting)
                        return;
                    extractfinished = false;
                    extractprepared = false;
                    extracting = true;
                }
                //clear error on extract
                err = null;

                if(String.IsNullOrWhiteSpace(zipfilename))
                    zipfilename = string.Format("{0,0:yyyy-MM-dd_HH-mm}.extract", DateTime.Now);

                try
                {
                    var datasource = new DBDataSource();
                    var dataconsumer = new XMLzipConsumer();
                    datasource.init();
                    dataconsumer.init(this.filepath + zipfilename);
                
                    while (datasource.hasData())
                    {
                        using (var ctx = datasource.getReadContext())
                        {
                            dataconsumer.ProcessReadContext(ctx);
                        }
                    }
                    dataconsumer.closeZip();

                    extractfinished = true;
                    extractprepared = true;
                    extracting = false;
                    return;
                }
                catch (Exception e)
                {
                    this.err = e;
                }
                extracting = false;
                //begin prepare -- zip files produced by extract
                //this.prepareExtractedFiles();

            }

            public bool IsExtractPrepared() { return extractprepared; }
            public bool IsExtracting() { return extracting; }
            public bool ExtractFinished() { return extractfinished; }
            public string ExtractFilename() { return zipfilename; }

            public int getExtractStateCode()
            {
                if (extractprepared)
                    return 3;
                if (extractfinished)
                    return 2;
                else if (extracting)
                    return 1;
                else
                    return 0;
            }

            public Exception getExtractError()
            {
                return this.err;
            }

            public string getExtractState()
            {
                if(extractprepared)
                    return "Extract finished";
                if (extractfinished)
                    return "Preparing Extract file";
                else if (extracting)
                    return "Extracting";
                else
                    return "No current extract";
            }
        }

        private ExtractManager() { }
        private static readonly ExtractHandler handler = new ExtractHandler();

#region Handler Property Accessors
        public static Exception getExtractError()
        {
            return handler.getExtractError();
        }
        public static bool ExtractFinished()
        {
            return handler.ExtractFinished();
        }

        public static bool IsExtracting()
        {
            return handler.IsExtracting();
        }

        public static string getExtractState()
        {
            return handler.getExtractState();
        }
        public static bool IsPrepared()
        {
            return handler.IsExtractPrepared();
        }

        public static int getExtractStateCode()
        {
            return handler.getExtractStateCode();
        }

        public static string getExtractFilename()
        {
            return handler.ExtractFilename();
        }

#endregion
        public static void BeginExtract(string basepath = "", string extractconfigpath = "", string filename = "")
        {
            SQLQueryData.Instance.configpath = extractconfigpath;
            SQLQueryData.Instance.init();
            handler.filepath = basepath;
            if(!String.IsNullOrWhiteSpace(filename))
                handler.filename = filename;
            var task = new Task(handler.BeginExtract);
            task.Start();
        }
    }
}
