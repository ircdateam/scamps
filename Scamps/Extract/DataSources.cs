﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Configuration;
using Newtonsoft.Json;
using Scamps.Extract.Interfaces;

namespace Scamps.Extract
{
    public class JSONDataSource : IDataSource
    {
        protected iSQLextract sql;
        protected IEnumerator<KeyValuePair<string, string>> currFile;

        public void init()
        {
            sql = SQLQueryData.Instance.sqldata;
            currFile = sql.SelectStatements.GetEnumerator();
            //going to test the enumerator here

            if (!currFile.MoveNext())
                throw new Exception("Could not enumerate through select statements");
        }

        public bool hasData()
        {
            return (currFile != null);
        }

        public ReadContext getReadContext()
        {
            ReadContext ctx = null;

            if (currFile == null)
                return ctx;
            var curr = currFile.Current;

            var filename = string.Concat(curr.Key, ".json");

            var jtxt = new JsonTextReader(File.OpenText(filename));

            if (!jtxt.Read() || !jtxt.Read() || jtxt.TokenType != JsonToken.PropertyName)
                throw new Exception("Unable to read JSON source");


            var jscontext = jtxt.Value.ToString();

            var reader = new ExtractInfoDataReader(new JSONDataReader(jtxt));

            ctx = new ReadContext()
            {
                reader = reader,
                context = jscontext
            };

            if (!currFile.MoveNext())
                currFile = null;

            return ctx;
        }
    }
    public class XMLDataSource : IDataSource
    {
        protected iSQLextract sql;
        protected IEnumerator<KeyValuePair<string, string>> currFile;

        public void init()
        {
            sql = SQLQueryData.Instance.sqldata;
            currFile = sql.SelectStatements.GetEnumerator();
            //going to test the enumerator here

            if (!currFile.MoveNext())
                throw new Exception("Could not enumerate through select statements");
        }

        public bool hasData()
        {
            return (currFile != null);
        }

        public ReadContext getReadContext()
        {
            ReadContext ctx = null;

            if (currFile == null)
                return ctx;
            var curr = currFile.Current;

            var filename = string.Concat(curr.Key, ".xml");

            var settings = new XmlReaderSettings() { IgnoreComments = true, DtdProcessing = DtdProcessing.Parse };
            var xr = XmlReader.Create(filename, settings);
            xr.MoveToContent();

            //retrieve context from xml
            var xmlcontext = xr.Name;

            var reader = new ExtractInfoDataReader(new XMLDataReader(xr));

            ctx = new ReadContext()
            {
                reader = reader,
                context = xmlcontext
            };

            if (!currFile.MoveNext())
                currFile = null;

            return ctx;
        }
    }

    /// <summary>
    /// A DBDataSource extracts information from a database defined on queries provided by the SQLQueryData singleton.
    /// It's connection string is defined in App.config
    /// </summary>
    public class DBDataSource : IDataSource
    {
        private static readonly string _CONNECTION_NAME = "SCAMPs";
        protected ConnectionStringSettings cs;
        protected iSQLextract sql;
        protected IEnumerator<KeyValuePair<string, string>> currQuery;

        public bool hasData()
        {
            return (currQuery != null);
        }

        public void init()
        {
            cs = ConfigurationManager.ConnectionStrings[_CONNECTION_NAME];
            sql = SQLQueryData.Instance.sqldata;
            currQuery = sql.SelectStatements.GetEnumerator();
            //going to test the enumerator here

            if (!currQuery.MoveNext())
                throw new Exception("Could not enumerate through select statements");
        }

        public ReadContext getReadContext()
        {
            ReadContext ctx = null;

            if (currQuery == null)
                return ctx;
            var curr = currQuery.Current;

            var table = curr.Key;
            var statement = curr.Value;

            //var db = new ExtractInfoDataReader(new DBadapter(cs).getDataReader(statement));
            var db = new DBadapter(cs).getDataReader(statement);

            ctx = new ReadContext()
            {
                reader = db,
                context = table
            };

            if (!currQuery.MoveNext())
                currQuery = null;

            return ctx;
        }
    }
}
