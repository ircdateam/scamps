﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Data.Common;
using System.Reflection;
using System.Configuration;
using Scamps.Extract.Interfaces;
using Ionic.Zip;
using Newtonsoft.Json;



namespace Scamps.Extract
{

    #region Document Consumers
    /// <summary>
    /// produces a JSON formatted Dataset
    /// </summary>
    public class JSONConsumer : IDataConsumer
    {
        private string basePath;

        public void init(string basePath) { this.basePath = basePath; }
        public void init() { }

        public int ProcessReadContext(ReadContext context)
        {
            int affected = 0;

            string filename;
            if (string.IsNullOrEmpty(basePath))
                filename = string.Concat(context.context, ".json");
            else
                filename = string.Concat(basePath, context.context, ".json");

            var rd = context.reader;

            using (var fs = new StreamWriter(filename, false, Encoding.UTF8))
            using (var jr = new Newtonsoft.Json.JsonTextWriter(fs))
            {
                jr.WriteStartObject();
                jr.WritePropertyName(context.context);
                jr.WriteStartArray();
                while (rd.Read())
                {
                    jr.WriteStartObject();
                    foreach (var i in Enumerable.Range(0, rd.FieldCount))
                    {
                        jr.WritePropertyName(rd.GetName(i));
                        jr.WriteValue(rd.GetValue(i));
                    }
                    jr.WriteEndObject();
                    affected++;
                }
                jr.WriteEndArray();
                jr.WriteEndObject();
            }
            return affected;
        }

    }

    /// <summary>
    /// streams a JSON formatted Dataset into a zip archive
    /// </summary>
    public class JSONzipConsumer : IDataConsumer
    {
        private string zipfilePath;
        private ZipOutputStream zipOut;

        public void closeZip()
        {
            if (zipOut != null)
                zipOut.Close();
        }
        public void init(string zipfilePath) { this.zipfilePath = zipfilePath; }
        public void init() { }

        public int ProcessReadContext(ReadContext context)
        {
            int affected = 0;

            var rd = context.reader;

            if (zipOut == null)
            {
                zipOut = new ZipOutputStream(this.zipfilePath);
                zipOut.CompressionLevel = Ionic.Zlib.CompressionLevel.Level6;
            }

            zipOut.PutNextEntry(context.context+ ".json");
            
            using (var fs = new StreamWriter(zipOut))
            using (var jr = new JsonTextWriter(fs))
            {
                jr.WriteStartObject();
                jr.WritePropertyName(context.context);
                jr.WriteStartArray();

                while (rd.Read())
                {
                    jr.WriteStartObject();
                    foreach (var i in Enumerable.Range(0, rd.FieldCount))
                    {
                        jr.WritePropertyName(rd.GetName(i));
                        jr.WriteValue(rd.GetValue(i));
                    }
                    jr.WriteEndObject();
                    affected++;
                }

                jr.WriteEndArray();
                jr.WriteEndObject();
            }

            return affected;
        }

    }

    /// <summary>
    /// streams an XML formatted Dataset into a zip archive
    /// </summary>
    public class XMLzipConsumer : IDataConsumer
    {
        private string zipfilePath;
        private ZipOutputStream zipOut;

        public void closeZip()
        {
            if (zipOut != null)
                zipOut.Close();
        }
        public void init(string zipfilePath) { this.zipfilePath = zipfilePath; }
        public void init() { }

        public int ProcessReadContext(ReadContext context)
        {
            int affected = 0;

            var rd = context.reader;

            if(zipOut == null){
                zipOut = new ZipOutputStream(this.zipfilePath);
                zipOut.CompressionLevel = Ionic.Zlib.CompressionLevel.Level6;
            }

            zipOut.PutNextEntry(context.context+ ".xml");
            try
            {
                using (var xw = XmlWriter.Create(zipOut))
                {
                    xw.WriteStartDocument();
                    xw.WriteStartElement(context.context);
                    while (rd.Read())
                    {
                        xw.WriteStartElement("RECORD");

                        foreach (var i in Enumerable.Range(0, rd.FieldCount))
                        {
                            xw.WriteStartElement(rd.GetName(i));

                            //xw.WriteAttributeString("Type", rd.GetFieldType(i).ToString());

                            var val = rd.GetValue(i);
                            if (val == null || val == DBNull.Value)
                                xw.WriteValue("");
                            else
                                xw.WriteValue(val);
                            xw.WriteEndElement();
                        }

                        xw.WriteEndElement();
                        affected++;
                    }
                    xw.WriteEndElement();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            
            return affected;
        }
    }
    /// <summary>
    /// produces an XML formatted Dataset
    /// </summary>
    public class XMLConsumer : IDataConsumer
    {
        private string basePath;

        public void init(string basePath) { this.basePath = basePath; }
        public void init() { }

        public int ProcessReadContext(ReadContext context)
        {
            int affected = 0;
            
            string filename;
            if(string.IsNullOrEmpty(basePath))
                filename = string.Concat(context.context, ".xml");
            else
                filename = string.Concat(basePath,context.context, ".xml");

            var rd = context.reader;

            using (var fs = new StreamWriter(filename, false, Encoding.UTF8))
            using (var xw = XmlWriter.Create(fs))
            {

                xw.WriteStartDocument();
                xw.WriteStartElement(context.context);
                //xw.WriteAttributeString("xmlns", "xs", null, "http://www.w3.org/2001/XMLSchema");

                while (rd.Read())
                {
                    xw.WriteStartElement("RECORD");


                    foreach (var i in Enumerable.Range(0, rd.FieldCount))
                    {
                        xw.WriteStartElement(rd.GetName(i));
                        
                        //xw.WriteAttributeString("Type", rd.GetFieldType(i).ToString());

                        var val = rd.GetValue(i);
                        if (val == null || val == DBNull.Value)
                            xw.WriteValue("");
                        else
                            xw.WriteValue(val);
                        xw.WriteEndElement();
                    }

                    xw.WriteEndElement();
                    affected++;
                }
                xw.WriteEndElement();
            }

            return affected;
        }
    }

    public class CSVConsumer : IDataConsumer
    {
        public void init() { }

        public int ProcessReadContext(ReadContext context)
        {
            int affected = 0;

            var filename = string.Concat(context.context, ".csv");
            var rd = context.reader;
            int fieldsize = rd.FieldCount;

            using (var fs = new StreamWriter(filename, false, Encoding.UTF8))
            {
                if (!rd.Read() || fieldsize < 1)
                    return affected;

                //initialize CSV header
                for (var i = 0; i < fieldsize - 1; i++)
                {
                    fs.Write(rd.GetName(i));
                    fs.Write(",");
                }
                fs.Write(rd.GetName(fieldsize - 1));
                fs.WriteLine();

                //begin writing data
                do
                {
                    for (var i = 0; i < fieldsize - 1; i++)
                    {
                        if (rd.IsDBNull(i))
                        {
                            fs.Write(",");
                            continue;
                        }
                        else if (rd.GetFieldType(i) == typeof(string))
                        {
                            fs.Write("\"");
                            foreach (var ch in rd.GetString(i))
                            {
                                if (ch == '"')
                                    fs.Write('"');
                                fs.Write(ch);
                            }
                            fs.Write("\",");
                        }
                        else if (rd.GetFieldType(i) == typeof(DateTime))
                        {
                            fs.Write("\"\"");
                            fs.Write(rd.GetDateTime(i).ToString());
                            fs.Write("\",");
                        }
                        else
                        {
                            fs.Write(rd.GetValue(i));
                            fs.Write(",");
                        }
                    }
                    var idx = fieldsize - 1;
                    if (rd.IsDBNull(idx))
                    {
                        continue;
                    }
                    else if (rd.GetFieldType(idx) == typeof(string))
                    {
                        fs.Write("\"");
                        foreach (var ch in rd.GetString(idx))
                        {
                            if (ch == '"')
                                fs.Write('"');
                            fs.Write(ch);
                        }
                        fs.Write("\"");
                    }
                    else if (rd.GetFieldType(idx) == typeof(DateTime))
                    {
                        fs.Write("\"");
                        fs.Write(rd.GetDateTime(idx).ToString());
                        fs.Write("\"");
                    }
                    else
                    {
                        fs.Write(rd.GetValue(idx));
                    }
                    fs.WriteLine();
                    affected++;
                } while (rd.Read());
            }
            return affected;
        }

    }
    
    #endregion

    #region Database Consumers

    /// <summary>
    /// a generic database consumer which attempts to insert records on its defined database.
    /// Is reliant on the SQLQueryData singleton which provides the queries necessary to process the dataset
    /// <seealso cref="SQLQueryData" />
    /// </summary>
    public class DbDataConsumer : IDataConsumer
    {
        protected static readonly string _CONNECTION_NAME = "destination";
        protected DBadapter db;
        protected iSQLextract queries;

        public virtual void init()
        {
            LoadConnectionString();
            LoadQueries();
        }

        public void LoadConnectionString()
        {
            var cstr = ConfigurationManager.ConnectionStrings[_CONNECTION_NAME];
            this.db = new DBadapter(cstr);
        }
        protected void LoadQueries()
        {
            if (queries == null)
                queries = SQLQueryData.Instance.sqldata;
        }

        public int ProcessReadContext(ReadContext context)
        {
            int affected = 0;

            string statement = SQLQueryData.Instance.sqldata.InsertStatements[context.context];

            affected = db.BulkQuery(statement, context.reader);

            return affected;
        }
    }

    /// <summary>
    /// a special subset of the DbDataConsumer. Requires additional initialization.
    /// A base Access database is defined and embedded in the application. 
    /// On initialization, the embedded file is extracted and written to disk.
    /// Has a filename template with a prefix configurable in app.config.
    /// </summary>
    /// 
    public class AccessDBconsumer : DbDataConsumer
    {
        private static readonly string FILENAME_PREFIX_KEY = "FILENAME_PREFIX";
        private static readonly string DEFAULT_ACCESSDB_NAME = "{0}extract_{1}.accdb";
        private static readonly string ACCESS_EMBEDDED_RESOURCE = "SCAMPExtract.Resources.scamps.dat";

        public override void init()
        {
            //retrieve filename prefix from config
            var filenamePrefix = ConfigurationManager.AppSettings[FILENAME_PREFIX_KEY];
            if (string.IsNullOrEmpty(filenamePrefix))
                filenamePrefix = "";

            //generate filename from defined prefix and current time
            string filename = string.Format(DEFAULT_ACCESSDB_NAME, filenamePrefix, DateTime.Now.ToString("yyyy-MM-ddThh_mm_ss"));

            //given path, write the embedded acccess db resource to the path
            var _assembly = Assembly.GetExecutingAssembly();

            using (var resStream = _assembly.GetManifestResourceStream(ACCESS_EMBEDDED_RESOURCE))
            using (var fileOutStream = new FileStream(filename, FileMode.CreateNew))
            {
                int streamByte;

                while ((streamByte = resStream.ReadByte()) != -1)
                    fileOutStream.WriteByte((byte)streamByte);
            }

            //load the configuration file
            LoadQueries();

            //using the db connection string builder, set the path to the access database
            var accdbConnString = ConfigurationManager.ConnectionStrings[_CONNECTION_NAME];
            var cbuilder = new DbConnectionStringBuilder();
            cbuilder.ConnectionString = ConfigurationManager.ConnectionStrings[_CONNECTION_NAME].ConnectionString;
            cbuilder["Data Source"] = filename;
            var cc = new ConnectionStringSettings("_AccessDB", cbuilder.ConnectionString, accdbConnString.ProviderName);
            this.db = new DBadapter(cc);
        }
    }
    #endregion
}
