﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;
using Newtonsoft.Json;
using Scamps.Extract.Interfaces;

namespace Scamps.Extract
{
    public class JSONDataReader : ObjectArrayDataReader
    {
        private JsonReader reader;

        public JSONDataReader(JsonReader reader) { this.reader = reader; }

        public override bool Read()
        {
            if (fields != null && values == null)
                return false;

            while (reader.Read())
            {

                if (reader.TokenType == JsonToken.StartObject)
                {
                    var mindepth = reader.Depth + 1;

                    if (fields == null)
                    {
                        var dfv = new Dictionary<string, object>();
                        var lastkey = "";
                        while (reader.Read() && !(reader.TokenType == JsonToken.EndObject && reader.Depth <= mindepth))
                        {
                            if (reader.TokenType == JsonToken.PropertyName)
                            {
                                lastkey = reader.Value.ToString();
                                dfv.Add(lastkey, null);
                            }
                            else if (reader.TokenType != JsonToken.Null)
                            {
                                if (!string.IsNullOrEmpty(lastkey))
                                {
                                    dfv[lastkey] = reader.Value;
                                    lastkey = "";
                                }
                            }
                        }

                        if (dfv.Count == 0)
                            return false;
                        fields = new string[dfv.Count];
                        values = new object[dfv.Count];
                        foreach (var idx in Enumerable.Range(0, dfv.Count))
                        {
                            fields[idx] = dfv.ElementAt(idx).Key;
                            values[idx] = dfv.ElementAt(idx).Value;
                        }
                    }
                    else
                    {
                        var lastidx = -1;
                        while (reader.Read() && !(reader.TokenType == JsonToken.EndObject && reader.Depth <= mindepth))
                        {
                            if (reader.TokenType == JsonToken.PropertyName)
                            {
                                lastidx = Array.IndexOf(fields, reader.Value.ToString());
                            }
                            else if (reader.TokenType != JsonToken.Null)
                            {
                                if (lastidx != -1)
                                {
                                    values[lastidx] = reader.Value;
                                    lastidx = -1;
                                }
                            }
                        }
                    }
                    total++;
                    return true;
                }
            }

            return false;
        }
        public override void Close()
        {
            if (reader != null)
                reader.Close();
        }

        public override void Dispose()
        {
            if (reader != null)
            {
                reader.Close();
                reader = null;
            }

        }
    }
    public class XMLDataReader : ObjectArrayDataReader
    {
        private XmlReader reader;

        private static readonly Dictionary<string, int> TypeMap = new Dictionary<string, int>(){
                                                                    {typeof(bool).ToString(),1},
                                                                    {typeof(DateTime).ToString(),2},
                                                                    {typeof(decimal).ToString(),3},
                                                                    {typeof(double).ToString(),4},
                                                                    {typeof(float).ToString(),5},
                                                                    {typeof(int).ToString(),6},
                                                                    {typeof(long).ToString(),7}
                                                               };
        public XMLDataReader(XmlReader reader) { this.reader = reader; }


        private object getXMLcontentVal(string typestring, XmlReader reader)
        {
            object val = null;
            int typeswitch = 0;

            if (!string.IsNullOrEmpty(typestring) && TypeMap.ContainsKey(typestring))
                typeswitch = TypeMap[typestring];

            switch (typeswitch)
            {
                case 1:
                    val = reader.ReadContentAsBoolean();
                    break;
                case 2:
                    val = reader.ReadContentAsDateTime();
                    break;
                case 3:
                    val = reader.ReadContentAsDecimal();
                    break;
                case 4:
                    val = reader.ReadContentAsDouble();
                    break;
                case 5:
                    val = reader.ReadContentAsFloat();
                    break;
                case 6:
                    val = reader.ReadContentAsInt();
                    break;
                case 7:
                    val = reader.ReadContentAsLong();
                    break;
                default:
                    val = reader.ReadContentAsString();
                    break;
            }
            return val;
        }

        public override bool Read()
        {

            if (fields != null && values == null)
                return false;

            if (reader.ReadToFollowing("RECORD"))
            {
                var mindepth = reader.Depth;

                if (fields == null)
                {
                    var initfieldvals = new Dictionary<string, object>();

                    string lastkey = "";
                    string type = "";
                    while (reader.Read() && !(reader.NodeType == XmlNodeType.EndElement && reader.Depth == mindepth))
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            lastkey = reader.Name;
                            type = reader.GetAttribute("Type");
                            initfieldvals.Add(lastkey, null);
                        }
                        else if (reader.NodeType == XmlNodeType.Text)
                        {
                            initfieldvals[lastkey] = getXMLcontentVal(type, reader);
                        }
                    }
                    if (initfieldvals.Count == 0)
                        return false;
                    fields = new string[initfieldvals.Count];
                    values = new object[initfieldvals.Count];
                    foreach (var idx in Enumerable.Range(0, initfieldvals.Count))
                    {
                        fields[idx] = initfieldvals.ElementAt(idx).Key;
                        values[idx] = initfieldvals.ElementAt(idx).Value;
                    }
                }
                else
                {
                    int lastidx = -1;
                    string type = "";
                    while (reader.Read() && !(reader.NodeType == XmlNodeType.EndElement && reader.Depth == mindepth))
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            lastidx = Array.IndexOf(fields, reader.Name);
                            if (lastidx != -1)
                                type = reader.GetAttribute("Type");
                        }
                        else if (reader.NodeType == XmlNodeType.Text)
                        {
                            if (lastidx != -1)
                                values[lastidx] = getXMLcontentVal(type, reader);
                        }
                    }
                }
                total++;
            }
            else
            {
                values = null;
                return false;
            }
            return true;

        }

        public override void Close()
        {
            if (reader != null && reader.ReadState != ReadState.Closed)
                reader.Close();
        }
        public override void Dispose()
        {
            if (fields != null)
                fields = null;
            if (values != null)
                values = null;
            if (reader != null)
            {
                if (reader.ReadState != ReadState.Closed)
                    reader.Close();
                reader = null;
            }
        }
    }

    public abstract class ObjectArrayDataReader : IDataReader
    {
        protected int total = 0;
        protected int currDepth = 0;

        protected string[] fields;
        protected object[] values;

        public object this[int i]
        {
            get
            {
                if (values == null || i < 0 || i > values.Length)
                    return null;
                return values[i];
            }
        }
        public object this[string i]
        {
            get
            {
                int idx = 0;
                if (string.IsNullOrEmpty(i) || fields == null || (idx = Array.IndexOf(fields, i)) == -1)
                    return null;

                return values[idx];
            }
        }
        public int FieldCount
        {
            get
            {
                if (fields == null)
                    return 0;

                return fields.Length;
            }
        }

        public int Depth
        {
            get
            {
                return currDepth;
            }
        }

        public bool IsClosed
        {
            get
            {
                return (values == null);
            }
        }

        public int RecordsAffected
        {
            get
            {
                return total;
            }
        }

        public ObjectArrayDataReader() { }

        public bool GetBoolean(int i) { return (bool)values[i]; }
        public byte GetByte(int i) { return (byte)values[i]; }
        public long GetBytes(int i, long io, byte[] b, int bo, int l)
        {
            throw new NotImplementedException();
        }
        public char GetChar(int i) { return (char)values[i]; }
        public long GetChars(int i, long io, char[] b, int bo, int l)
        {
            throw new NotImplementedException();
        }
        public decimal GetDecimal(int i) { return (decimal)values[i]; }
        public double GetDouble(int i) { return (double)values[i]; }
        public Type GetFieldType(int i)
        {
            if (values[i] == null)
                return typeof(object);
            return values[i].GetType().UnderlyingSystemType;
        }
        public float GetFloat(int i) { return (float)values[i]; }
        public Guid GetGuid(int i) { return (Guid)values[i]; }
        public short GetInt16(int i) { return (short)values[i]; }
        public int GetInt32(int i) { return (int)values[i]; }
        public long GetInt64(int i) { return (long)values[i]; }
        public string GetName(int i) { return fields[i]; }
        public int GetOrdinal(string i) { return Array.IndexOf(this.fields, i); }
        public DataTable GetSchemaTable() { throw new NotImplementedException(); }
        public string GetString(int i) { return (string)values[i]; }
        public object GetValue(int i) { return values[i]; }
        public int GetValues(object[] i)
        {
            if (i == null)
                return 0;
            int count = 0;
            int minlength = i.Length > fields.Length ? fields.Length : i.Length;
            foreach (var idx in Enumerable.Range(0, minlength))
            {
                i[idx] = (values[idx] == null) ? DBNull.Value : values[idx];
                count++;
            }

            return count++;
        }
        public bool IsDBNull(int i) { return (values[i] == null || values[i] == DBNull.Value); }
        public bool NextResult() { return (values == null); }

        public string GetDataTypeName(int i) { return typeof(string).ToString(); }
        public DateTime GetDateTime(int i) { return (DateTime)values[i]; }
        public IDataReader GetData(int i) { throw new NotImplementedException(); }

        public abstract bool Read();

        public abstract void Close();

        public abstract void Dispose();
    }
    /// <summary>
    /// An IDataReader wrapper class which prints the number of records read to console at a defined interval
    /// </summary>
    public class ExtractInfoDataReader : IDataReader
    {
        private IDataReader innerReader = null;
        private int currcounter = 0;
        private static readonly int countmax = 1000;
        private long total = 0;
        private string lastmsg;

        public long TotalRead
        {
            get
            {
                return total + currcounter;
            }
        }

        public int FieldCount
        {
            get
            {
                return innerReader.FieldCount;
            }
        }

        public object this[int i]
        {
            get
            {
                return innerReader[i];
            }
        }
        public object this[string i]
        {
            get
            {
                return innerReader[i];
            }
        }

        public int RecordsAffected
        {
            get
            {
                return innerReader.RecordsAffected;
            }
        }

        public int Depth
        {
            get
            {
                return innerReader.Depth;
            }
        }

        public bool IsClosed
        {
            get
            {
                return innerReader.IsClosed;
            }
        }

        public void clearLastMessage()
        {
            if (string.IsNullOrEmpty(lastmsg))
                return;
            foreach (var i in Enumerable.Range(0, lastmsg.Length))
                Console.Write("\b");
        }

        public ExtractInfoDataReader()
        {
        }

        public ExtractInfoDataReader(IDataReader dr)
        {
            innerReader = dr;
        }

        public bool GetBoolean(int i) { return innerReader.GetBoolean(i); }
        public byte GetByte(int i) { return innerReader.GetByte(i); }
        public long GetBytes(int i, long io, byte[] b, int bo, int l) { return innerReader.GetBytes(i, io, b, bo, l); }
        public char GetChar(int i) { return innerReader.GetChar(i); }
        public long GetChars(int i, long io, char[] b, int bo, int l) { return innerReader.GetChars(i, io, b, bo, l); }
        public decimal GetDecimal(int i) { return innerReader.GetDecimal(i); }
        public double GetDouble(int i) { return innerReader.GetDouble(i); }
        public Type GetFieldType(int i) { return innerReader.GetFieldType(i); }
        public float GetFloat(int i) { return innerReader.GetFloat(i); }
        public Guid GetGuid(int i) { return innerReader.GetGuid(i); }
        public short GetInt16(int i) { return innerReader.GetInt16(i); }
        public int GetInt32(int i) { return innerReader.GetInt32(i); }
        public long GetInt64(int i) { return innerReader.GetInt64(i); }
        public string GetName(int i) { return innerReader.GetName(i); }
        public int GetOrdinal(string i) { return innerReader.GetOrdinal(i); }
        public DataTable GetSchemaTable() { return innerReader.GetSchemaTable(); }
        public string GetString(int i) { return innerReader.GetString(i); }
        public object GetValue(int i) { return innerReader.GetValue(i); }
        public int GetValues(object[] i) { return innerReader.GetValues(i); }
        public bool IsDBNull(int i) { return innerReader.IsDBNull(i); }
        public bool NextResult() { return innerReader.NextResult(); }
        public bool Read()
        {
            if (!innerReader.Read())
            {
                this.clearLastMessage();
                return false;
            }

            this.currcounter++;
            if (this.currcounter >= ExtractInfoDataReader.countmax)
            {
                this.clearLastMessage();
                total += currcounter;
                currcounter = 0;
                lastmsg = string.Format("{0} Records Read...", this.total);
                Console.Write(lastmsg);

            }
            return true;
        }
        public string GetDataTypeName(int i) { return innerReader.GetDataTypeName(i); }
        public DateTime GetDateTime(int i) { return innerReader.GetDateTime(i); }
        public IDataReader GetData(int i) { return innerReader.GetData(i); }

        public void Close()
        {
            if (innerReader != null)
                innerReader.Close();
        }
        public void Dispose()
        {
            if (innerReader == null)
                return;
            innerReader.Dispose();
        }
    }
}
