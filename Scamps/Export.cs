﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data.EntityClient;
using System.Data.Metadata.Edm;
using System.Data.Objects;
using System.Data.Mapping;
using System.Data.Common;
using System.Text;
using System.IO;
using System.Reflection;
using System.Collections.Specialized;

namespace Scamps
{
    public static class qXMLextension
    {
        public static qXML WriteStartDoc(this qXML w)
        {
            w.WriteStartDocument();
            return w;
        }
        public static qXML WriteStartEle(this qXML w, string name)
        {
            w.WriteStartElement(name);
            return w;
        }
        public static qXML WriteEndEle(this qXML w)
        {
            w.WriteEndElement();
            return w;
        }
        public static qXML WriteEleVal(this qXML w, string name, string val)
        {
            w.WriteElementValue(name, val);
            return w;
        }
        public static qXML WriteIFEleVal(this qXML w, bool condition, string name, string val)
        {
            if (condition)
                return w.WriteEleVal(name, val);
            return w;
        }
    }

    public class qXML
    {
        Stack<string> lastEle;
        StreamWriter w;

        public qXML(StreamWriter t)
        {
            lastEle = new Stack<string>();
            this.w = t;
        }

        public void WriteStartDocument()
        {
            w.Write(string.Concat("<?xml version=\"1.0\" encoding=\"utf-8\"?>"));
            w.WriteLine();
        }

        public void WriteStartElement(string e)
        {
            for (var i = 0; i < lastEle.Count; i++)
                w.Write("\t");
            lastEle.Push(e);
            w.Write(string.Concat("<", e, ">"));
            w.WriteLine();
        }
        public void WriteEndElement()
        {
            var str = lastEle.Pop();
            for (var i = 0; i < lastEle.Count; i++)
                w.Write("\t");
            w.Write(string.Concat("</", str, ">"));
            w.WriteLine();
        }
        public void WriteElementValue(string name, string val)
        {
            for (var i = 0; i < lastEle.Count; i++)
                w.Write("\t");
            w.Write(string.Concat("<", name, ">", System.Security.SecurityElement.Escape(val), "</", name, ">"));
            w.WriteLine();
        }
        public void Close()
        {
            w.Close();
        }
    }

    public static class StrExtension
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static IDictionary<string, string> ToDictionary(this NameValueCollection col)
        {
            IDictionary<string, string> dict = new Dictionary<string, string>();
            foreach (var k in col.AllKeys)
            {
                dict.Add(k, col[k]);
            }
            return dict;
        }

        public static string getValueOrDefault(this IDictionary<string, string> dict, string key)
        {
            if (dict.ContainsKey(key))
                return dict[key];
            else
                return null;
        }

        public static T getValueOrOriginal<T>(this IDictionary<string, string> dict, string key, T startingValue)
        {
            if (dict.ContainsKey(key))
            {
                try
                {
                    //TODO: Revisit this. (null)/Default values/etc. http://stackoverflow.com/questions/3531318/convert-changetype-fails-on-nullable-types
                    var coerced = (T)Convert.ChangeType(dict[key], Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T));
                    return coerced;
                }
                catch
                {
                    return startingValue;
                }
            }
            else
                return startingValue;
        }
    }
    /// <summary>
    /// Encapsulates export of legacy scamp forms to an XML format usable by the new template system.
    /// </summary>
    public class LegacyExport
    {
        private class frmElement
        {
            public string name { get; set; }
            public string type { get; set; }
            public string datatype { get; set; }
            public string multiline { get; set; }
            public string min { get; set; }
            public string max { get; set; }
            public string maxlength { get; set; }
            public string label { get; set; }
            public string subtitle { get; set; }
            public string required { get; set; }
            public string list { get; set; }

            public string processType()
            {
                string rtype = string.Empty;

                switch (type)
                {
                    case "combobox":
                        switch (datatype)
                        {
                            case "picklist":
                                rtype = "select";
                                break;
                            default:
                                rtype = "checklist";
                                break;
                        }
                        break;
                    case "listbox":
                        rtype = "select";
                        break;
                    default:
                        if (!string.IsNullOrEmpty(multiline))
                        {
                            rtype = "textarea";
                            break;
                        }
                        switch (datatype)
                        {
                            case "image":
                            case "float":
                            case "datetime":
                                rtype = datatype;
                                break;
                            case "integer":
                                rtype = "signedint";
                                break;
                            case "picklist":
                                rtype = "multiselect";
                                break;
                            default:
                                rtype = "textbox";
                                break;
                        }
                        break;
                }

                return rtype;
            }
        }
        private class FormInfo
        {
            public long id { get; set; }
            public string name { get; set; }
            public string scamp { get; set; }
            public string entity { get; set; }
            public string distinctlists { get; set; }
        }
        private class workspaceGenerate
        {
            public static MetadataWorkspace getWorkspace(string classNamespace, string className, string provider)
            {
                var edmItemCollection = new EdmItemCollection(new[] { createCSDL(classNamespace, className) });
                var storeItemCollection = new StoreItemCollection(new[] { createSSDL(classNamespace, className, provider) });
                var storeMappingItemCollection = new StorageMappingItemCollection(edmItemCollection, storeItemCollection, new[] { createMSL(classNamespace, className) });
                var mdw = new MetadataWorkspace();
                mdw.RegisterItemCollection(edmItemCollection);
                mdw.RegisterItemCollection(storeItemCollection);
                mdw.RegisterItemCollection(storeMappingItemCollection);
                return mdw;
            }
            private static void printXMLdoc(XContainer d)
            {
                using (var stringWriter = new StringWriter())
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    d.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    Console.WriteLine(stringWriter.GetStringBuilder().ToString());
                }
            }
            private static XmlReader createCSDL(string classNamespace, string className)
            {
                var nsURL = "http://schemas.microsoft.com/ado/2008/09/edm";
                var anURL = "http://schemas.microsoft.com/ado/2009/02/edm/annotation";
                XNamespace ns = nsURL;
                XNamespace an = anURL;
                var l = new XDocument(new XDeclaration("1.0", "utf-8", null),
                    new XElement(ns + "Schema",
                        new XAttribute("xmlns", nsURL),
                        new XAttribute(XNamespace.Xmlns + "annotation", anURL),
                        new XAttribute("Namespace", classNamespace),
                    //new XAttribute("Namespace", "CHRACTSTModel"),
                        new XAttribute("Alias", "Self"),
                        new XElement(ns + "EntityContainer",
                    //new XAttribute("Name","CHRACTSTEntities"),
                            new XAttribute("Name", className),
                            new XAttribute(an + "LazyLoadingEnabled", "true")
                        )
                    )
                );
                return l.CreateReader();
            }

            private static XmlReader createMSL(string classNamespace, string className)
            {
                var nsURL = "http://schemas.microsoft.com/ado/2008/09/mapping/cs";
                XNamespace ns = nsURL;
                var l = new XDocument(new XDeclaration("1.0", "utf-8", null),
                    new XElement(ns + "Mapping",
                        new XAttribute("xmlns", nsURL),
                        new XAttribute("Space", "C-S"),
                        new XElement(ns + "EntityContainerMapping",
                            new XAttribute("StorageEntityContainer", classNamespace + "StoreContainer"),
                            new XAttribute("CdmEntityContainer", className)
                        )
                    )
                );
                return l.CreateReader();
            }
            private static XmlReader createSSDL(string classNamespace, string className, string provider)
            {
                var nsURL = "http://schemas.microsoft.com/ado/2009/02/edm/ssdl";
                var storeURL = "http://schemas.microsoft.com/ado/2007/12/edm/EntityStoreSchemaGenerator";
                XNamespace ns = nsURL;
                XNamespace store = storeURL;
                var l = new XDocument(new XDeclaration("1.0", "utf-8", null),
                    new XElement(ns + "Schema",
                        new XAttribute("xmlns", nsURL),
                        new XAttribute(XNamespace.Xmlns + "store", storeURL),
                        new XAttribute("Namespace", classNamespace + "s.Store"),
                        new XAttribute("Alias", "Self"),
                        new XAttribute("Provider", provider),
                        new XAttribute("ProviderManifestToken", "11.2"),
                        new XElement(ns + "EntityContainer",
                            new XAttribute("Name", classNamespace + "StoreContainer")
                        )
                    )
                );
                return l.CreateReader();
            }
        }

        const string _queryFormByID = "select en.encounter_id as id, en.encounter_name as name, s.scamp_name as scamp, en.form_name as entity from admin_encounters en join scamp s on s.scamp_id = en.scamp_id where en.encounter_id = :01";
        const string _queryFormByName = "select en.encounter_id as id, en.encounter_name as name, s.scamp_name as scamp, en.form_name as entity from admin_encounters en join scamp s on s.scamp_id = en.scamp_id where upper(en.form_name) = upper(:01)";
        const string _queryFormByEncID = "select en.encounter_id as id, en.encounter_name as name, s.scamp_name as scamp, en.form_name as entity from admin_encounters en join pt_encounters e on en.encounter_id = e.encounter_type join scamp s on s.scamp_id = en.scamp_id where e.APPT_ID = :01";
        const string _queryFormBySubID = "select en.encounter_id as id, en.encounter_name as name, s.scamp_name as scamp, en.form_name as entity from admin_encounters en join visits_inpt_subunit v on en.encounter_id = v.encounter_type join scamp s on s.scamp_id = en.scamp_id where v.ID = :01";
        const string _queryDistinctLists = "select distinct el.list_cd from admin_encounters en join admin_form_fields af on en.form_name = af.form_name join admin_element e on e.element_cd = af.element_cd join admin_element_list el on e.list_cd = el.list_cd where en.encounter_id = :01";
        const string _queryFormElements = "select (case when af.required_flag = 1 then 'true' else '' end) as required, lower(e.list_cd) as list, coalesce(e.element_cd,'s'||to_char(af.id)) as name, lower(af.control_type) as type, lower(e.data_type_cd) as datatype,(case when upper(af.textmode) = 'MULTILINE' then 'true' else '' end) as multiline, to_char(e.lowest_val) as min, to_char(e.highest_val) as max, to_char(e.length_val) as maxlength, e.name_txt as label, coalesce(e.description_txt,af.descr_txt) as subtitle from admin_encounters en join admin_form_fields af on en.form_name = af.form_name left join admin_element e on e.element_cd = af.element_cd where en.encounter_id = :01 order by af.display_seq_num";

        /// <summary>
        /// Exports a 'legacy' SCAMP form from its database representation to an XML format given a cnnection, an export path, and a form identifier specifying which SCAMP form to export.
        /// </summary>
        /// <param name="connection">Database connection to access the form to export. Requires that the connection string is already defined.</param>
        /// <param name="providerName">The provider for the database connection. e.g. 'Oracle.DataAccess.Client'</param>
        /// <param name="exportPath">The destination folder of the exported SCAMP. The filename is generated using the form's name (e.g. form PUP_SDF1_PREOPCLINIC under scamp PUP will be named PUP_SDF1_PREOPCLINIC.form by default) if no filename is specified by the 'filename' parameter</param>
        /// <param name="formIdent">Specifies which form to retrieve. If formIdent delimits 2 numeric values (a long and an int representing a appt_id and an id for a inpatient subunit record respectively), a form is looked up by the subunit id. If formIdent is numeric and parsable to long, a form is looked up based on an encounter appointment id. Otherwise, the form is found by its unique form name (e.g. PUP_SDF1_PREOPCLINIC)</param>
        /// /// <param name="filename">specific filename to export the SCAMP from as. Should include file extension to be used.</param>
        /// <returns>state of export-- true if successful</returns>
        public static bool exportSCAMPFormDBtoXML(string connectionString, string providerName, string formIdent, string exportPath, out string createdFormName, string filename = "")
        {

            long? appt_id = null;
            int? subunit = null;
            long tmp = 0;
            bool firstPanel = true;
            string fullPath;
            createdFormName = string.Empty;
            
            //attempt to split form ident by null termination string '\0'
            if (formIdent.Contains('\0'))
            {
                var identsplit = formIdent.Split('\0');
                if (identsplit.Length != 2)
                    return false;
                int itmp = 0;
                if (!int.TryParse(identsplit[1], out itmp))
                    return false;
                subunit = itmp;
            }
            //determines if form is exported by appointment id or form name
            else if (long.TryParse(formIdent, out tmp))
                appt_id = tmp;
            try
            {
                var dbConn = new Oracle.ManagedDataAccess.Client.OracleConnection();
                dbConn.ConnectionString = connectionString;
                var workspace = workspaceGenerate.getWorkspace("Models", "SCAMPEntities", providerName);
                var t = new ObjectContext(new EntityConnection(workspace, dbConn));

                var distList = new StringBuilder();

                FormInfo FormInfo;
                if(subunit.HasValue)
                    FormInfo = t.ExecuteStoreQuery<FormInfo>(_queryFormBySubID, subunit.Value).First();
                else if (appt_id.HasValue)
                    FormInfo = t.ExecuteStoreQuery<FormInfo>(_queryFormByEncID, appt_id.Value).First();
                else
                    FormInfo = t.ExecuteStoreQuery<FormInfo>(_queryFormByName, formIdent).First();

                if (string.IsNullOrEmpty(filename))
                    createdFormName = string.Concat(FormInfo.entity, ".form");
                else
                    createdFormName = filename;

                fullPath = string.Concat(exportPath,createdFormName);

                if (File.Exists(fullPath))
                {
                    Console.Error.WriteLine("Error export SCAMP Form: File already exists at path: {0}", fullPath);
                    return false;
                }

                var strList = t.ExecuteStoreQuery<string>(_queryDistinctLists, FormInfo.id).ToList();
                var eleList = t.ExecuteStoreQuery<frmElement>(_queryFormElements, FormInfo.id).ToArray();

                strList.ForEach(st => distList.AppendFormat("{0},", st));

                var xmlStream = new qXML(new StreamWriter(new FileStream(fullPath, FileMode.CreateNew), Encoding.UTF8))
                .WriteStartDoc()
                .WriteStartEle("form")
                    .WriteStartEle("info")
                        .WriteEleVal("name", FormInfo.entity)
                        .WriteEleVal("title", FormInfo.name)
                        .WriteEleVal("scamp", FormInfo.scamp)
                        .WriteEleVal("distinctlists", distList.ToString())
                    .WriteEndEle()
                    .WriteStartEle("panels");

                foreach (var el in eleList)
                {
                    if (el.type == "section")
                    {
                        if (firstPanel)
                            firstPanel = false;
                        else
                            xmlStream.WriteEndEle().WriteEndEle();

                        xmlStream.WriteStartEle("panel")
                            .WriteEleVal("name", el.name.ToLower())
                            .WriteEleVal("title", el.subtitle)
                            .WriteEleVal("tableclass", "scamp")
                            .WriteStartEle("elements");
                    }
                    else
                    {
                        if (firstPanel)
                        {
                            firstPanel = false;
                            xmlStream.WriteStartEle("panel")
                                .WriteEleVal("tableclass","scamp")
                                .WriteStartEle("elements");
                        }

                        var curEleType = el.processType().ToLower();
                        xmlStream.WriteStartEle("element")
                            .WriteEleVal("name", el.name.ToLower())
                            .WriteEleVal("type", curEleType);
                        //we have an exception for when element type is an image
                        if(curEleType.Equals("image",StringComparison.OrdinalIgnoreCase)){
                            xmlStream.WriteEleVal("value", el.name)
                                .WriteEleVal("label", el.subtitle);
                        }
                        else{
                            xmlStream.WriteIFEleVal(!el.label.IsNullOrEmpty(), "label", el.label)
                            .WriteIFEleVal(!el.subtitle.IsNullOrEmpty(), "subtitle", el.subtitle);
                        }
                        
                        xmlStream.WriteIFEleVal(!el.min.IsNullOrEmpty(), "min", el.min)
                            .WriteIFEleVal(!el.max.IsNullOrEmpty(), "max", el.max)
                            .WriteIFEleVal(!el.maxlength.IsNullOrEmpty(), "length", el.maxlength)
                            .WriteIFEleVal(!el.required.IsNullOrEmpty(), "required", el.required)
                            .WriteIFEleVal(!el.list.IsNullOrEmpty(), "list", "@" + el.list)
                        .WriteEndEle();
                    }
                }
                // close tags for elements, panel, panels, form
                xmlStream.WriteEndEle().WriteEndEle()
                    .WriteEndEle().WriteEndEle()
                .Close();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Failed to export SCAMP form to XML: {0}", e.ToString());
                return false;
            }

            return true;
        }

    }
}
